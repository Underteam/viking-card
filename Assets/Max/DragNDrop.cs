﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class DragNDrop : MonoBehaviour {

	protected Vector3 deltaPos;

	protected bool dragging;
	protected bool pointerDown;

	//protected GameAction actionDragStart = new GameAction(GameAction.Type.dragStart);

	public bool createCopy = false;
	protected Transform copy;

	public bool invokeEventsOnCopy = false;

	public bool dragStartsOnTap = false;

	public UnityEvent onBeginDrag;

	public UnityEvent onDrop;

	public UnityEvent onClick;

	private Vector3 startPos;

	public string dragLayer = "UI";

	// Use this for initialization
	void Start () {

	}

	private DragNDrop copyDND;

	public virtual void OnMouseDown() {
	
		/*if (!actionDragStart.Allowed()) {
			Debug.Log ("Drag not allowed");
			return;
		}//*/
		
		deltaPos = Camera.main.WorldToScreenPoint (transform.position) - Input.mousePosition;
		startPos = Camera.main.ScreenToWorldPoint (Input.mousePosition + deltaPos);
		pointerDown = true;

		if (dragStartsOnTap) {
		
			if (createCopy) {
				GameObject go = (GameObject)Instantiate (gameObject, transform.position, transform.rotation);
				copy = go.transform;
				copyDND = go.GetComponent<DragNDrop> ();
				copyDND.createCopy = false;
				copy.transform.SetParent (transform.parent);
				copy.transform.SetAsLastSibling ();
				sr = copy.GetComponent<SpriteRenderer>();

			} else sr = GetComponent<SpriteRenderer>();

			if(sr != null) {
				storedSortingLayer = sr.sortingLayerID;
				storedSortingOrder = sr.sortingOrder;
				storedLayer = sr.gameObject.layer;
				sr.sortingLayerName = dragLayer;
				sr.sortingOrder = 1500;
				sr.gameObject.layer = LayerMask.NameToLayer ("UI");
			}

			if (createCopy && invokeEventsOnCopy) {
				if (copyDND.onBeginDrag != null) copyDND.onBeginDrag.Invoke ();
			} else {
				if (onBeginDrag != null) onBeginDrag.Invoke ();
			}

			dragging = true;

		}
	}

	private SpriteRenderer sr;
	private int storedSortingLayer;
	private int storedSortingOrder;
	private int storedLayer;
	public virtual void OnMouseDrag() {

		if (!pointerDown) {
			return;
		}

		if (!dragging) {

			if(Vector3.Distance(startPos, Camera.main.ScreenToWorldPoint (Input.mousePosition + deltaPos)) > 0.1f) {

				if (createCopy) {
					GameObject go = (GameObject)Instantiate (gameObject, transform.position, transform.rotation);
					copy = go.transform;
					copyDND = go.GetComponent<DragNDrop> ();
					copyDND.createCopy = false;
					copy.transform.SetParent (transform.parent);
					copy.transform.SetAsLastSibling ();
					sr = copy.GetComponent<SpriteRenderer>();

				} else sr = GetComponent<SpriteRenderer>();
				
				if(sr != null) {
					storedSortingLayer = sr.sortingLayerID;
					storedSortingOrder = sr.sortingOrder;
					storedLayer = sr.gameObject.layer;
					sr.sortingLayerName = dragLayer;
					sr.sortingOrder = 1500;
					sr.gameObject.layer = LayerMask.NameToLayer ("UI");
				}

				if (createCopy && invokeEventsOnCopy) {
					if (copyDND.onBeginDrag != null) copyDND.onBeginDrag.Invoke ();
				} else {
					if (onBeginDrag != null) onBeginDrag.Invoke ();
				}
				dragging = true;
			}
		}

		if(dragging) {

			if (createCopy)
				copy.position = Camera.main.ScreenToWorldPoint(Input.mousePosition + deltaPos);
			else
				transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition + deltaPos);
		}
	}

	public virtual void OnMouseUp() {

		pointerDown = false;

		if (!dragging) {
			if (createCopy && invokeEventsOnCopy) {
				if (copyDND != null && copyDND.onClick != null) copyDND.onClick.Invoke ();
			} else {
				if (onClick != null) onClick.Invoke ();
			}

			return;
		}

		if(sr != null) {
			sr.sortingLayerID = storedSortingLayer;
			sr.sortingOrder = storedSortingOrder;
			sr.gameObject.layer = storedLayer;
		}

		if (createCopy && invokeEventsOnCopy) {
			if (copyDND.onDrop != null) copyDND.onDrop.Invoke ();
		} else {
			if (onDrop != null) onDrop.Invoke ();
		}


		dragging = false;
	}

	public void DestroyGO () {
	
		Destroy (gameObject);
	}
}
