﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Square : MonoBehaviour {

	private SpriteRenderer sr;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (sr == null)
			sr = GetComponent<SpriteRenderer> ();

		if (sr == null)
			return;

		Vector3 scale = transform.lossyScale;

		float w = sr.sprite.textureRect.width * scale.x;
		float h = sr.sprite.textureRect.height * scale.y;

		//Vector3 pos = transform.position;

		if (w > h) {
			Vector3 k = Vector3.one;
			k.x = h / w;
			Vector3 s = transform.localScale;
			s.Scale (k);
			transform.localScale = s;
		} else if (h > w) {
			Vector3 k = Vector3.one;
			k.y = w / h;
			Vector3 s = transform.localScale;
			s.Scale (k);
			transform.localScale = s;
		}

		//transform.position = pos;
	}
}
