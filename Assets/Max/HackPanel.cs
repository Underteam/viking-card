﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HackPanel : MonoBehaviour, IPointerDownHandler {

	public List<Image> passes;

	public List<Image> points;

	private int currPos;

	public float startSpeed = 100;
	private float speed = 100;

	private System.Action<bool> onFinish;

	// Use this for initialization
	void Start () {
		
	}

	private bool running;
	private bool waitForTouches;

	// Update is called once per frame
	void Update () {

		if (!running)
			return;

		points [currPos].transform.Rotate (0, 0, -speed*Time.deltaTime);
	}

	void LateUpdate () {
	
		if (running)
			waitForTouches = true;
	}

	[EditorButtonAttribute]
	public void Run (System.Action<bool> onFinish) {
	
		Debug.Log ("Try to run minigame " + running);

		if (running) return;

		this.onFinish = onFinish;

		speed = startSpeed;

		gameObject.SetActive (true);

		running = true;
		waitForTouches = false;

		for (int i = 0; i < points.Count; i++)
			points [i].gameObject.SetActive (false);

		points [0].gameObject.SetActive (true);
		points [0].transform.localRotation = Quaternion.identity;

		float delta = 360 / passes.Count;

		float a = Random.Range (0f, 360f);
		for (int i = 0; i < passes.Count; i++) {
			passes [i].transform.localRotation = Quaternion.Euler (0, 0, a);
			float min = (20 + (passes.Count - i) * 5) / 360f;
			float max = (25 + (passes.Count - i) * 5) / 360f;
			Debug.Log (i + " " + min + " " + max);
			passes [i].fillAmount = Random.Range (min, max);
			a += Random.Range (delta/2, 3*delta/2);
			while (a > 360) a -= 360;
		}

		currPos = 0;

		Debug.Log ("Start minigame " + currPos);
	}

	public void OnPointerDown(PointerEventData data) {
	
		if (!running) {
			gameObject.SetActive (false);
			return;
		}

		if (!waitForTouches)
			return;
		
		float min = -passes [currPos].transform.localRotation.eulerAngles.z;
		while (min < 0) min += 360;
		while (min > 360) min -= 360;
		float max = min + 360 * passes [currPos].fillAmount;

		/*while (max > 360) {
			max -= 360;
			min -= 360;
		}//*/

		float a = -points [currPos].transform.localRotation.eulerAngles.z;

		while (a < 0) a += 360;
		while (a > 360) a -= 360;

		while (a < min) a += 360;

		if (a >= min && a <= max) {
			points [currPos].gameObject.SetActive (false);
			currPos++;
			if (currPos < points.Count) {
				points [currPos].transform.localRotation = points [currPos - 1].transform.localRotation;
				points [currPos].gameObject.SetActive (true);
				speed *= 1.5f;
				Debug.Log ("Next "  + min + " " + max + " " + a);
			} else {
				Debug.Log ("Win "  + min + " " + max + " " + a);
				running = false;
				gameObject.SetActive (false);
				if (onFinish != null)
					onFinish (true);
			}
		} else {
			Debug.Log ("Lose " + min + " " + max + " " + a);
			running = false;
			gameObject.SetActive (false);
			if (onFinish != null)
				onFinish (false);
		}
	}
}
