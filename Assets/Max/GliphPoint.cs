﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GliphPoint : MonoBehaviour {

	public List<GliphPoint> posibleLinks;

	public UnityEvent onPointerDown;

	public UnityEvent onTriggerEnter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//[EditorButton]
	public void Check () {
	
		for (int i = 0; i < posibleLinks.Count; i++) {
			if (!posibleLinks [i].posibleLinks.Contains (this))
				posibleLinks [i].posibleLinks.Add (this);
		}
	}

	void OnDrawGizmos () {
	
		for (int i = 0; i < posibleLinks.Count; i++) {
			Gizmos.color = Color.red;
			Gizmos.DrawLine (transform.position, posibleLinks [i].transform.position);
		}
	}

	public void OnMouseDown() {
	
		Debug.Log ("Down");
	}

	public void OnMouseUp() {
	
		Debug.Log ("Up");
	}
}
