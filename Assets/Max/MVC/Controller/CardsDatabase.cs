﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsDatabase : MonoBehaviour {

	public static CardsDatabase instance;

	public enum CardType {
		Player,
		Enemy,
		Shield,
		Healer,
		Blaster,
		Chest,
		Money,
		Key,
		Tesla,
		Barrel,
		Trap,
		Bomb,
	}

	[System.Serializable]
	public class CardDescription {
	
		public string name;
		public CardType type;
		public Sprite image;
		public int min;
		public int max;
	}

	public CardDescription player;

	public List<CardDescription> enemies;

	public List<CardDescription> loot;

	public CardDescription money;

	private List<CardDescription> allCards = new List<CardDescription> ();

	void Awake () {
	
		instance = this;
	}

	// Use this for initialization
	void Start () {

		allCards.Add (player);
		allCards.AddRange (enemies);
		allCards.AddRange (loot);
		allCards.Add (money);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public CardDescription GetCardDescription (int i) {
	
		return allCards [i];
	}

	public Model.Card GetEnemyCard (int i, int amount = -1) {
	
		if (enemies == null || i < 0 || enemies.Count < 1 || i >= enemies.Count)
			return null;

		CardDescription cd = enemies [i];

		Model.Enemy card = new Model.Enemy (new Coord(-1, -1));
		card.name = cd.name;
		card.image = cd.image;
		if(amount < 0) card.health = Random.Range (cd.min, cd.max + 1);
		else card.health = amount;
		card.index = allCards.IndexOf(cd);
		return card;
	}

	public Model.Card GetLootCard (int i, int amount = -1) {

		if (loot == null || i < 0 || loot.Count < 1 || i >= loot.Count)
			return null;

		CardDescription cd = loot [i];

		switch (cd.type) {
		case CardType.Shield:
			{
				Model.Shield card = new Model.Shield (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				if(amount < 0) card.shield = Random.Range (cd.min, cd.max + 1);
				else card.shield = amount;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Healer:
			{
				Model.Healer card = new Model.Healer (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				if(amount < 0) card.heal = Random.Range (cd.min, cd.max + 1);
				else card.heal = amount;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Blaster:
			{
				Model.Blaster card = new Model.Blaster (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				if(amount < 0) card.damage = Random.Range (cd.min, cd.max + 1);
				else card.damage = amount;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Chest:
			{
				Model.Chest card = new Model.Chest (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Key:
			{
				Model.Key card = new Model.Key (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Tesla:
			{
				Model.Tesla card = new Model.Tesla (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				if(amount < 0) card.damage = Random.Range (cd.min, cd.max + 1);
				else card.damage = amount;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Barrel:
			{
				Model.Barrel card = new Model.Barrel (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				if(amount < 0) card.amount = Random.Range (cd.min, cd.max + 1);
				else card.amount = amount;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Trap:
			{
				Model.Trap card = new Model.Trap (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				if(amount < 0) card.damage = Random.Range (cd.min, cd.max + 1);
				else card.damage = amount;
				card.active = true;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		case CardType.Bomb:
			{
				Model.Bomb card = new Model.Bomb (new Coord(-1, -1));
				card.name = cd.name;
				card.image = cd.image;
				if (amount < 0) card.damage = Random.Range (cd.min, cd.max + 1);
				else card.damage = amount;
				card.index = allCards.IndexOf(cd);
				return card;
				//break;
			}
		}

		return null;
	}

	public Model.Card GetMoneyCard (int n) {
	
		Model.Money card = new Model.Money (new Coord(-1, -1));
		card.name = money.name;
		card.image = money.image;
		card.money = n;
		card.index = allCards.IndexOf(money);
		return card;
	}

	public Model.Card GetPlayerCard () {
		Model.Player card = new Model.Player (new Coord(-1, -1));
		card.name = player.name;
		card.image = player.image;
		card.health = Random.Range (player.min, player.max + 1);
		card.maxHealth = card.health;
		card.index = allCards.IndexOf(player);
		return card;
	}
}
