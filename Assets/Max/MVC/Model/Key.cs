﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Key : Inanimate {

		public Key (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Key;
		}

		public override void Apply (Card another)
		{
			if (another is Player) {
				(another as Player).numKeys++;
			}
		}

		public override int GetUsefulness ()
		{
			return 1;
		}
	}
}
