﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {

	public class Player : Animate {

		public Shield shield;

		public Blaster blaster;

		public int numKeys;

		public Player (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Player;
		}
	}
}