﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Trap : Card {

		public int damage;

		public bool active;

		public Trap (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Trap;
		}

		public override void Apply (Card another)
		{
			if (!active)
				return;
			
			if (another is Player) {
			
				(another as Player).health -= damage;
				if ((another as Player).health < 0)
					(another as Player).health = 0;
			}
		}

		public override void NextTurn ()
		{
			active = !active;
		}

		public override int GetUsefulness ()
		{
			return -1;
		}
	}
}