﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {

	public class Healer : Inanimate {

		public int heal;

		public Healer (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Healer;
		}

		public override void Apply (Card another) {

			Debug.Log ("Apply Healer to " + another);

			if (another is Player) {

				int toAdd = (another as Player).maxHealth - (another as Player).health;
				toAdd = Mathf.Min (toAdd, heal);
				(another as Player).health += toAdd;

				heal -= toAdd;

				Debug.Log ("Heal " + toAdd + " " + heal);
			}
		}

		public override bool Empty ()
		{
			return heal <= 0;
		}

		public override int GetIndex ()
		{
			return heal;
		}

		public override void AddToIndex (int am)
		{
			heal += am;

			if (heal < 0) heal = 0;
		}

		public override int GetUsefulness ()
		{
			return 1;
		}
	}
}
