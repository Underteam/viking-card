﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class RandomCard {

		public static Card GetRandomCard (int currUsefulness) {
		
			if (Random.Range (-5, 6) < currUsefulness)
				return CardsDatabase.instance.GetEnemyCard (Random.Range (0, CardsDatabase.instance.enemies.Count));
			else
				return CardsDatabase.instance.GetLootCard (Random.Range (0, CardsDatabase.instance.loot.Count));
		}
	}
}
