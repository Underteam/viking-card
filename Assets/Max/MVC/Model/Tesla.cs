﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Tesla : Card {

		public int damage;

		public Tesla (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Tesla;
		}

		public bool HasEffectOn(Card card) {

			if (card is Enemy) return true;
			if (card is Blaster) return true;
			if (card is Shield) return true;
			if (card is Healer) return true;

			return false;
		}

		public override int GetIndex ()
		{
			return damage;
		}

		public override int GetUsefulness ()
		{
			return 1;
		}
	}
}