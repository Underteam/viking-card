﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Money : Inanimate {

		public Money (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Money;
		}

		public int money;

		public override int GetUsefulness ()
		{
			return 1;
		}
	}
}
