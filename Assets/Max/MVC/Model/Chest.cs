﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Chest : Inanimate {

		public Chest (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Chest;
		}

		public Card Open (int currUsefulness) {
		
			if (Random.Range (-5, 6) < currUsefulness) {
				Card card = CardsDatabase.instance.GetEnemyCard (Random.Range (0, CardsDatabase.instance.enemies.Count));
				return card;
			} else {
				Card card = CardsDatabase.instance.GetLootCard (Random.Range (0, CardsDatabase.instance.loot.Count));
				int cnt = 100;
				while (card is Chest && cnt-- > 0) {
					card = CardsDatabase.instance.GetLootCard (Random.Range (0, CardsDatabase.instance.loot.Count));
				}
				return card;
			} 
		}
	}
}
