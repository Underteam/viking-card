﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Barrel : Card {

		public int amount;

		public Barrel (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Barrel;
		}

		public override void NextTurn () {

			if (amount > 2)
				amount--;
		}

		public Card Open (int currUsefulness) {

			if (Random.Range (-5, 6) < currUsefulness) {
				Card card = CardsDatabase.instance.GetEnemyCard (Random.Range (0, CardsDatabase.instance.enemies.Count), amount);
				return card;
			} else {
				Card card = CardsDatabase.instance.GetLootCard (Random.Range (0, CardsDatabase.instance.loot.Count), amount);
				int cnt = 100;
				while (card is Barrel && cnt-- > 0) {
					card = CardsDatabase.instance.GetLootCard (Random.Range (0, CardsDatabase.instance.loot.Count), amount);
				}
				return card;
			} 
		}
	}
}
