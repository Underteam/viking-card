﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Bomb : Card {

		public int damage;

		public int cntdwn;

		public Bomb (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Bomb;
		}

		public bool HasEffectOn(Card card) {

			if (card is Enemy) return true;
			if (card is Blaster) return true;
			if (card is Shield) return true;
			if (card is Healer) return true;

			return false;
		}

		public override int GetIndex ()
		{
			return damage;
		}

		public override void NextTurn ()
		{
			cntdwn--;
		}

		public override int GetUsefulness ()
		{
			return -1;
		}
	}
}
