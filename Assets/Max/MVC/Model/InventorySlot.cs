﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Model {
	
	public class InventorySlot {

		public enum Type {
		
			Blaster,
			Healer,
			Universal,
		}

		public Type type;

		public Card card;

		public InventorySlot (Type type, Card card = null) {
		
			this.type = type;
			this.card = card;
		}
	}
}