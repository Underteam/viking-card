﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Model {
	
	public class Shield : Inanimate {

		public int shield;

		public Shield (Coord coord) : base(coord) {

			type = CardsDatabase.CardType.Shield;
		}

		public override void Apply (Card another) {
			
		}

		public override bool Empty ()
		{
			return shield <= 0;
		}

		public override int GetIndex ()
		{
			return shield;
		}

		public override void AddToIndex (int am)
		{
			shield += am;

			if (shield < 0) shield = 0;
		}

		public override int GetUsefulness ()
		{
			return 1;
		}
	}
}