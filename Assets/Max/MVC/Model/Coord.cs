﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Coord {

	public int x { get; private set; }
	public int y { get; private set; }

	public Coord(int x, int y) {
	
		this.x = x;
		this.y = y;
	}

	public void Set(int x, int y) {

		//Debug.LogWarning ("Set " + this.x + " " + this.y + " -> " + x + " " + y);

		this.x = x;
		this.y = y;
	}

	public void Set(ref int x, ref int y) {
		
		//Debug.LogWarning ("Set2 " + this.x + " " + this.y + " -> " + x + " " + y);

		x = this.x;
		y = this.y;
	}

	public override string ToString ()
	{
		return string.Format (x + " " + y);
	}
}
