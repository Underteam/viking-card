#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("i92zAYKShYDWnqOHAYKLswGCh7Ovo+Dm8ffq5erg4vfmo/Ps7+rg+rMBhzizAYAgI4CBgoGBgoGzjoWK7+ajyu3grbKls6eFgNaHiJCewvMrX/2htkmmVlqMVehXIaegknQiL8pb9Rywl+Yi9BdKroGAgoOCIAGCpbOnhYDWh4iQnsLz8+/mo8Dm8feHhZCB1tCykLOShYDWh4mQicLz84wevnCoyquZS31NNjqNWt2fVUi+2iSGiv+Uw9WSnfdQNAiguMQgVuzh7+aj8Pfi7efi8eej9+bx7vCj4gOXqFPqxBf1in136A6twyV0xM789+vs8er3+rKVs5eFgNaHgJCOwvOnYWhSNPNcjMZipEly7vtuZDaUlOrl6uDi9+rs7aPC9vfr7PHq9/qyMrPbb9mHsQ/rMAyeXebwfOTd5j+1Gs+u+zRuDxhfcPQYcfVR9LPMQgzwAuNFmNiKrBExe8fLc+O7HZZ2WrX8QgTWWiQaOrHBeFtW8h39ItGFs4yFgNaekIKCfIeGs4CCgnyznvHi4Pfq4Oaj8Pfi9+bu5u338K2zNrkud4yNgxGIMqKVrfdWv45Y4ZWsswJAhYuohYKGhoSBgbMCNZkCMLaxsrezsLXZlI6wtrOxs7qxsrez+qPi8PD27ubwo+Lg4Obz9+Lt4Obt56Pg7O3n6vfq7O3wo+zlo/bw5vPv5qPA5vH36uXq4OL36uzto8L28+/mo9Hs7PejwMKznZSOs7Wzt7GOhYqpBcsFdI6CgoaGg4ABgoKD3/T0reLz8+/mreDs7qzi8/Pv5uDiSprxdt6NVvzcGHGmgDnWDM7ejnLR5u/q4u3g5qPs7aP36+rwo+Dm8T138BhtUeeMSPrMt1shvXr7fOhL57aglsiW2p4wF3R1Hx1M0zlC29OGg4ABgoyDswGCiYEBgoKDZxIqiigg8hHE0NZCLKzCMHt4YPNOZSDPhG/+ugAI0KNQu0cyPBnMieh8qH+Vs5eFgNaHgJCOwvPz7+aj0ezs94uohYKGhoSBgpWd6/f38/C5rKz0sLXZs+GyiLOKhYDWh4WQgdbQspDG/ZzP6NMVwgpH9+GIkwDCBLAJAhYd+Y8nxAjYV5W0sEhHjM5Nl+pSqQXLBXSOgoKGhoOz4bKIs4qFgNaj7OWj9+vmo/fr5u2j4vPz7+rg4uQMizejdEgvr6Ps8zW8grMPNMBMnBJYncTTaIZu3foHrmi1IdTP1m+cBgAGmBq+xLRxKhjDDa9XMhORW4WA1p6Nh5WHl6hT6sQX9Yp9d+gOo+Lt56Pg5vH36uXq4OL36uzto/MBgoOFiqkFywV04OeGgrMCcbOphUPgsPR0uYSv1WhZjKKNWTnwmsw2CJoKXXrI73aEKKGzgWubvXvTilD36uXq4OL35qPh+qPi7fqj8+Lx9/zCKxt6UknlH6foklMgOGeYqUCco8DCswGCobOOhYqpBcsFdI6CgoK+peSjCbDpdI4BTF1oIKx60OnY5/mzAYL1s42FgNaejIKCfIeHgIGCs5KFgNaHiZCJwvPz7+ajyu3grbKtwyV0xM78i92znIWA1p6gh5uzlTSYPhDBp5GpRIyeNc4f3eBLyAOU0ykJVllnf1OKhLQz9vai");
        private static int[] order = new int[] { 33,44,33,3,54,27,10,33,22,52,42,32,27,53,32,26,48,55,58,29,46,23,37,33,40,43,54,42,47,36,36,38,50,46,50,40,51,42,40,42,43,58,44,58,46,54,49,51,54,50,57,54,57,55,55,56,56,57,58,59,60 };
        private static int key = 131;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
