#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("CywAWcyYsxNSZVCemyOJSGQO1lC0Nzk2BrQ3PDS0Nzc2rL/stdjj+FSiBEcZQNaoRoisTgRfqj40wEmrBrQ3FAY7MD8csH6wwTs3NzczNjU7XN55qxzNZAi73bYhA6A7cpCay3CkKMFUEoAV24uYxzk4iSTG1fzSmYtvFTV6HLdG4M0hbFllbdfqAU8+bhquWGNas3yeclFalIzggkNlnMUIPuxmT9/G2PNLqqv/fJgRpqILFFfOWcPGYzp79uVog6a8b+dfh0cbJPZOIb9KL3CBX2uXNd8WkOX9qN/3hCEyXKTr+cXzAbeUizB+GKLuXD7WVRFUe07BUBsrawB3YZnYO/0Szj8lRBkpSr5/iFwZgOyHL9OCHHCGLlcqVINolzQ1NzY3");
        private static int[] order = new int[] { 11,1,2,11,4,13,12,12,8,11,13,13,12,13,14 };
        private static int key = 54;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
