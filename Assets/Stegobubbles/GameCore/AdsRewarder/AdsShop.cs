﻿using GameCore.ShopSystem;
using GameCore.Tutorial;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore
{
    public class AdsShop : MonoBehaviour
    {
        private static AdsShop _instance;
        public static AdsShop instance { get { if (_instance == null) _instance = FindObjectOfType<AdsShop>(); return _instance; } }

        public bool useDebugLogs = false;
        public bool useDebugGetting = false;

        public UnityEngine.Events.UnityAction onSuccessOperation;

        //public GameObject btnBuy;
        //public GameObject btnSelect;

        //public static int lastSelect { get { return PlayerPrefs.GetInt("lastSelect", 0); } set { PlayerPrefs.SetInt("lastSelect", value); PlayerPrefs.Save(); } }
        ////////public static int GetCountWatch(int id) { return PlayerPrefs.GetInt("watch" + id, 0); }
        ////////public static void SetCountWatch(int id, int count) { PlayerPrefs.SetInt("watch" + id, count); PlayerPrefs.Save(); }

        //public int[] costs = new int[] { 0, 5, 10, 30 };

        //private int _currentID = -1;
        //private int currentID { get { if (_currentID == -1) _currentID = lastSelect; return _currentID; } set { _currentID = value; } }

        //public void OnChangeCost(int id)
        //{
        //    if (costs.Length == 0) return;

        //    //Debug.Log("id =" + id);
        //    currentID = id;
        //    int countWatch = GetCountWatch(id);
        //    bool isBought = countWatch >= costs[id];
        //    //Debug.Log(string.Format("currentID = {0} | countWatch = {1} | isBought = {2}", currentID, countWatch, isBought));

        //    //UIMainMenu.instance.textSelectCharacter.text = (UIMainMenu.instance.SelectCharacter + 1) + "/" + (UIMainMenu.instance.MaxCharacter + 1);
        //    if (isBought == false && costs[id] > 0)
        //    {
        //        btnBuy.SetActive(true);
        //        btnSelect.SetActive(false);

        //        Text text = btnBuy.GetComponentInChildren<Text>();
        //        if (text) text.text = string.Format("{0}/{1}", GetCountWatch(id), costs[id]);
        //    }
        //    else
        //    {
        //        btnBuy.SetActive(false);
        //        btnSelect.SetActive(true);
        //        if (lastSelect == currentID) btnSelect.SetActive(false);
        //    }

        //}

        public void Buy()
        {
            //AdsRewardItem(currentID);
        }

        //[EditorButton]
        //public void Refresh()
        //{
        //    OnChangeCost(currentID);
        //}

        //public void Select(bool set)
        //{
        //    if (set)
        //    {
        //        //lastSelect = UIMainMenu.instance.SelectCharacter;
        //    }
        //    else
        //    {
        //        //UIMainMenu.instance.SelectCharacter = lastSelect;
        //        currentID = lastSelect;
        //    }
        //    Refresh();
        //}

        #region Ads

        public ItemManager itemManager;

        [Header("Ads")]
        // Награда за ревард (просмотр видео)
        public int rewardCount = 30;

        // Показывать рекламу или нет
        public bool showAds { get; set; }

        public void CheckAndAds()
        {
            // Если туториал не используется или закончен
            bool tutorialFlag = TutorialManager.instance == null || TutorialManager.instance.finished;
            // Показываем рекламу
            if (showAds && tutorialFlag)
            {
                showAds = false;
                Ads();
            }
        }

        // Проигрывание регулярной рекламы
        public void Ads()
        {
            if (IAPNoAds.noAds) return;
            if (useDebugLogs) Debug.Log("[ADS](Regular) TryToShowVideoBannerSkippable");
#if ADS_CONTROLLER
            AdsController.Instance().TryToShowVideoBannerSkippable();
#endif
        }

        // Проигрывание ревард рекламы
        public void AdsReward()
        {
            if (useDebugLogs) Debug.Log("[ADS](Reward) TryToShowVideoBanner");
#if ADS_CONTROLLER
            AdsController.Instance().onVideoFinished = AddReward;
            AdsController.Instance().TryToShowVideoBanner();
#endif

            //#if UNITY_EDITOR
            if (useDebugGetting && Debug.isDebugBuild)
            {
                AddReward();
            }
            //#endif
        }

        // Награда за ревард (просмотр видео)
        private void AddReward()
        {
            Shop.AddMoney(rewardCount);
        }

        // Рекламный предмет по имени
        public void AdsRewardItem(string itemName)
        {
            lastItemName = itemName;
            if (useDebugLogs) Debug.Log("[ADS](RewardItem) TryToShowVideoBanner");
#if ADS_CONTROLLER
            AdsController.Instance().onVideoFinished = AddRewardItem;
            //AdsController.Instance().TryToShowVideoBanner();

            if (AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo))
                AdsController.Instance().ShowAd(AdsProvider.AdType.unskipablevideo, (b) => {
                    Debug.Log("VideoFinished " + b);
                    if (b && AdsController.Instance().onVideoFinished != null) AdsController.Instance().onVideoFinished();
                    else
                    {
                        Shop.instance.CancelItem(lastItemName);
                    }
                });
#endif
            //#if UNITY_EDITOR
            if (useDebugGetting && Debug.isDebugBuild)
            {
                AddRewardItem();
            }
            //#endif
        }

        private string lastItemName;
        private void AddRewardItem()
        {
            bool addItemResult = false;
            //Debug.Log(string.Format("lastAdsButton = {0} | lastItemName = {1} | lastItem = {2}", lastAdsButton, lastItemName, lastItem));
            // Действие рекламной кнопки
            //SetAdsButtonActive(lastAdsButton);
            // Даем предмет
            if (string.IsNullOrEmpty(lastItemName) == false)
            {
                //Debug.Log("STRING");
                //AddItem(lastItemName);
                if (GameManager.instance.gameController != null && GameManager.instance.gameController.AddItem(lastItemName, 0))
                {
                    Debug.Log("Add Item MyGame");
                    addItemResult = true;
                }
                else
                {
                    addItemResult = itemManager.AddItem(lastItemName);
                }
                
                //Refresh();
                lastItemName = "";
            }
            //else
            //{
            //    //Debug.Log("INT");
            //    AddItem(currentID, 1);
            //}
            if (onSuccessOperation != null && addItemResult) onSuccessOperation.Invoke();
        }

        public bool AddItem(int idItem, int count)
        {
            //Debug.Log(string.Format("AddItem {0} {1} {2}", idItem, GetCountWatch(idItem), count));
            //////SetCountWatch(idItem, GetCountWatch(idItem) + count);
            //Refresh();
            return false;
        }

        //public bool AddItem(string nameItem)
        //{
        //    switch (nameItem)
        //    {
        //        case "Color":
        //            {
        //                SetAdsButtonActive();
        //            } break;
        //        case "Skin":
        //            {
        //                AddItem(currentID, 1);
        //                //Debug.Log("ADDITEM SKIN = " + nameItem);
        //                //SetAdsButtonActive();
        //            } break;
        //        case "Respawn":
        //            {
        //                SetAdsButtonActive();
        //            } break;
        //        default:
        //            break;
        //    }
        //    Refresh();
        //    return false;
        //}

        #endregion

        #region AdsButtons

        public List<AdsButton> adsButtons;
        private string lastAdsButton;

        public void RegisterButton(AdsButton btn)
        {
            if (adsButtons.Contains(btn) == false) adsButtons.Add(btn);
        }

        public void AdsButtonActivate(string buttonName)
        {
            lastAdsButton = buttonName;
        }

        public void SetAdsButtonActive(string buttonName = "")
        {
            if (string.IsNullOrEmpty(buttonName))
            {
                buttonName = lastAdsButton;
            }
            for (int i = 0; i < adsButtons.Count; i++)
            {
                if (adsButtons[i].buttonName.Equals(buttonName))
                {
                    adsButtons[i].OnDone.Invoke();
                    lastAdsButton = "";
                }
            }
            lastAdsButton = "";
        }

        #endregion
    }
}