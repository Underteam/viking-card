﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore
{
    public class AdsRewardText : MonoBehaviour
    {
        public Text rewardText;
        public string rewardFormat = "{0}";

        private void Awake()
        {
            if (rewardText == null) rewardText = GetComponent<Text>();
            rewardText.text = string.Format(rewardFormat, AdsShop.instance.rewardCount);
        }
    }
}