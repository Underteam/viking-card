﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsChecker : MonoBehaviour
{

    public bool debugBool = true;
    [Header("Events")]
    public UnityEngine.Events.UnityEvent onAdsAvailable;
    public UnityEngine.Events.UnityEvent onAdsUnavailable;
    public bool customCondition = true;
    public GameObject noAdsObject;
    public GameObject customObject;

    private void Start()
    {
        if (checkOnStart) Check();
        if (autoCheck) StartCoroutine(Refresh());
    }

    private void OnEnable()
    {
        if (checkOnEnable) Check();
    }

    [Header("Config")]
    public bool checkOnStart = false;
    public bool checkOnEnable = false;
    public bool autoCheck = false;
    public float timeStepToSave = 5;
    IEnumerator Refresh()
    {
        WaitForSeconds wfs = new WaitForSeconds(timeStepToSave);
        while (true)
        {
            yield return wfs;
            Check();
        }
    }

    public void Check()
    {
        if (noAdsObject != null && IAPNoAds.noAds) //&& GameCore.ShopSystem.Shop.noAds)
        {
            //Debug.Log("!!!!!!!!!! 0");
            noAdsObject.SetActive(true);
            gameObject.SetActive(false);
            return;
        }
        if (check())
        {
            //Debug.Log("!!!!!!!!!! 1");
            if (onAdsAvailable != null) onAdsAvailable.Invoke();
        }
        else
        {
            //Debug.Log("!!!!!!!!!! 2");
            if (onAdsUnavailable != null) onAdsUnavailable.Invoke();
        }
    }

    private bool check()
    {
        //if (IAPNoAds.noAds) return false;
        // Как запросить проверку внешнего условия?
        //if (customCondition && AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo)) return true;
        //#if UNITY_EDITOR
        //        if (customCondition && Debug.isDebugBuild)// && customCond)
        //            return debugBool;
        //#endif
        //Debug.Log("IsAdReady = "  + AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo));
#if GMA_ADS
        if (AdsController.Instance().IsAdReady(AdsProvider.AdType.unskipablevideo)) return true;
#endif
        //return AdsController.Instance.CanShowAds(); ??
        return false;
    }
}
