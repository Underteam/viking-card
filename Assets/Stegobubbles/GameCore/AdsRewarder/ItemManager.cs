﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.ShopSystem
{
    public class ItemManager : MonoBehaviour
    {

        [System.Serializable]
        public class AdsItem
        {
            public string key;
            public UnityEngine.Events.UnityEvent onDone;
        }

        public List<AdsItem> adsItems;

        public virtual bool AddItem(string itemName)
        {
            Debug.Log("ItemManager " + itemName);

            if (Shop.Buy(itemName))
            {
                return true;
            }
            //Debug.Log("!!!");
            for (int i = 0; i < adsItems.Count; i++)
            {
                if (adsItems[i].key.Equals(itemName))
                {
                    adsItems[i].onDone.Invoke();
                    break;
                }
            }

            switch (itemName)
            {
                case "Money":
                    {
                        Shop.AddMoney(AdsShop.instance.rewardCount, true);
                    }
                    break;
                case "AdsMoney":
                    {
                        Shop.AddMoney(AdsShop.instance.rewardCount, true);
                    }
                    break;
                case "Continue":
                    {
                        StateManager.instance.ChangeState(StateManager.States.ContinueGame);
                    }
                    break;
                case "AdsContinue":
                    {
                        StateManager.instance.ChangeState(StateManager.States.ContinueGame);
                    }
                    break;
                case "Color":
                    {

                    }
                    break;
                case "Skin":
                    {

                    }
                    break;
                case "Respawn":
                    {

                    }
                    break;
                default:
                    break;
            }
            return false;
        }

        /*
        public bool AddItem(string itemName, int count)
        {
            bool result = false;
            if (useDebugLogs) Debug.Log(string.Format("AddItem {0} ({1})", itemName, count));
            switch (itemName)
            {
                case "Coin":
                    AddScore(count); result = true; break;
                default:
                    result = false; break;
            }
            if (result == false)
            {
                return gameController.AddItem(itemName, count);
            }
            return result;
        }//*/
    }
}