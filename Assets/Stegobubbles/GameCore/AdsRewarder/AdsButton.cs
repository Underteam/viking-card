﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore
{
    public class AdsButton : MonoBehaviour {

        public string buttonName;

        public UnityEngine.Events.UnityEvent action;
        public UnityEngine.Events.UnityEvent OnDone;

        private void Awake()
        {
            AdsShop.instance.RegisterButton(this);
        }

        public void Activate()
        {
            AdsShop.instance.AdsButtonActivate(buttonName);
            action.Invoke();
        }
    }
}