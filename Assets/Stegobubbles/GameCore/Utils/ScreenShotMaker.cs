﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenShotMaker : MonoBehaviour {

    public Canvas shareCanvas;
    public new Camera camera;

    public Vector2 textureSize = new Vector2(1024, 1024);
    public Image shareBtnImage;

    private Texture2D _screenShot;

    public void TakeScreenShot()
    {
        System.Action actionBefore = () =>
        {
            // Включаем канвас для захвата
            shareCanvas.gameObject.SetActive(true);
            // Двигаем камеру
            camera.transform.localPosition = new Vector3(0f, 5f, 80f);
            //shareCanvas.transform.GetChild(0).GetComponent<Text>().text = highScore + string.Empty;
            //shareCanvas.transform.GetChild(1).GetComponent<Text>().text = score + string.Empty;
        };

        System.Action actionAfter = () =>
        {
            // Возвращаем камеру
            camera.transform.localPosition = Vector3.zero;
            // Выключаем канвас для захвата
            shareCanvas.gameObject.SetActive(false);
        };

        Texture2D screenShot = TakeScreenShot(camera, actionBefore, actionAfter);
        // Создаем спрайт из текстуры
        Sprite sprite = Sprite.Create(screenShot, new Rect(0f, 0f, textureSize.x, textureSize.y), new Vector2(0f, 0f));
        // Назначаем спрайт Image'у
        shareBtnImage.sprite = sprite;
    }

    Texture2D TakeScreenShot(Camera camera, System.Action actionBefore, System.Action actionAfter)
    {
        if (actionBefore != null) actionBefore.Invoke();

        // Создаем рендер-текстуру
        RenderTexture renderTexture = new RenderTexture((int)textureSize.x, (int)textureSize.y, 24);
        // Назначаем текстуру камере
        camera.GetComponent<Camera>().targetTexture = renderTexture;
        // Создаем текстуру
        Texture2D result = new Texture2D((int)textureSize.x, (int)textureSize.x, TextureFormat.RGB24, false);
        // Рендерим камеру
        camera.Render();
        // Выбираем активной нашу рендер-текстуру
        RenderTexture.active = renderTexture;
        // Считываем данные в текстуру
        result.ReadPixels(new Rect(0f, 0f, textureSize.x, textureSize.y), 0, 0);
        // Применяем
        result.Apply();
        // Убираем рендер текстуру из камеры
        camera.targetTexture = null;
        // Убираем текущую активную рендер текстуру
        RenderTexture.active = null;
        // Уничтожаем рендер текстуру
        Destroy(renderTexture);

        if (actionAfter != null) actionAfter.Invoke();
        return result;
    }
}
