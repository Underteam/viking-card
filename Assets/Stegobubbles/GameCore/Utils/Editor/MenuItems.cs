﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MenuItems : Editor {

    [MenuItem("ClearPlayerPrefs", menuItem = "Stegobubbles/PlayerPrefs/Clear")]
	static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("PlayerPrefs was cleared!");
    }
}
