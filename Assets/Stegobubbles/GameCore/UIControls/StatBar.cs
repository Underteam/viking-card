﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatBar : MonoBehaviour {

    public Slider slider;
    public Text text;
    public string format = "{0}";
    public string formatFull = "{0}/{1}";

    public void SetMax(float value)
    {
        slider.maxValue = value;
    }

    public void SetCurrent(float value)
    {
        slider.value = value;
        if (text != null && text.enabled) text.text = string.Format(formatFull, slider.value, slider.maxValue);
    }
}
