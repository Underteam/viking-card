﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ButtonToggle : MonoBehaviour {

    Image image;
    public Sprite on;
    public Sprite off;
    public bool active = false;

    public UnityEvent eventOn;
    public UnityEvent eventOff;

    private void Start()
    {
        image = GetComponent<Image>();
        SwitchTo(active);
    }

    public void SwitchTo(bool active)
    {
        image.sprite = active ? on : off;
        if (active)
        {
            eventOn.Invoke();
        }
        else
        {
            eventOff.Invoke();
        }
    }

    public void Switch()
    {
        active = !active;
        SwitchTo(active);
    }
}
