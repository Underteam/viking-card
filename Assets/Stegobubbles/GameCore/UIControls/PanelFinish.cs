﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using GameCore.ShopSystem;

namespace GameCore
{
    public class PanelFinish : MonoBehaviour
    {
        public Text textTitle;
        public bool changeTitle = true;

        public Text countCurrent;
        public Text countTotal;

        public Text[] otherCountCurrent;
        public Text[] otherCountTotal;

        public string countCurrentFormat = "{0}";
        public string countTotalFormat = "{0}";

        public bool useButtonEvents = false;

        public UnityEvent onEnable;

        public Button btnClose;
        public UnityEvent onClose;

        public Button btnRestart;
        public UnityEvent onRestart;

        public Button btnContinue;
        public UnityEvent onContinue;

        public GameObject AdsActionPanel;

        public string titleWin = "Level completed!";
        public string titleLoose = "Level failed!";

        public bool useLocalization = true;
        public string titleWinKey = "finish";
        public string titleLooseKey = "gameOver";

        public enum State { Pause, Win, Loose }
        public State state = State.Pause;

        public void Init(State state = State.Pause)
        {
            this.state = state;

            if (changeTitle && textTitle)
            {
                if (useLocalization)
                {
                    textTitle.text = Localization.Localizer.instance.GetText(state == State.Win ? titleWinKey : titleLooseKey);
                }
                else
                {
                    textTitle.text = state == State.Win ? titleWin : titleLoose;
                }
                
            }

            // Инициализация кнопок
            if (useButtonEvents)
            {
                // Закрытие
                if (btnClose)
                {
                    btnClose.onClick.RemoveAllListeners();
                    btnClose.onClick.AddListener(() => onClose.Invoke());
                }
                // Рестарт
                if (btnRestart)
                {
                    btnRestart.onClick.RemoveAllListeners();
                    btnRestart.onClick.AddListener(() => onRestart.Invoke());
                }
                // Продолжение игры
                if (btnContinue)
                {
                    btnContinue.onClick.RemoveAllListeners();
                    btnContinue.onClick.AddListener(() => onContinue.Invoke());
                }
            }
            if (btnContinue)
            {
                //Debug.Log(state);
                // Включение/выключение объектов в зависимости от состояния
                switch (state)
                {
                    case State.Pause:
                        {
                            btnContinue.gameObject.SetActive(true);
                        }
                        break;
                    case State.Win:
                        {
                            btnContinue.gameObject.SetActive(true);
                        }
                        break;
                    case State.Loose:
                        {
                            btnContinue.gameObject.SetActive(false);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public void CheckContinue()
        {
            if (AdsActionPanel)
            {
                GameManager.instance.panelContinue.Show();
                //AdsActionPanel.SetActive(true);
            }
        }

        public void InitScore(int score, int highScore)
        {
            //int score = GameManager.instance.score;
            //int highScore = GameManager.instance.GetHighScore();

            GameManager.instance.SetLastScore(score);
            if (score > highScore)
            {
                highScore = score;
                GameManager.instance.SetHighScore(score);
            }

            string textCountCurrent = string.Format(countCurrentFormat, score);
            string textCountTotal = string.Format(countTotalFormat, highScore);

            if (countCurrent) countCurrent.text = textCountCurrent;
            if (countTotal) countTotal.text = textCountTotal;

            if (otherCountCurrent.Length > 0)
            {
                for (int i = 0; i < otherCountCurrent.Length; i++)
                {
                    otherCountCurrent[i].text = textCountCurrent;
                }
            }

            if (otherCountTotal.Length > 0)
            {
                for (int i = 0; i < textCountTotal.Length; i++)
                {
                    otherCountTotal[i].text = textCountTotal;
                }
            }
        }

        private void OnEnable()
        {
            Init(state);
        }

        private void OnDisable()
        {
            if (state == State.Win)
            {
                AddScore();
            }
        }

        void AddScore()
        {
            if (GameManager.instance.useDebugLogs) Debug.Log("Score Added: " + GameManager.instance.score);
            Shop.AddMoney(GameManager.instance.score);
            ResetScore();
        }


        public void ResetScore()
        {
            GameManager.instance.score = 0;
        }


    }
}