﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore
{
    public class DataText : MonoBehaviour
    {
        [System.Serializable]
        public class TextData
        {
            public string key;
            public Text textField;
            public string format = "{0}";

            public GameManager.GameDataType dataType = GameManager.GameDataType.Int;

            public void UpdateValue()
            {
                if (textField) textField.text = string.Format(format, GameManager.GetGameData(dataType, key));
            }
        }

        public List<TextData> textDatas;

        void OnEnable()
        {
            if (textDatas.Count > 0)
            {
                for (int i = 0; i < textDatas.Count; i++)
                {
                    textDatas[i].UpdateValue();
                }
            }
        }
    }
}