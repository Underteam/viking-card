﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TogglePanel : MonoBehaviour {

    // Группа
    public ToggleGroup toggleGroup;

    // Список кнопок-переключателей
    public List<Toggle> list;

    public UnityEngine.Events.UnityAction<int> onSelect;
    private bool actionCheck = false;

    private void Awake()
    {
        // Инициализация событий
        for (int i = 0; i < list.Count; i++)
        {
            list[i].onValueChanged.AddListener(OnChanged);
        }
    }

    // При изменении значения
    void OnChanged(bool active)
    {
        if (actionCheck) return;
        else
        {
            actionCheck = true;
            Invoke("Change", 1f);
        }
    }

    // Применение изменения
    void Change()
    {
        int[] selected = GetSelected();
        for (int i = 0; i < selected.Length; i++)
        {
            Set(selected[i]);
        }
        actionCheck = false;
    }

    // Получение списка выбранных
    int[] GetSelected()
    {
        List<int> result = new List<int>();
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].isOn) result.Add(i);
        }
        return result.ToArray();
    }

    [EditorButton]
    // Выбираем
	public void Set(int id)
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i].isOn = i == id;
            if (i == id)
            {
                if (onSelect != null) onSelect.Invoke(id);
            }
        }
    }
}
