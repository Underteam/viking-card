﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[ExecuteInEditMode]
public class CountPanel : MonoBehaviour {

    [Header("Main")]
    public IntRange countValue;
    //[Range(0, 10)]
    public int count { get { return countValue.Value; } set { countValue.Value = value; ChangeCount(countValue.Value); } }
    public int countMax { get { return countValue.MaxValue; } set { countValue.MaxValue = value; ChangeCount_SetActive(countValue.MaxValue); } }
    private int oldCount = 5;

    public enum Behaviour { SetActive, ChangeColor, ChangeSprite, SpawnActive }
    public Behaviour behaviour;

    public List<GameObject> objects;

    [Header("ChangeColor")]
    public Color colorOn;
    public Color colorOff;
    [Header("ChangeSprite")]
    public Sprite spriteOn;
    public Sprite spriteOff;
    [Header("Spawn")]
    public GameObject objectPrefab;
    public Transform parent;

    public UnityEngine.Events.UnityEvent actionZero;
    public UnityEngine.Events.UnityEvent onChangeCountObjects;

    private void OnValidate()
    {
        if (oldCount != count)
        {
            count = Mathf.Clamp(count, 0, countMax);
            ChangeCount(count);
            //if (count <= 0) actionZero.Invoke();
            oldCount = count;
        }
    }

    [EditorButton]
    public void RefreshMax()
    {
        countMax = countMax;
    }

    [EditorButton]
    public void ResetCountMax()
    {
        SetCount(countMax);
    }

    [EditorButton]
    public void ResetCountZero()
    {
        SetCount(0);
    }

    // Добавляем всех детей в список объектов (с возможностью инвертировать последовательность)
    [EditorButton]
    void AddChildren(bool invert = false)
    {
        List<Transform> childs = new List<Transform>( GetComponentsInChildren<Transform>());
        childs.Remove(transform);
        List<GameObject> result = new List<GameObject>();
        for (int i = 0; i < childs.Count; i++)
        {
            result.Add(childs[i].gameObject);
        }
        if (invert) result.Reverse();
        objects = result;

    }

    // Добавляем/отнимаем количество
    public void AddCount(int count)
    {
        if (this.count + count <= countMax && this.count + count >= 0)
        {
            this.count += count;
            ChangeCount(this.count);
        }
    }

    // Устанавливаем значение
    public void SetCount(int count)
    {
        this.count = count;
        ChangeCount(this.count);
    }

    // Изменение количества
    void ChangeCount(int newValue)
    {
        switch (behaviour)
        {
            case Behaviour.SetActive: { ChangeCount_SetActive(newValue); } break;
            case Behaviour.ChangeColor: { ChangeCount_ChangeColor(newValue); } break;
            case Behaviour.ChangeSprite: { ChangeCount_ChangeSprite(newValue); } break;
            case Behaviour.SpawnActive: { ChangeCount_SpawnActive(newValue); } break;
            default: break;
        }

        if (newValue <= 0)
        {
            actionZero.Invoke();
        }
    }

    // Изменение активности объектов
    void ChangeCount_SetActive(int newValue)
    {
        for (int i = 0; i < objects.Count; i++)
        {
            objects[i].SetActive(i < count);
        }
    }

    // Изменение цвета объектов
    void ChangeCount_ChangeColor(int newValue)
    {
        SpriteRenderer spriteRenderer;
        Image image;
        for (int i = 0; i < objects.Count; i++)
        {
            // SpriteRenderer
            spriteRenderer = objects[i].GetComponent<SpriteRenderer>();
            if (spriteRenderer) spriteRenderer.color = i < count ? colorOn : colorOff;
            // Image
            image = objects[i].GetComponent<Image>();
            if (image) image.color = i < count ? colorOn : colorOff;
        }
    }

    // Изменение спрайта объектов
    void ChangeCount_ChangeSprite(int newValue)
    {
        SpriteRenderer spriteRenderer;
        Image image;
        for (int i = 0; i < objects.Count; i++)
        {
            // SpriteRenderer
            spriteRenderer = objects[i].GetComponent<SpriteRenderer>();
            if (spriteRenderer) spriteRenderer.sprite = i < count ? spriteOn : spriteOff;
            // Image
            image = objects[i].GetComponent<Image>();
            if (image) image.sprite = i < count ? spriteOn : spriteOff;
        }
    }

    void ChangeCount_SpawnActive(int newValue)
    {
        // Удаляем пустые
        for (int i = objects.Count - 1; i >= 0; i--)
        {
            if (objects[i] == null) objects.RemoveAt(i);
        }
        // Парент
        if (parent == null) parent = transform;
        // Создаем недостающие
        if (objects.Count < countMax)
        {
            int count = countMax - objects.Count;
            for (int i = 0; i < count; i++)
            {
                GameObject newGO = Instantiate(objectPrefab, parent);
                newGO.name = string.Format("{0} ({1})", objectPrefab.name, i + 1);
                objects.Add(newGO);
            }
        }
        // Меняем состояние
        for (int i = 0; i < objects.Count; i++)
        {
            objects[i].SetActive(i < newValue);
        }
        // Вызов события при изменении количества
        if (onChangeCountObjects != null) onChangeCountObjects.Invoke();
    }
}
