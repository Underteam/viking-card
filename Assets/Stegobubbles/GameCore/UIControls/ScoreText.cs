﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore
{
    public class ScoreText : MonoBehaviour
    {
        public Text scoreText;
        public Text highText;
        public Text singleText;
        public Text lastText;

        public string scoreFormat = "{0}";
        public string highFormat = "{0}";
        [Multiline]
        public string singleFormat = "{0}/n{1}";
        public string lastFormat = "{0}";

        public bool scoreIsLast = false;

        void OnEnable()
        {
            if (scoreText) scoreText.text = string.Format(scoreFormat, score);
            if (highText) highText.text = string.Format(highFormat, high);
            if (singleText) singleText.text = string.Format(singleFormat, score, high);
            if (lastText) lastText.text = string.Format(lastFormat, lastScore);
        }

        int lastScore { get { return GameCore.GameManager.instance.GetLastScore(); } }
        int score { get { return GameCore.GameManager.instance.score; } }
        int high { get { return GameCore.GameManager.instance.GetHighScore(); } }
    }
}