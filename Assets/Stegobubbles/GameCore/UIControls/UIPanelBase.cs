﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanelBase : MonoBehaviour {

    public UnityEngine.Events.UnityEvent onShow;
    public UnityEngine.Events.UnityEvent onHide;

    public bool activeSelf { get { return gameObject.activeSelf; } }

    [EditorButton]
    public void Show()
    {
        //Debug.Log(string.Format("Show: {0}", name));
        gameObject.SetActive(true);
        if (onShow != null) onShow.Invoke();
        // Проверяем рекламу
        if (GameCore.AdsShop.instance) GameCore.AdsShop.instance.CheckAndAds();
    }

    [EditorButton]
    public void Hide()
    {
        //Debug.Log(string.Format("Hide: {0}", name));
        gameObject.SetActive(false);
        if (onHide != null) onHide.Invoke();
    }

}
