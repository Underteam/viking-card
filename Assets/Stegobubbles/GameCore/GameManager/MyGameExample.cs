﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using GameCore;
using GameCore.StatsSystem;

public class MyGameExample : GameCore.Game
{

    private static MyGameExample _instance;
    public static MyGameExample instance { get { if (_instance == null) _instance = FindObjectOfType<MyGameExample>(); return _instance; } }

    public RectTransform fader;

    public GameObject[] objectsToStart;

    public GameObject BtnFinishContinue;
    public GameObject BtnAction;

    private void Awake()
    {

    }

    #region Base

    public override void StartGame(bool newGame)
    {
        // Выключаем 
        for (int i = 0; i < objectsToStart.Length; i++)
        {
            if (objectsToStart[i]) objectsToStart[i].SetActive(false);
        }
        for (int i = 0; i < objectsToStart.Length; i++)
        {
            if (objectsToStart[i]) objectsToStart[i].SetActive(true);
        }

        //MapMagic.MapMagic.instance.Generate(true);

        // Сбрасываем счет
        GameCore.GameManager.instance.SetScore(0);
        // Текущий левел
        //currentLevel = 1;

        statsManagerVehicle.Init(true);
        statsManagerPlayer.Init(true);

        SetCanMove(true);
        //statsManagerVehicle.GetComponentInParent<ThirdPersonSwimmerController>().canMove = true;
        //statsManagerPlayer.GetComponentInParent<ThirdPersonSwimmerController>().canMove = true;

        //if (QuestManager.instance.currentQuestID != currentLevel) QuestManager.instance.SelectLevel(currentLevel);

        //if (currentLevel != LevelManager.instance.lastOpennedLevel)
        //{
        //    currentLevel = LevelManager.instance.lastOpennedLevel;
        //    LevelManager.instance.SelectLevel(currentLevel);
        //}
        //StartLevel();
        //gameState = GameState.ingame;
    }

    public override void RestartGame()
    {
        //if (vehicleSitter.inVehicle) vehicleSitter.UseIt();
        //QuestManager.instance.currentQuestID = -1;
        GameCore.GameManager.instance.StartGame(true);
    }

    public override void ContinueGame()
    {

        //if (gameState == GameState.end)
        //{
        //    if (currentLevel < LevelManager.instance.lastOpennedLevel)
        //    {
        //        LevelManager.instance.InitButtons();
        //        currentLevel++;
        //        GameManager.instance.StartGame();
        //    }
        //}
        //else
        //{
        //    gameState = GameState.ingame;
        //}
    }

    // Победа
    public override void Win()
    {
        //gameState = GameState.end;
        SetCanMove(false);

        // Инициализируем попап на победу
        GameManager.instance.panelWin.GetComponent<PanelFinish>().Init(PanelFinish.State.Win);
        // Меняем состояние
        StateManager.instance.ChangeState(StateManager.States.Finish);

        //if (currentLevel == LevelManager.instance.lastOpennedLevel) LevelManager.instance.lastOpennedLevel++;
    }

    // Проигрыш
    public override void Loose()
    {
        Debug.Log("Loose");
        //gameState = GameState.end;
        SetCanMove(false);
        //StartCoroutine(this.iWaitAndReset());

        // Инициализируем попап на проигрыш
        GameManager.instance.panelLoose.GetComponent<PanelFinish>().Init(PanelFinish.State.Loose);
        // Меняем состояние
        StateManager.instance.ChangeState(StateManager.States.Finish);
    }

    // Проигрыш
    IEnumerator iWaitAndReset()
    {
        yield return new WaitForSeconds(1.5f);

        // Проигрываем
        base.Loose();
        // Задаем счет
        GameManager.instance.panelWin.GetComponent<PanelFinish>().InitScore(GameManager.instance.score, currentLevel);

        // Score => HighScore
        int score = GameManager.instance.score;
        int highScore = int.Parse(GameManager.GetGameData(GameManager.GameDataType.Int, "HighScore"));
        if (score > highScore)
        {
            GameManager.SetGameData(GameManager.GameDataType.Int, "HighScore", score);
        }

        // Stage => HighStage
        int stage = currentLevel;
        int highStage = int.Parse(GameManager.GetGameData(GameManager.GameDataType.Int, "HighStage"));
        if (stage > highStage)
        {
            GameManager.SetGameData(GameManager.GameDataType.Int, "HighStage", stage);
        }

    }

    #endregion


    #region Game

    public void ResetGameState()
    {
        //gameState = GameState.lobby;
    }

    private int _currentLevel;
    public int currentLevel { get { return _currentLevel; } set { _currentLevel = value; } }

    public Transform playerTransform;
    public Transform waterTransform;

    public StatsManager statsManager { get { return statsManagerPlayer.isActiveAndEnabled ? statsManagerPlayer : statsManagerVehicle; } }

    public StatsManager statsManagerPlayer;
    public StatsManager statsManagerVehicle;

    //public VehicleSitter vehicleSitter;

    //[Header("Game")]
    //public GameState gameState = GameState.lobby;


    void StartLevel()
    {
        if (gameDebug) Debug.Log("Start level №" + currentLevel);

        //gameState = GameState.ingame;

    }

    // Проигрыш игрока
    public void PlayerDead()
    {
        //if (gameState != GameState.end)
        //{
        //    gameState = GameState.end;
        //    GameCore.GameManager.instance.Loose();
        //}
    }

    public void SetCanMove(bool active)
    {
        //ThirdPersonSwimmerController vehicleSwimmer = statsManagerVehicle.GetComponentInParent<ThirdPersonSwimmerController>();
        //if (vehicleSwimmer) vehicleSwimmer.canMove = active;
        //ThirdPersonSwimmerController playerSwimmer = statsManagerPlayer.GetComponentInParent<ThirdPersonSwimmerController>();
        //if (playerSwimmer) playerSwimmer.canMove = active;
    }

    #endregion // Game

    #region Ads

    public override bool useAdsAction { get { return StateManager.instance.previousState.state == StateManager.States.NewGame || StateManager.instance.previousState.state == StateManager.States.RestartGame; } }

    public override bool AddItem(string itemName, int count)
    {
        //Debug.Log(string.Format("Add item id{0} ({1})", idItem, count));
        switch (itemName)
        {
            // Здоровье
            case "Health":
                {
                    statsManager.GetStatData("Health").SetMax();
                }
                break;
            // Энергия
            case "Energy":
                {
                    statsManager.GetStatData("Energy").SetMax();
                }
                break;
            // Кислород
            case "Oxygen":
                {
                    statsManager.GetStatData("Oxygen").SetMax();
                }
                break;
            // Продолжение игры (дополнительная жизнь)
            case "Continue":
                {
                    //gameState = GameState.ingame;
                    // Дополнительная жизнь
                    statsManager.GetStatData("Health").SetMax();
                    statsManager.GetStatData("Energy").SetMax();
                    statsManager.GetStatData("Oxygen").SetMax();
                    // Смена состояния
                    StateManager.instance.ChangeState(StateManager.States.ContinueGame);
                }
                break;
            default:
                break;
        }

        // Деньги
        //{
        //Debug.Log("Add Gold!");
        //GameCore.ShopSystem.Shop.AddMoney(GameCore.GameManager.instance.rewardCount, true);
        //}


        return false;
    }

    #endregion
}


