﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using GameCore.ShopSystem;

namespace GameCore
{
    public class GameManager : MonoBehaviour
    {
        public bool useDebugLogs = false;
        public bool isDebug { get; set; }

        private static GameManager _instance;
        public static GameManager instance { get { if (_instance == null) _instance = FindObjectOfType<GameManager>(); return _instance; } }

        [Header("Main")]
        public bool gameStarted;
        public Game gameController;

        [Header("Panels")]
        public UIPanelBase panelGame;
        public UIPanelBase panelMessage;
        public UIPanelBase panelPause;
        public UIPanelBase panelWin;
        public UIPanelBase panelLoose;
        public UIPanelBase panelContinue;
        public UIPanelBase panelResume;

        public PanelFinish panelFinish;

        [Header("Resume")]
        public Text resumeLabel;

        [Header("Score")]
        public Text scoreText;
        public int score = 0;

        #region TEMP (ИЗБАВИТЬСЯ ОТ ЭТОГО)

        #if UNITY_EDITOR

        [EditorButton]
        void AddMoney(int count = 100)
        {
            Shop.AddMoney(count);
        }

        [EditorButton]
        public void ClearPrefs()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("PlayerPrefs Cleared!");
        }

        #endif

        #endregion

        private void Awake()
        {

        }

        #region Main States

        // Поиск поверхности
        public void FindGame()
        {
            if (useDebugLogs) Debug.Log("FindGame");
            gameController.FindGame();
        }

        // Начинаем игру
        public void StartGame(bool newGame)
        {
            if (useDebugLogs) Debug.Log("StartGame");
            gameController.StartGame(newGame);
        }

        // Перезапускаем игру
        public void RestartGame()
        {
            if (useDebugLogs) Debug.Log("RestartGame");
            gameController.RestartGame();
        }

        // Продолжаем игру
        public void ContinueGame()
        {
            if (useDebugLogs) Debug.Log("ContinueGame");
            gameController.ContinueGame();
        }

        // Прекращаем игру
        public void StopGame()
        {
            if (useDebugLogs) Debug.Log("StopGame");
            gameController.StopGame();
        }

        // Установка паузы
        public void Pause(bool showPopup = false)
        {
            if (useDebugLogs) Debug.Log("Pause");
            //Time.timeScale = 0;
            if (panelPause)
            {
                panelPause.Show();
            }
            else
            {
                gameController.Pause(showPopup);
            }
        }

        // Снятие с паузы
        public void UnPause()
        {
            if (useDebugLogs) Debug.Log("UnPause");
            Time.timeScale = 1;
            gameController.UnPause();
        }

        // Победа
        public void Win()
        {
            if (useDebugLogs) Debug.Log("Win");
            gameController.Win();
        }

        // Проигрыш
        [EditorButton]
        public void Loose()
        {
            if (useDebugLogs) Debug.Log("Loose");
            gameController.Loose();
        }

        #endregion

        #region ApplicationResume

        //private void OnApplicationPause(bool pauseStatus)
        //{
        //    if (gameStarted)
        //    {
        //        if (pauseStatus)
        //        {
        //            panelResume.Show();
        //            //Time.timeScale = 0f;
        //        }
        //        else
        //        {
        //            StartCoroutine(resumeGame());
        //        }
        //    }
        //}

        public void ResumeGame()
        {
            StartCoroutine(resumeGame());
        }

        // Продолжение игры после вншней паузы
        private IEnumerator resumeGame()
        {
            if (resumeLabel)
            {
                resumeLabel.text = "3";
                yield return new WaitForSecondsRealtime(0.5f);
                resumeLabel.text = "2";
                yield return new WaitForSecondsRealtime(0.5f);
                resumeLabel.text = "1";
                yield return new WaitForSecondsRealtime(0.5f);
            }
            Time.timeScale = 1f;
            if (panelResume) panelResume.Hide();
            yield return null;
        }

        #endregion

        #region Score 

        public void AddScore(int count)
        {
            score++;
            SetScore(score);
        }

        public void SetScore(int count)
        {
            score = count;
            if (scoreText) scoreText.text = score.ToString();
        }

        public int GetHighScore()
        {
            return PlayerPrefs.GetInt("HighScore", 0);
        }

        public void SetHighScore(int value)
        {
            PlayerPrefs.SetInt("HighScore", value);
            PlayerPrefs.Save();
        }

        public int GetLastScore()
        {
            return PlayerPrefs.GetInt("LastScore", 0);
        }

        public void SetLastScore(int value)
        {
            PlayerPrefs.SetInt("LastScore", value);
            PlayerPrefs.Save();
        }

        public int GetMoneyByScore()
        {
            return gameController.GetMoneyByScore();
        }

        #endregion

        #region GameData

        public enum GameDataType { Int, Float, Bool, String };

        public static string GetGameData(GameDataType dataType, string key)
        {
            switch (dataType)
            {
                case GameDataType.Int: return PlayerPrefs.GetInt(key, 0).ToString();
                case GameDataType.Float: return PlayerPrefs.GetFloat(key, 0).ToString();
                case GameDataType.Bool: return (PlayerPrefs.GetInt(key, 0) == 1 ? true : false).ToString();
                case GameDataType.String: return PlayerPrefs.GetString(key, "");
                default: return "";
            }
        }

        public static void SetGameData(GameDataType dataType, string key, object value)
        {
            switch (dataType)
            {
                case GameDataType.Int: PlayerPrefs.SetInt(key, int.Parse(value.ToString())); break;
                case GameDataType.Float: PlayerPrefs.GetFloat(key, float.Parse(value.ToString())); break;
                case GameDataType.Bool: PlayerPrefs.SetInt(key, bool.Parse(value.ToString()) ? 1 : 0); break;
                case GameDataType.String: PlayerPrefs.SetString(key, value.ToString()); break;
                default: break;
            }
            PlayerPrefs.Save();
        }

        #endregion
    }
}