﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore
{
    public class Game : MonoBehaviour
    {
        public bool gameDebug = false;

        #region Main States

        public virtual void FindGame()
        {
            GameManager.instance.StopGame();
        }

        public virtual void StartGame(bool newGame)
        {

        }

        public virtual void RestartGame()
        {

        }

        public virtual void ContinueGame()
        {

        }

        public virtual void StopGame()
        {

        }

        public virtual void Pause(bool showPopup = false)
        {
            if (showPopup)
            {
                // Инициализируем попап на паузу
                GameManager.instance.panelWin.GetComponent<PanelFinish>().Init(PanelFinish.State.Pause);
                // Меняем состояние и показываем поп-ап (если нужно)
                StateManager.instance.ChangeState(StateManager.States.Pause);
            }
        }

        public virtual void UnPause()
        {

        }

        public virtual void Win()
        {
            //panelWin.SetActive(true);
            //panelWin.GetComponent<PanelFinish>().state = PanelFinish.State.Win;
            // Инициализируем попап на победу
            GameManager.instance.panelWin.GetComponent<PanelFinish>().Init(PanelFinish.State.Win);
            // Задаем счет
            GameManager.instance.panelWin.GetComponent<PanelFinish>().InitScore(GameManager.instance.score, GameManager.instance.GetHighScore());
            // Меняем состояние
            StateManager.instance.ChangeState(StateManager.States.Finish);
        }

        // Проигрыш
        public virtual void Loose()
        {
            //panelLoose.SetActive(true);
            //panelWin.GetComponent<PanelFinish>().state = PanelFinish.State.Loose;

            // Меняем состояние
            StateManager.instance.ChangeState(StateManager.States.Finish);
            // Инициализируем попап на проигрыш
            GameManager.instance.panelWin.GetComponent<PanelFinish>().Init(PanelFinish.State.Loose);
            // Задаем счет
            GameManager.instance.panelWin.GetComponent<PanelFinish>().InitScore(GameManager.instance.score, GameManager.instance.GetHighScore());
        }

        #endregion

        #region Score

        public virtual int GetMoneyByScore()
        {
            return GameManager.instance.score;
        }

        #endregion

        #region Items

        public virtual bool AddItem(string itemName, int count = 1)
        {
            return false;
        }

        #endregion

        #region Ads

        public virtual bool useAdsAction { get { return false; } }

        #endregion

    }
}