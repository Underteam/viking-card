﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameCore.ShopSystem
{
    public interface IShopSelectable
    {
        // Текущее значение
        int currentID { get; set; }
        // Следующий
        void Next();
        // Предыдущий
        void Previous();
        // Установить значение
        void Set(int id);
    }

    public class ShopItemSelector : MonoBehaviour, IShopSelectable
    {

        #region Special Classes

        [System.Serializable]
        public class IntEvent : UnityEvent<int> { }

        [System.Serializable]
        public class Events
        {
            public UnityEvent onNext;
            public UnityEvent onPrevious;
            public IntEvent onSet;
        }

        #endregion

        public bool debugLog = false;

        // Ключ для сохранения в префсы
        public string prefsKey = "Item";
        public int savedID { get { return PlayerPrefs.GetInt(prefsKey, 0); } set { PlayerPrefs.SetInt(prefsKey, value); PlayerPrefs.Save(); } }
        // Имя предмета для магазина
        public string itemName { get { return string.Format("{0}{1}", prefsKey, currentID); } }

        public bool setOnLoad = false;

        // События для переключения
        public Events events;
        // Переключаемый объект
        public GameObject selectable;
        public GameObject[] otherSelectable;

        private IShopSelectable _iss;
        private IShopSelectable iss { get { if (_iss == null && selectable != null) _iss = selectable.GetComponent<IShopSelectable>(); return _iss; } set { if (iss != null) _iss = value; } }

        [Header("Buttons")]
        public Button btnSelect;
        public Button btnBuyMoney;
        public Button btnBuyWatch;

        // Инициализация кнопок
        public void InitButton(Shop.ShopItemData itemData) //Shop.PayType type, string costString) //, string itemName)
        {
            string costString = GetCostString(itemData);

            // Сбрасываем все 
            btnBuyMoney.onClick.RemoveAllListeners();
            btnBuyWatch.onClick.RemoveAllListeners();

            // Кнопка выбора
            if (string.IsNullOrEmpty(costString))
            {
                SetActive(btnSelect.gameObject, currentID == savedID ? false : true); // +
                SetActive(btnBuyMoney.gameObject, false);
                SetActive(btnBuyWatch.gameObject, false);

                btnSelect.onClick.RemoveAllListeners();
                btnSelect.onClick.AddListener(() => Set(currentID));

                StateButton stateButton = btnSelect.GetComponent<StateButton>();
                if (stateButton) btnSelect.onClick.AddListener(() => stateButton.Activate());
            }
            // Кнопки покупки
            else
            {
                switch (itemData.payType)
                {
                    // За валюту
                    case Shop.PayType.money:
                        {
                            SetActive(btnSelect.gameObject, false);
                            SetActive(btnBuyMoney.gameObject, true); // +
                            SetActive(btnBuyWatch.gameObject, false);
                            btnBuyMoney.GetComponentInChildren<Text>().text = costString;
                        }
                        break;
                    // За просмотры
                    case Shop.PayType.watch:
                        {
                            SetActive(btnSelect.gameObject, false);
                            SetActive(btnBuyMoney.gameObject, false);
                            SetActive(btnBuyWatch.gameObject, true); // +
                            btnBuyWatch.GetComponentInChildren<Text>().text = costString;
                        }
                        break;
                    default:
                        break;
                }
            }

            ShopButton shopButtonMoney = btnBuyMoney.GetComponent<ShopButton>();
            if (shopButtonMoney)
            {
                shopButtonMoney.SetData(itemData);
                btnBuyMoney.onClick.AddListener(() =>
                {
                    if (debugLog) Debug.Log("shopButtonMoney");
                    shopButtonMoney.Buy();
                    Refresh();
                });
            }
            ShopButton shopButtonWatch = btnBuyWatch.GetComponent<ShopButton>();
            if (shopButtonWatch)
            {
                shopButtonWatch.SetData(itemData);
                btnBuyWatch.onClick.AddListener(() =>
                {
                    if (debugLog) Debug.Log("shopButtonWatch");
                    shopButtonWatch.Buy();
                    Refresh();
                });
            }
        }

        void SetActive(GameObject obj, bool active)
        {
            if (obj != null) obj.SetActive(active);
        }

        private void Awake()
        {
            // Инициализация основного
            InitSelectable(selectable);
            // Инициализация дополнительных
            if (otherSelectable != null && otherSelectable.Length > 0)
            {
                for (int i = 0; i < otherSelectable.Length; i++)
                {
                    if (otherSelectable[i] != null) InitSelectable(otherSelectable[i], true);
                }
            }
            // Установка при загрузке
            if (setOnLoad) events.onSet.Invoke(savedID);
        }

        // Инициализация переключаемого объекта
        void InitSelectable(GameObject selectable, bool onlySet = false)
        {
            IShopSelectable iss = selectable.GetComponent<IShopSelectable>();
            if (iss != null)
            {
                if (onlySet == false)
                {
                    //if (events.onNext == null || events.onNext.GetPersistentEventCount() == 0)
                    events.onNext.AddListener(iss.Next);
                    //if (events.onPrevious == null || events.onPrevious.GetPersistentEventCount() == 0)
                    events.onPrevious.AddListener(iss.Previous);
                }
                //if (events.onSet == null || events.onSet.GetPersistentEventCount() == 0)
                events.onSet.AddListener(iss.Set);
                //Debug.Log(string.Format("next={0} prev={1} set={2}", events.onNext.GetPersistentEventCount(), events.onPrevious.GetPersistentEventCount(), events.onSet.GetPersistentEventCount()));
            }
        }

        private void OnEnable()
        {
            // Загружаем текущее значение
            currentID = savedID;
            // Обновляем
            Refresh();
            // Подписываемся на успешную покупку
            AdsShop.instance.onSuccessOperation += Refresh;
        }

        void OnDisable()
        {
            // Отписываемся на успешную покупку
            AdsShop.instance.onSuccessOperation -= Refresh;
        }

        // Обновить
        void Refresh()
        {
            //currentID = savedID;
            Shop.ShopItemData itemData = Shop.GetShopItemData(itemName);
            // Инициализация кнопок
            InitButton(itemData);
        }

        string GetCostString(Shop.ShopItemData itemData)
        {
            string result = "";
            if (itemData.bought == false)
            {
                switch (itemData.payType)
                {
                    case Shop.PayType.money:
                        {
                            if (itemData.cost > 0) result = string.Format("{0}", itemData.cost);
                        }
                        break;
                    case Shop.PayType.watch:
                        {
                            if (/*Shop.noAds*/ IAPNoAds.noAds == false && Shop.GetCountWatch(itemData.name) < itemData.cost) result = string.Format("{0}/{1}", Shop.GetCountWatch(itemData.name), itemData.cost);
                        }
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        #region IShopSelectable

        // Текущее значение
        public int currentID { get { return iss.currentID; } set { iss.currentID = value; } }

        // Следующий
        public void Next()
        {
            if (debugLog) Debug.Log("Next");
            if (events.onNext != null) events.onNext.Invoke();
            Refresh();
        }

        // Предыдущий
        public void Previous()
        {
            if (debugLog) Debug.Log("Previous");
            if (events.onPrevious != null) events.onPrevious.Invoke();
            Refresh();
        }

        // Установить значение
        public void Set(int id)
        {
            if (debugLog) Debug.Log("Set" + id);
            savedID = currentID;
            if (events.onSet != null) events.onSet.Invoke(id);

            Refresh();
        }

        #endregion
    }
}