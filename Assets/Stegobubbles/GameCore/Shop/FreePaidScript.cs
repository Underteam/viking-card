﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.ShopSystem
{
    public class FreePaidScript : MonoBehaviour
    {
        public Shop.TypeApp typeOff = Shop.TypeApp.paid;

        void Awake()
        {
            if (Shop.instance.typeApp == typeOff)
            {
                gameObject.SetActive(false);
            }
        }
    }
}