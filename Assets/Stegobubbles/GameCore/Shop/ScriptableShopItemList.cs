﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.ShopSystem
{
    [CreateAssetMenu(fileName = "ItemList New", menuName = "Shop/ItemList")]
    public class ScriptableShopItemList : ScriptableObject
    {
        public List<ShopItem> data;
    }
}