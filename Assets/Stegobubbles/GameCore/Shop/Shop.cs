﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//#if SGB_Shop

namespace GameCore.ShopSystem
{
    public class Shop : MonoBehaviour
    {
        public enum TypeApp { free, paid }
        public TypeApp typeApp = TypeApp.free;

        [Header("Debug Money")]
        public bool isDebugMoney = false;
        public int countDebugMoney = 1000;

        private void Awake()
        {
            if (Debug.isDebugBuild && isDebugMoney && money < countDebugMoney) money += countDebugMoney;
        }

        #region MoneyTexts

        public static List<Text> textsMoney = new List<Text>();

        public static void RegisterMoneyText(Text moneyText)
        {
            if (textsMoney == null) textsMoney = new List<Text>();
            if (textsMoney.Contains(moneyText) == false)
            {
                textsMoney.Add(moneyText);
                UpdateMoney();
            }
        }

        #endregion

        public List<string> categories;

        public int GetIdCategory(string name) { for (int i = 0; i < categories.Count; i++) { if (categories[i].Equals(name)) return i; } return -1; }
        public string GetNameCategory(int id) { return categories[id]; }

        // Добавляем деньги
        public static void AddMoney(int count = 100, bool animation = false)
        {
            if (animation)
            {
                int start = money;
                int end = start + count;
                LeanTween.value(instance.gameObject, (float value) => { money = (int)value; UpdateMoney(); }, start, end, 1);
            }
            else
            {
                money += count;
            }
            UpdateMoney();
        }

        // Обновляем все деньги
        public static void UpdateMoney()
        {
            string moneyString = money.ToString();
            for (int i = 0; i < textsMoney.Count; i++)
            {
                if (textsMoney[i] != null) textsMoney[i].text = moneyString;
            }
        }

        // Обновление магазина
        public void Refresh()
        {
            // Покупка всех предметов
            if (typeApp == TypeApp.paid)
            {
                for (int i = 0; i < shopData.Count; i++)
                {
                    shopData[i].cost = 0;
                    shopData[i].bought = true;
                }
            }
            // Рекламные поблажки (предметы за рекламу становятся бесплатными)
            if (IAPNoAds.noAds)
            {
                for (int i = 0; i < shopData.Count; i++)
                {
                    if (shopData[i].payType == PayType.watch)
                    {
                        shopData[i].cost = 0;
                        shopData[i].bought = true;
                    }
                }
            }
            // Обновляем деньги
            UpdateMoney();
            // Обновляем все карточки в магазине
            RefreshAllShopItems();
        }

        // Проверка и покупка (если хватает денег)
        public static bool CheckAndBuy(string name, int cost)
        {

            if (CanUse(name) == false && money >= cost)
            {
                if (Buy(name))
                {
                    AddMoney(-cost, true);
                    SetBool(name, true);
                }
            }
            return false;
        }

        public static bool Buy(string itemName)
        {
            ShopItemData itemData = GetShopItemData(itemName);
            //Debug.Log("Shop itemData = null is " + (itemData == null));
            if (itemData == null) return false;
            switch (itemData.payType)
            {
                case PayType.money:
                    {
                        //if (CanUse(itemName) == false)
                        {
                            SetBool(itemName, true);
                            // Эффекты
                            instance.BuyItemEffect();
                            return true;
                        }
                    }
                    break;
                case PayType.watch:
                    {
                        if (itemData.cost > 0)
                        {
                            int currentCount = GetCountWatch(itemName);
                            if (currentCount >= itemData.cost)
                            {
                                SetBool(itemName, true);
                            }
                            else
                            {
                                SetCountWatch(itemName, GetCountWatch(itemName) + 1);
                            }
                            return true;
                        }
                    }
                    break;
                default:
                    break;
            }



            return false;
        }

        // "Визуальное" отображение покупки
        public void BuyItemEffect()
        {
            // Меняется сумма оставшихся денег

            // Звук покупки

        }


        #region Static

        private static Shop _instance;
        public static Shop instance { get { if (_instance == null) _instance = FindObjectOfType<Shop>(); return _instance; } }

        // Деньги игрока
        public static int money { get { return GetInt("money", 0); } set { SetInt("money", value); } }

        // Наличие отключения рекламы
        //public static bool noAds { get { return PlayerPrefs.GetInt("noAds", 0) == 1 ? true : false; } set { PlayerPrefs.SetInt("noAds", value ? 1 : 0); PlayerPrefs.Save(); } }

        // Получение количества просмотров выбранного предмета
        public static int GetCountWatch(int id) { return PlayerPrefs.GetInt("watch" + id, 0); }
        // Запись количества просмотров выбранного предмета
        public static void SetCountWatch(int id, int count) { PlayerPrefs.SetInt("watch" + id, count); PlayerPrefs.Save(); }

        // Получение количества просмотров выбранного предмета
        public static int GetCountWatch(string name) { return PlayerPrefs.GetInt("watch_" + name, 0); }
        // Запись количества просмотров выбранного предмета
        public static void SetCountWatch(string name, int count) { PlayerPrefs.SetInt("watch_" + name, count); PlayerPrefs.Save(); }

        #region Selected Item

        // Выбран ли предмет
        public static bool isSelected(string category, int id)
        {
            //if (category.Equals("Cats") && id == selectedCat) return true;
            //else if (category.Equals("Colors") && id == selectedColor) return true;
            if (GetInt("selected" + category, 1) == id) return true;
            return false;
        }

        // Выбор предмета
        public static void SelectItem(string category, int id)
        {
            //if (category.Equals("Cats") && id != selectedCat) selectedCat = id;
            //else if (category.Equals("Colors") && id != selectedColor) selectedColor = id;
            SetInt("selected" + category, id);
        }

        // Выбранный предмет из категории
        public static int GetSelected(string category) { return GetInt("selected" + category, 1); }

        #endregion

        #region Last Selected Item

        //// Выбран ли предмет
        //public static bool GetLastSelected(string category)
        //{
        //    //if (category.Equals("Cats") && id == selectedCat) return true;
        //    //else if (category.Equals("Colors") && id == selectedColor) return true;
        //    if (GetInt("selected" + category, 1) == id) return true;
        //    return false;
        //}

        #endregion

        // Куплен ли предмет (можно ли его использовать)
        public static bool CanUse(string name)
        {
            return GetBool(name);
        }

        #endregion

        #region ShopItem

        public void BuyItem(string itemName)
        {
            ShopItemData itemData = GetShopItemData(itemName);
            if (itemData != null)
            {
                switch (itemData.payType)
                {
                    case PayType.money:
                        {
                            if (itemData.cost > 0)
                            {

                                CheckAndBuy(itemData.name, itemData.cost);
                            }
                            else
                            {
                                AdsShop.instance.itemManager.AddItem(itemName);
                                //AdsShop.instance.AdsRewardItem(itemName);
                                //Buy(itemName);
                            }
                        }
                        break;
                    case PayType.watch:
                        {
                            AdsShop.instance.AdsRewardItem(itemName);
                        }
                        break;
                    default:
                        break;
                }
            }
            else
            {
                Debug.LogError(string.Format("Предмет {0} отсутствует в каталоге!", itemName));
            }
        }

        public void AddItem(string itemName)
        {

        }


        #endregion



        #region Buttons

        public List<ShopButton> buttons;
        private string lastAdsButton;

        public void RegisterButton(ShopButton btn)
        {
            if (buttons.Contains(btn) == false) buttons.Add(btn);
        }

        public void ButtonActivate(string buttonName)
        {
            lastAdsButton = buttonName;
        }

        public void SetAdsButtonActive(string buttonName = "")
        {
            if (string.IsNullOrEmpty(buttonName))
            {
                buttonName = lastAdsButton;
            }
            for (int i = 0; i < buttons.Count; i++)
            {
                if (buttons[i].itemName.Equals(buttonName))
                {
                    buttons[i].OnDone.Invoke();
                    lastAdsButton = "";
                }
            }
            lastAdsButton = "";
        }

        public virtual bool CancelItem(string buttonName)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                if (buttons[i].itemName.Equals(buttonName))
                {
                    buttons[i].OnCancel.Invoke();
                    return true;
                    //break;
                }
            }
            return false;
        }

        #endregion

        #region ShopItemData

        // Список возможных предметов
        public List<ShopItemData> shopData = new List<ShopItemData>();

        public enum PayType { none, money, watch }

        [System.Serializable]
        public class ShopItemData
        {
            public string name = "Item";
            public int idCategory = 0;
            public PayType payType = PayType.money;
            public int cost = 0;
            public bool bought { get { return CanUse(name); } set { CheckAndBuy(name, cost); } }
            public Object data;

            //int countWatch = GetCountWatch(id);
            //bool isBought = countWatch >= costs[id];

            public bool check(bool useMoney = false)
            {
                switch (payType)
                {
                    case PayType.money:
                        {
                            if (Shop.money >= cost)
                            {
                                if (useMoney) Shop.AddMoney(-cost);
                                return true;
                            }
                        }
                        break;
                    case PayType.watch:
                        {
                            return true;
                        }
                        break;
                    default:
                        break;
                }

                return false;
            }
        }

        // Список карточек предметов
        private List<ShopItem> listShopItems = new List<ShopItem>();

        // Регистрация карточки предмета
        public static void RegisterItem(ShopItem item)
        {
            if (!instance.listShopItems.Contains(item)) instance.listShopItems.Add(item);
        }

        // Обновление всех карточек предметов
        public static void RefreshAllShopItems()
        {
            //Debug.Log("Shop.RefreshAllShopItems");
            for (int i = 0; i < instance.listShopItems.Count; i++)
            {
                instance.listShopItems[i].Init();
            }
        }

        // Получение данных о предмете по имени
        public static ShopItemData GetShopItemData(string nameItem)
        {
            if (instance.shopData == null || instance.shopData.Count == 0)
            {
                Debug.LogError("Магазин пуст!");
            }
            for (int i = 0; i < instance.shopData.Count; i++)
            {
                if (instance.shopData[i].name.Equals(nameItem)) return instance.shopData[i];
            }
            return null;
        }

        // Получение данных о предмете по имени
        public static ShopItemData GetShopItemData(string nameCategory, int id)
        {
            if (instance.shopData == null || instance.shopData.Count == 0)
            {
                Debug.LogError("Магазин пуст!");
            }
            List<ShopItemData> categoryItems = GetShopItemDataByCategory(nameCategory);
            //Debug.Log(string.Format("GetShopItemData {0}", id));
            if (id < categoryItems.Count) return categoryItems[id];
            return null;
        }

        public static List<ShopItemData> GetShopItemDataByCategory(string nameCategory)
        {
            List<ShopItemData> categoryItems = new List<ShopItemData>();
            int idCategory = instance.GetIdCategory(nameCategory);
            for (int i = 0; i < instance.shopData.Count; i++)
            {
                if (instance.shopData[i].idCategory == idCategory) categoryItems.Add(instance.shopData[i]);
            }
            return categoryItems;
        }

        #endregion

        #region Prefs

        public static int GetInt(string name, int defaultValue = 0)
        {
            return PlayerPrefs.GetInt(name, defaultValue);
        }
        public static void SetInt(string name, int value)
        {
            PlayerPrefs.SetInt(name, value);
            PlayerPrefs.Save();
        }

        public static bool GetBool(string name, bool defaultValue = false)
        {
            return PlayerPrefs.GetInt(name, defaultValue ? 1 : 0) == 1 ? true : false;
        }
        public static void SetBool(string name, bool value)
        {
            PlayerPrefs.SetInt(name, value ? 1 : 0);
            PlayerPrefs.Save();
        }

        #endregion

        #region Debug

#if UNITY_EDITOR

        [EditorButton]
        void DebugAddMoney(int count)
        {
            AddMoney(count, true);
        }

        [EditorButton]
        public void ClearData()
        {
            for (int i = 0; i < instance.shopData.Count; i++)
            {
                SetBool(instance.shopData[i].name, false);
                //SetBool(Shop.GetShopItemData(instance.GetNameCategory(instance.shopData[i].idCategory)).name, false); // ????
            }
        }

#endif

        #endregion
    }
}

//#endif