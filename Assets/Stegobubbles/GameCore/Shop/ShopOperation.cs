﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.ShopSystem
{
    public class ShopOperation : MonoBehaviour
    {
        //public int cost = 0;
        //public int Cost { get { return cost; } set { cost = value; if (costText) costText.text = cost.ToString(); } }
        public bool useMoney = false;

        public Text costText;
        public enum TypeCheck { none, update, coroutine }
        public TypeCheck typeCheck = TypeCheck.none;
        public float coroutineTime = 1;

        public UnityEngine.Events.UnityEvent onTrue;
        public UnityEngine.Events.UnityEvent onFalse;

        [HideInInspector]
        public Shop.ShopItemData shopItemData;

        private void Awake()
        {
            //Cost = cost;
            if (typeCheck == TypeCheck.coroutine) StartCheckCoroutine();
        }

        private void Update()
        {
            if (typeCheck == TypeCheck.update)
            {
                Check();
            }
        }

        public void StartCheckCoroutine()
        {
            StartCoroutine(Coroutine());
        }

        private IEnumerator Coroutine()
        {
            WaitForSeconds wfs = new WaitForSeconds(coroutineTime);
            while(true)
            {
                if (shopItemData.check())
                {
                    onTrue.Invoke();
                }
                else
                {
                    onFalse.Invoke();
                }
                yield return wfs;
            }
            
        }

        public void Check()
        {
            bool result = shopItemData.check(useMoney);
            if (result)
            {
                onTrue.Invoke();
            }
            else
            {
                onFalse.Invoke();
            }
        }
    }
}