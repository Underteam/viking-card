﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.ShopSystem
{
    public class MoneyText : MonoBehaviour
    {
        public Text text;

        void Awake()
        {
            if (text == null) text = GetComponent<Text>();

            if (text != null)
            {
                Shop.RegisterMoneyText(text);
            }
        }
    }
}