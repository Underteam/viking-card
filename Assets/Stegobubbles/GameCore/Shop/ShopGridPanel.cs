﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.ShopSystem
{
    public class ShopGridPanel : MonoBehaviour
    {

        public GameObject itemPrefab;

        public bool loadShopData = false;
        public int startShopDataID = 1;
        public bool useFakeItems = false;
        public int countFakeItems = 0;

        public List<ShopItem> spawnedItems;

        public GameObject previewItem;
        public Text costText;

        public RectTransform content;


        private void Awake()
        {
            InitItems();
        }

        void InitItems()
        {
            int countItems = Shop.instance.shopData.Count;
            if (useFakeItems) countItems += countFakeItems;
            Shop.ShopItemData shopData;
            for (int i = startShopDataID; i < countItems; i++)
            {
                GameObject newGO = Instantiate(itemPrefab, content);
                newGO.SetActive(true);
                ShopItem item = newGO.GetComponent<ShopItem>();

                item.id = i;
                if (i < Shop.instance.shopData.Count)
                {
                    shopData = Shop.instance.shopData[i];
                }
                else
                {
                    shopData = Shop.instance.shopData[0];
                    //item.id = 0;
                }

                item.name = string.Format("{0} ({1})", itemPrefab.name, i);
                item.itemName = shopData.name;
                item.costText = costText;
                if (item.shopOperation) item.shopOperation.costText = costText;

                item.SmartInit();
                spawnedItems.Add(item);
            }
        }
    }
}