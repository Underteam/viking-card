﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.ShopSystem
{
    public class ShopButton : MonoBehaviour
    {

        public string itemName;

        public bool actionOnBuy = false;
        public UnityEngine.Events.UnityEvent action;
        public UnityEngine.Events.UnityEvent OnDone;
        public UnityEngine.Events.UnityEvent OnCancel;

        [HideInInspector]
        public Shop.ShopItemData shopItemData = null;
        private bool useShopItemData = false;

        public void SetData(Shop.ShopItemData shopItemData)
        {
            this.shopItemData = shopItemData;
            useShopItemData = true;
        }

        private void Awake()
        {
            Shop.instance.RegisterButton(this);
        }

        public void Activate()
        {
            Shop.instance.ButtonActivate(itemName);
            action.Invoke();
        }

        public void Buy()
        {
            Debug.Log("ShopButton Buy " + itemName);
            Shop.instance.BuyItem(useShopItemData ? shopItemData.name : itemName);
            if (actionOnBuy)
            {
                action.Invoke();
                OnDone.Invoke();
            }
        }
    }
}