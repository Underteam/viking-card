﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.ShopSystem
{
    public class ShopItem : MonoBehaviour
    {
        public GameObject cost;
        public GameObject locker;
        public GameObject selecter;
        public Text costText;
        public Text selectText;
        public int id = -1;
        public string category;
        public bool itemNameAndID = false;
        public ShopOperation shopOperation;

        public enum BuyMode { select, buyButton }
        public BuyMode buyMode = BuyMode.select;

        public Object itemPreview;
        public Object itemRenderer;

        public bool initAtAwake = false;
        public bool useSmartInit = false;
        public string itemName = "Item";
        public string itemNameID { get { return Shop.GetShopItemData(category, id).name; } }
        private Shop.ShopItemData _shopItemData;
        private Shop.ShopItemData shopItemData { get { if (_shopItemData == null) _shopItemData = Shop.GetShopItemData(itemName); if (_shopItemData == null) _shopItemData = Shop.GetShopItemData(category, id); return _shopItemData; } set { _shopItemData = value; } }

        public bool useTwoButtons = false;
        public GameObject buttonBuy;
        public GameObject buttonSelect;

        private void OnEnable()
        {
            if (initAtAwake)
            {
                Shop.RegisterItem(this);
                if (useSmartInit) SmartInit();
                //ChangeID(0);
            }
        }

        public void SmartInit()
        {
            Init(Shop.isSelected(category, id));
        }

        public void ChangeID(int id)
        {
            shopItemData = null;
            this.id = id;
            Init();
        }

        public void Init(bool select = false)
        {
            bool bought = shopItemData.bought;

            if (bought == false && shopItemData.cost == 0)
            {
                Buy();
                bought = shopItemData.bought;
            }
            // Подгрузка изображения предмета
            if (shopItemData.data != null)
            {
                if (itemRenderer != null)
                {
                    //Debug.Log(shopItemData.data.name);
                    SetSprite(itemRenderer, shopItemData.data as Sprite);
                    //if (itemRenderer.GetType().Equals(typeof(SpriteRenderer))) { (itemRenderer as SpriteRenderer).sprite = shopItemData.data as Sprite; }
                    //if (itemRenderer.GetType().Equals(typeof(Image))) { (itemRenderer as Image).sprite = shopItemData.data as Sprite; }
                }
            }

            //if (buyMode == BuyMode.select)
            //{
            //    // И он не куплен
            //    if (bought == false)
            //    {
            //        // Назначаем цвет силуэту
            //        SetColor(itemRenderer, Color.black);
            //        // Назначаем цену
            //        costText.text = shopItemData.cost.ToString();

            //        // Если элемент выбирается
            //        if (select || shopItemData.cost == 0)
            //        {
            //            if (buyMode == BuyMode.select)
            //            {
            //                Debug.Log("Покупка " + (itemNameAndID ? itemNameID : itemName));
            //                // Покупаем (если можем)
            //                //Shop.CheckAndBuy(itemNameAndID ? itemNameID : itemName, shopItemData.cost);
            //                Buy();
            //            }
            //        }

            //        if (locker) locker.SetActive(true);
            //        if (cost) cost.SetActive(true);
            //        if (selectText) selectText.gameObject.SetActive(false);

            //        if (shopOperation)
            //        {
            //            shopOperation.Cost = shopItemData.cost;
            //            //shopOperation.useMoney = true;
            //        }
            //    }
            //    // Если предмет куплен
            //    if (bought == true)
            //    {
            //        // Назначаем цвет силуэту
            //        SetColor(itemRenderer, Color.white);
            //        // И мы выбираем его
            //        if (select)
            //        {
            //            // То выбираем предмет
            //            Shop.SelectItem(category, id);
            //            //Shop.RefreshAllShopItems();
            //            Debug.Log(string.Format("Selected {0} {1}", category, id));

            //        }
            //        // Задаем цену
            //        //costTest.text = Shop.isSelected(category, id) ? "V" : "X";
            //        costText.text = "";
            //        // Ставим/убираем рамочку выбора
            //        selecter.SetActive(Shop.isSelected(category, id));

            //        if (locker) locker.SetActive(false);
            //        if (cost) cost.SetActive(false);
            //        if (selectText) selectText.gameObject.SetActive(true);

            //        if (shopOperation)
            //        {
            //            shopOperation.Cost = 0;
            //            //shopOperation.useMoney = false;
            //        }
            //    }
            //}
            if (buyMode == BuyMode.buyButton)
            {
                bool isComingSoon = id > Shop.GetShopItemDataByCategory(category).Count - 1;
                bool isSelected = Shop.isSelected(category, id);
                // Назначаем цвет силуэту
                SetColor(itemRenderer, bought ? Color.white : Color.black, true);
                // Ставим/убираем рамочку выбора
                selecter.SetActive(Shop.isSelected(category, id));

                if (select)
                {
                    // То выбираем предмет
                    Shop.SelectItem(category, id);
                    isSelected = true;

                    SetSprite(itemPreview, shopItemData.data as Sprite);
                    SetColor(itemPreview, bought ? Color.white : Color.black, true);
                }

                if (locker) { locker.SetActive(isComingSoon); }
                //if (locker) locker.SetActive(true);
                //if (cost) cost.SetActive(true);
                
                if (isSelected)
                {
                    if (shopOperation)
                    {
                        shopOperation.shopItemData = shopItemData;

                        Button btnBuy = buttonBuy.GetComponent<Button>();
                        if (btnBuy)
                        {
                            btnBuy.interactable = shopItemData.check();
                            btnBuy.onClick.RemoveAllListeners();
                            btnBuy.onClick.AddListener(Buy);
                        }
                        //shopOperation.useMoney = true;
                    }
                    if (bought)
                    {
                        if (useTwoButtons)
                        {
                            buttonBuy.SetActive(false);
                            buttonSelect.SetActive(true);

                            buttonSelect.GetComponent<Button>().interactable = true;
                            //buttonSelect.GetComponent<KnifeButtonSelect>().knifeID = id;
                        }
                    }
                    else
                    {
                        if (selectText)
                        {
                            selectText.gameObject.SetActive(true);
                            selectText.text = isComingSoon ? "COMING SOON" : "SELECT";
                        }
                        
                        if (useTwoButtons)
                        {
                            if (isComingSoon)
                            {
                                buttonBuy.SetActive(false);
                                buttonSelect.SetActive(true);
                            }
                            else
                            {
                                buttonBuy.SetActive(true);
                                buttonSelect.SetActive(false);
                            }

                            buttonSelect.GetComponent<Button>().interactable = false;
                        }
                    }

                }
            }
        }

        void SetSprite(Object objectRenderer, Sprite sprite)
        {
            if (objectRenderer != null)
            {
                // SpriteRenderer
                SpriteRenderer spriteRenderer = (objectRenderer as GameObject).GetComponent<SpriteRenderer>();
                if (spriteRenderer) spriteRenderer.sprite = sprite;
                // Image
                Image image = (objectRenderer as GameObject).GetComponent<Image>();
                if (image) image.sprite = sprite;
            }
        }

        void SetColor(Object objectRenderer, Color color, bool animation = false)
        {
            if (objectRenderer != null)
            {
                // SpriteRenderer
                SpriteRenderer spriteRenderer = (objectRenderer as GameObject).GetComponent<SpriteRenderer>();
                if (spriteRenderer)
                {
                    if (animation && spriteRenderer.color.Equals(Color.black))
                    {
                        LeanTween.color(gameObject, color, 1);
                    }
                    else
                    {
                        spriteRenderer.color = color;
                    }
                }
                // Image
                Image image = (objectRenderer as GameObject).GetComponent<Image>();
                if (image)
                {
                    image.color = color;
                }
            }
        }

        public void Buy()
        {
            Shop.CheckAndBuy(itemNameAndID ? itemNameID : itemName, shopItemData.cost);
            Init(true);
        }

        public void Select()
        {
            Init(true);
            Shop.RefreshAllShopItems();
        }

    }
}