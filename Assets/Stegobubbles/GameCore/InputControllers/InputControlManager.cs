﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputControlManager : MonoBehaviour {

    [System.Serializable]
	public class InputControl
    {
        public string name;
        public GameObject gameObject;
        public UnityEngine.Events.UnityEvent onActivate;

        public void Activate()
        {
            //Debug.Log(name + " activated");
            if (gameObject) gameObject.SetActive(true);
            if (onActivate != null) onActivate.Invoke();
        }
        
        public void DeActivate()
        {
            if (gameObject) gameObject.SetActive(false);
        }
    }

    public List<InputControl> list;

    public InputControl currentControl;

    public TogglePanel togglePanel;

    public int inputControls { get { return PlayerPrefs.GetInt("inputControls", 0); } set { PlayerPrefs.SetInt("inputControls", value); PlayerPrefs.Save(); } }

    private void Awake()
    {
        if (togglePanel) togglePanel.onSelect += Activate;
        
    }

    private void Start()
    {
        if (togglePanel) togglePanel.Set(inputControls);
    }

    private void OnDestroy()
    {
        if (togglePanel) togglePanel.onSelect -= Activate;
    }

    public void Activate(int id)
    {
        inputControls = id;
        if (currentControl != null)
        {
            currentControl.DeActivate();
        }
        currentControl = list[id];
        currentControl.Activate();
    }
}
