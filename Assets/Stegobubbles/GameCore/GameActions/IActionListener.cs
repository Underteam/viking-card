﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.GameActions
{
    public interface IActionListener
    {
        void GetMessageActive(object[] args);
    }
}