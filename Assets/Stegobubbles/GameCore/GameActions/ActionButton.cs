﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.GameActions
{
    public class ActionButton : MonoBehaviour
    {

        public GameObject listener;
        public string method;
        public string key;
        public bool clearAfterUse;

        public void Init(GameObject listener, string key)
        {
            this.listener = listener;
            this.key = key;
        }

        public void Clear()
        {
            this.listener = null;
            this.key = "";
        }

        public void Activate()
        {
            if (listener != null && key != "")
            {
                listener.SendMessage(method, key);
                if (clearAfterUse)
                {
                    listener = null;
                    key = "";
                }
            }
        }
    }
}