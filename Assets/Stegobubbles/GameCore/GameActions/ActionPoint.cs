﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.GameActions
{
    public class ActionPoint : MonoBehaviour
    {

        public string Tag = "Player";
        public string Key = "";

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(Tag))
            {
                SendMessageActive(other.gameObject, true);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(Tag))
            {
                SendMessageActive(other.gameObject, false);
            }
        }

        void SendMessageActive(GameObject obj, bool active)
        {
            object[] data = new object[] { Key, gameObject, active };

            IActionListener[] listeners = obj.GetComponentsInChildren<IActionListener>(true);
            for (int i = 0; i < listeners.Length; i++)
            {
                listeners[i].GetMessageActive(data);
            }
            //ActionCounter ac = obj.GetComponentInChildren<ActionCounter>();
            //if (ac)
            //{
            //    ac.GetMessageActive(new object[] { Key, gameObject, active });
            //    Debug.Log("ActionPoint SendMessageActive Component! " + active);
            //    //return;
            //}
            //Debug.Log("ActionPoint SendMessageActive " + active);
            //obj.SendMessageUpwards("GetMessageActive", new object[] { Key, gameObject, active });
        }
    }
}