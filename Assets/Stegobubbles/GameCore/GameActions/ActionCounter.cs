﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.GameActions
{
    public class ActionCounter : MonoBehaviour, IActionListener
    {

        [System.Serializable]
        public class GameAction
        {
            public string key;
            public bool use = false;
            public int count = 0;
            public GameObject button;
            public bool offObject;
            public bool offObjectParent;
            public bool invokeMethod;
            public bool autoUse = false;
            public string method;

            public System.Action onChangeCount;

            private GameObject lastObj;

            public UnityEngine.Events.UnityEvent onAction;

            public void SetActive(GameObject counter, GameObject go, bool active)
            {
                //Debug.Log(string.Format("GameAction {0} SetActive {1}", key, active));
                if (active)
                {
                    lastObj = go;
                    if (button)
                    {
                        button.GetComponent<ActionButton>().Init(counter, key);
                        button.SetActive(true);
                    }
                    if (autoUse)
                    {
                        Apply();
                    }
                }
                else
                {
                    lastObj = null;
                    if (button)
                    {
                        button.GetComponent<ActionButton>().Clear();
                        button.SetActive(false);
                    }
                }
                //if (lastObj != null && lastObj.Equals(go))
                //{
                //    Apply();
                //}
                //else
                //{
                //    lastObj = go;
                //    button.GetComponent<ActionButton>().Init(counter, key);
                //}
            }

            public void Apply()
            {
                count++;
                if (onChangeCount != null) onChangeCount.Invoke();
                onAction.Invoke();
                if (offObject)
                {
                    lastObj.SetActive(false);
                }
                if (offObjectParent)
                {
                    lastObj.transform.parent.gameObject.SetActive(false);
                }
                if (invokeMethod)
                {
                    lastObj.transform.parent.SendMessage(method);
                }

                if (button)
                {
                    button.GetComponent<ActionButton>().Clear();
                    button.SetActive(false);
                }
                lastObj = null;
            }

            public void Reset()
            {
                count = 0;
            }
        }

        public List<GameAction> gameActions;

        public GameAction GetGameAction(string key)
        {
            for (int i = 0; i < gameActions.Count; i++)
            {
                if (gameActions[i].key.Equals(key)) return gameActions[i];
            }
            return null;
        }

        public void GetMessageActive(object[] args)
        {
            //Debug.Log("ActionCounter GetMessageActive!");
            if (args.Length > 1)
            {
                string key = (string)args[0];
                GameObject other = (GameObject)args[1];
                bool active = (bool)args[2];

                GameAction gameAction = GetGameAction(key);
                //Debug.Log("ActionCounter " + gameAction);
                if (gameAction != null)
                {
                    gameAction.SetActive(gameObject, other, active);
                }
            }
        }

        // Сбросить по ключу
        public void ResetByKey(string key)
        {
            GameAction gameAction = GetGameAction(key);
            if (gameAction != null)
            {
                gameAction.Reset();
            }
        }

        // Сбросить все
        public void ResetAll(bool onlyUsed = true)
        {
            for (int i = 0; i < gameActions.Count; i++)
            {
                if (onlyUsed && gameActions[i].use)
                {
                    gameActions[i].Reset();
                }
                else
                {
                    gameActions[i].Reset();
                }
            }
        }

        // Применить по ключу
        public void Apply(string key)
        {
            GameAction gameAction = GetGameAction(key);
            if (gameAction != null)
            {
                gameAction.Apply();
            }
        }
    }
}