﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.Tutorial
{
    public class TutorialManager : MonoBehaviour
    {

        private static TutorialManager _instance;
        public static TutorialManager instance { get { if (_instance == null) _instance = FindObjectOfType<TutorialManager>(); return _instance; } }

        public TutorialFinger finger;

        public bool finished { get { return PlayerPrefs.GetInt("tutorialFinished", 0) == 1; } set { PlayerPrefs.SetInt("tutorialFinished", value ? 1 : 0); PlayerPrefs.Save(); } }

        public int currentStage { get { return PlayerPrefs.GetInt("tutorialStage", 0); } set { PlayerPrefs.SetInt("tutorialStage", value); PlayerPrefs.Save(); } }

        public Canvas canvas;

        [System.Serializable]
        public class State
        {
            public string name;
            public enum Type { None, Tap, Move, MoveDrag }
            public Type type;
            public Transform parent;
            public Vector3 offset;
            public List<GameObject> objects;
            public Vector3[] points;
            public List<Image> notInteractive;

            public Vector3 GetPoint()
            {
                //if (is3D) return GetPointOn3DObject(objects[0].transform.position) + offset;
                return GetScreenPoint(objects[0].transform.position + offset);
            }

            public Vector3[] GetPoints()
            {
                List<Vector3> result = new List<Vector3>();
                if (objects.Count > 0)
                {
                    for (int i = 0; i < objects.Count; i++)
                    {
                        if (objects[i] != null) result.Add(objects[i].transform.position + offset);
                    }
                }
                else
                {
                    result.AddRange(points);
                }
                return result.ToArray();
            }

            public void SetInteractive(bool active)
            {
                for (int i = 0; i < notInteractive.Count; i++)
                {
                    notInteractive[i].raycastTarget = active;
                }
            }

            public Vector3 GetScreenPoint(Vector3 point)
            {
                Vector3 screen_point = instance.canvas.worldCamera.WorldToScreenPoint(point) / instance.canvas.scaleFactor;
                return new Vector3(screen_point.x, screen_point.y, screen_point.z);
            }
        }

        public List<State> states;

        [EditorButton]
        void SetState(int id)
        {
            // Сохраняем шаг
            currentStage = id;

            if (LeanTween.isTweening(finger.gameObject)) LeanTween.cancel(finger.gameObject);
            // Включаем палец
            finger.gameObject.SetActive(true);
            // Вкладываем палец в нужный объект
            finger.transform.parent = states[id].parent;
            // Совершаем нужное действие
            switch (states[id].type)
            {
                case State.Type.Tap:
                    finger.Tap(states[id].GetPoint(), true);
                    break;
                case State.Type.Move:
                    finger.MoveByPoints(states[id].GetPoints(), true);
                    break;
                case State.Type.MoveDrag:
                    finger.MoveAndTap(states[id].GetPoints(), true);
                    break;
                default:
                    break;
            }
        }

        // Кастомные действия
        void CustomPreAction(int id)
        {
            switch (id)
            {
                case 2: // Ищем пингвина
                    {
                        //states[id].objects.Add(AnimalSpawner.instance.GetLastAnimal().gameObject);
                    }
                    break;
                case 4: // Ищем пингвина
                    {
                        //states[id].objects.Add(AnimalSpawner.instance.GetLastAnimal().gameObject);
                    }
                    break;
                case 5: // Заканчиваем туториал
                    {
                        finger.gameObject.SetActive(false);
                        finished = true;
                    }
                    break;
                default:
                    break;
            }
        }

        // Совершаем переход
        public void Action(int id)
        {
            //Debug.Log("Tutorial Action " + id);
            if (id >= currentStage && id < currentStage + 2)
            {
                // Включаем прошлые выключенные объекты
                if (id - 1 >= 0)
                {
                    states[id - 1].SetInteractive(true);
                }
                // Выключаем текущие объекты
                if (id < states.Count)
                {
                    states[id].SetInteractive(false);
                }
                // Выполняем кастомные действия
                CustomPreAction(id);
                // Меняем состояние туториала
                SetState(id);
            }
            else
            {
                //Debug.Log("Недопустимое действие! " + id);
            }
        }
    }
}