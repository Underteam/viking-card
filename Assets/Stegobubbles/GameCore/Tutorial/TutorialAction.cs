﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.Tutorial
{
    public class TutorialAction : MonoBehaviour
    {
        public int actionID = -1;

        public void Activate()
        {
            TutorialManager.instance.Action(actionID);
        }
    }
}