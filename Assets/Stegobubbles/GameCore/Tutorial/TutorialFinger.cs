﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.Tutorial
{
    public class TutorialFinger : MonoBehaviour
    {

        public Sprite pressed;
        public Sprite unpressed;

        private Image _image;
        private Image image { get { if (_image == null) _image = GetComponent<Image>(); return _image; } set { _image = value; } }

        private RectTransform _rectTransform;
        private RectTransform rectTransform { get { if (_rectTransform == null) _rectTransform = GetComponent<RectTransform>(); return _rectTransform; } set { _rectTransform = value; } }

        public void ChangeSprite(bool active)
        {
            image.sprite = active ? pressed : unpressed;
        }

        public void Tap(Vector3 point, bool loop)
        {
            Vector3 scale = transform.localScale;
            rectTransform.anchoredPosition = point;
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 0);
            LTDescr action = LeanTween.scale(gameObject, scale * 1.2f, 1f).setLoopPingPong().setLoopCount(-1);
            if (loop) action.setLoopPingPong();
        }

        public void MoveByPoints(Vector3[] points, bool loop)
        {
            rectTransform.anchoredPosition = points[0];
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 0);

            for (int i = 0; i < points.Length; i++)
            {
                points[i] = new Vector3(points[i].x, points[i].y, 0);
            }

            LTDescr action = LeanTween.moveLocal(gameObject, points, 1);
            if (loop) action.setLoopPingPong();
        }

        public void MoveAndTap(Vector3[] points, bool loop)
        {
            rectTransform.anchoredPosition = points[0];
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 0);
            ChangeSprite(true);

            for (int i = 0; i < points.Length; i++)
            {
                points[i] = new Vector3(points[i].x, points[i].y, 0);
            }

            LTDescr action = LeanTween.moveLocal(gameObject, points, 1).setDelay(1).setOnComplete(() =>
            {
                ChangeSprite(false);
            });
            if (loop) action.setLoopPingPong();
        }
    }
}