﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameCore
{
    public class StateManager : MonoBehaviour
    {
        public bool useDebugLogs = false;

        private static StateManager _instance;
        public static StateManager instance { get { if (_instance == null) _instance = FindObjectOfType<StateManager>(); return _instance; } }

        #region StateList

        //[System.Serializable]
        //public class States : StringEnum
        //{
        //    public new string[] values { get { return instance.GetStateList(); } }

        //    //public static bool operator ==(State _state, string value) { return (_state.state.Value.Equals(value)); }
        //    //public static bool operator !=(State _state, string value) { return (!_state.state.Value.Equals(value)); }

        //    public string this[string value]
        //    {
        //        get
        //        {
        //            for (int i = 0; i < instance.states.Count; i++)
        //            {
        //                if (instance.states[i].data.name.Equals(value))
        //                {
        //                    return instance.states[i].data.name;
        //                }
        //            }
        //            return null;
        //        }
        //    }

        //    public bool Equals(string value) { return Value.Equals(value); }
        //}

        //private string[] _stateList;
        //public string[] StateList { get { if (_stateList == null) _stateList = GetStateList(); return _stateList; } set { _stateList = value; } }
        //public string[] GetStateList()
        //{
        //    List<string> result = new List<string>();
        //    StateMonoBehaviour[] finded = transform.GetComponentsInChildren<StateMonoBehaviour>();
        //    for (int i = 0; i < finded.Length; i++) { result.Add(finded[i].data.name); }
        //    return result.ToArray();
        //}
        

        #endregion

        [System.Serializable]
        public class State
        {
            public string name;
            [TextArea(1, 5)]
            public string dectription;  
            public States state;
            public UIPanelBase panel;
            public UnityEvent onStart;
            public UnityEvent onComplete;
            public AdsConfig adsConfig;

            [SerializeField, HideInInspector]
            private StateMonoBehaviour behaviour;
        }

        [System.Serializable]
        public class AdsConfig
        {
            public bool showAdsOnComplete = false;
            public bool showAdsByCount = false;
            public int showAdsByCountStep = 5;
            public int showAdsByCountStepCurrent = 0;

            public void Apply()
            {
                // Проигрываем рекламу при завершении состояния
                if (showAdsOnComplete)
                {
                    AdsShop.instance.Ads();
                    //AdsController.Instance().TryToShowVideoBannerSkippable();
                }
                // Реклама по счетчику переходов
                if (showAdsByCount)
                {
                    if ((showAdsByCountStepCurrent + 1) % showAdsByCountStep == 0)
                    {
                        AdsShop.instance.Ads();
                    }
                }
                // Итерация перехода
                showAdsByCountStepCurrent++;
            }
        }

        public States startState = States.none;
        [Header("States")]
        public List<StateMonoBehaviour> states;

        [EditorButton]
        void UpdateStates() { states = new List<StateMonoBehaviour>(transform.GetComponentsInChildren<StateMonoBehaviour>()); }
        [EditorButton]
        void HideAll() { for (int i = 0; i < states.Count; i++) { if (states[i].data.panel) states[i].data.panel.Hide(); } }

        public enum States { none, MainMenu, Pause, Finish, Shop, NewGame, ContinueGame, RestartGame, Game, Resume, FindGame, SelectLevel }

        [HideInInspector] public State currentState;
        [HideInInspector] public State previousState;

        private void Awake()
        {
            if (startState != States.none)
            {
                ChangeState(startState);
            }
        }

        public void ChangeStateNow(States newState)
        {
            for (int i = 0; i < states.Count; i++)
            {
                if (states[i].data.state.Equals(newState))
                {
                    currentState = states[i].data;
                }
            }
        }

        public void ChangeState(States newState)
        {
            previousState = currentState;
            // Выполняем действия для текущего состояния
            if (currentState != null)
            {
                // Применение рекламы
                currentState.adsConfig.Apply();
                // Выполняем необходимые действия
                currentState.onComplete.Invoke();
                // Убираем текущую панель
                if (currentState.panel) currentState.panel.Hide();
            }
            // Переходим в новое состояние
            for (int i = 0; i < states.Count; i++)
            {
                if (states[i].data.state.Equals(newState))
                {
                    if (useDebugLogs) Debug.Log(string.Format("State changed: {0} => {1}", currentState.state, states[i].data.state));
                    currentState = states[i].data;
                    // Убираем текущую панель
                    if (currentState.panel) currentState.panel.Show();
                    // Выполняем необходимые действия на старте
                    states[i].data.onStart.Invoke();
                    break;
                }
            }
        }
    }
}