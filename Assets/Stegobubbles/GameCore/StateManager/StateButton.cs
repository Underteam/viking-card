﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore
{
    public class StateButton : MonoBehaviour
    {
        public StateManager.States state;

        public void Activate()
        {
            StateManager.instance.ChangeState(state);
        }

        public void Set()
        {
            StateManager.instance.ChangeStateNow(state);
        }
    }
}
