﻿using UnityEngine;
using System.Collections;

namespace GameCore.Localization
{
    public class Localization : MonoBehaviour
    {
        public LangCode langCode;
        public TextAsset asset;
    }
}