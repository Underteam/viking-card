﻿using UnityEngine;
using UnityEngine.UI;

namespace GameCore.Localization
{
    [RequireComponent(typeof(Text))]
    public class UILangText : MonoBehaviour
    {
        public string key;
        
        private Text text;

        private void Awake()
        {
            if (text == null) text = GetComponent<Text>();
            SetLocalizedText(Localizer.instance.Language);
        }

        private void Start()
        {
            Localizer.OnChangedLanguage += SetLocalizedText;
        }

        private void OnDestroy()
        {
            Localizer.OnChangedLanguage -= SetLocalizedText;
        }

        private void SetLocalizedText(LangCode code)
        {
            if (!string.IsNullOrEmpty(key))
            {
                text.text = Localizer.instance.GetText(key);
            }
            else
            {
                Debug.LogWarning("Key in the UILangText component on " + gameObject.name + " is null or empty");
            }
        }
    }
}


