﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameCore.Localization
{
    [CustomEditor(typeof(UILangText))]
    public class UILangTextInspector : Editor
    {

        private UILangText _target;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (_target == null) _target = target as UILangText;

            string[] list = Localizer.instance.GetKeys();
            int selectId = GetID(_target.key, list);
            if (selectId == -1) selectId = 0;
            int oldID = selectId;
            selectId = EditorGUILayout.Popup("Key", selectId, list);
            if (oldID != selectId)
            {
                _target.key = list[selectId];
            }
        }

        int GetID(string value, string[] values)
        {
            for (int i = 0; i < values.Length; i++) { if (values[i].Equals(value)) return i; }
            return -1;
        }
    }
}