﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace GameCore.Localization
{
    public enum LangCode
    {
        English,
        Russian
    }

    public class Localizer
    {
#if UNITY_EDITOR

        [UnityEditor.MenuItem("Default", menuItem = "Stegobubbles/Localization/Default (Device)")]
        static void ChangeToDefault()
        {
            instance.SetLang(deviceLang);
        }

        [UnityEditor.MenuItem("English", menuItem = "Stegobubbles/Localization/English")]
        static void ChangeToEnglish()
        {
            instance.SetLang(LangCode.English);
        }

        [UnityEditor.MenuItem("Russian", menuItem = "Stegobubbles/Localization/Russian")]
        static void ChangeToRussian()
        {
            instance.SetLang(LangCode.Russian);
        }

#endif

        #region NEW

        private const string rootPath = "Localization/";
        private const string keyPref = "currentLang";
        private const LangCode defaultLang = LangCode.English;
        //private LangCode langCode;
        //public LangCode Language { get { return langCode; } private set { langCode = value; Debug.Log("!" + langCode); } }
        public LangCode Language { get; private set; }
        public static LangCode deviceLang { get { return ConvertSystemLanguage(Application.systemLanguage); } }

        public static LangCode ConvertSystemLanguage(SystemLanguage selected)
        {
            switch (selected)
            {
                case SystemLanguage.English: return LangCode.English;
                case SystemLanguage.Russian: return LangCode.Russian;
                default: return defaultLang;
            }
        }

        public string[] GetKeys()
        {
            List<string> result = new List<string>();
            foreach (var item in dictionary) { result.Add(item.Key); }
            return result.ToArray();
        }

        #endregion

        public static event Action<LangCode> OnChangedLanguage;
        public static Dictionary<string, string> dictionary = new Dictionary<string, string>();

        private static Localizer _instance;
        public static Localizer instance { get { return _instance ?? (_instance = new Localizer()); } }

        public static bool Inited()
        {
            return instance != null;
        }

        private Localizer()
        {
            //Debug.Log("Localizer inited!");
            // Загружаем ключ текущей локализации
            string currLangString = PlayerPrefs.GetString(keyPref, "default");
            // Если стандартный, то ставим системный язык устройства
            if (currLangString.Equals("default"))
            {
                //Debug.Log("First");
                SetLang(deviceLang);
            }
            // Если не стандартный, конвертируем и загружаем
            else
            {
                LangCode code = (LangCode)Enum.Parse(typeof(LangCode), currLangString);
                //Debug.Log("Second " + code);
                SetLang(code);
            }
        }

        // Устанавливаем язык
        public void SetLang(LangCode code)
        {
            Language = code;
            // Загружаем словарь из TextAsset
            dictionary = ReadFromTextAsset(rootPath, code);
            // Сохраняем в префсы
            PlayerPrefs.SetString(keyPref, code.ToString());
            PlayerPrefs.Save();
            // Оповещаем об изменении языка
            if (OnChangedLanguage != null) OnChangedLanguage(code);
        }

        #region Text

        public string GetText(string key)
        {
            return dictionary.ContainsKey(key) ? dictionary[key] : key;
        }

        #endregion

        #region Sprite

        public Sprite GetSprite(string key)
        {
            Sprite sprite = Resources.Load<Sprite>(rootPath + Language.ToString() + "/" + key);
            if (sprite == null)
            {
                Debug.LogWarning("Sprite " + key + " not found in " + Language.ToString() + " language");
            }
            return sprite;
        }

        #endregion

        #region ReadFromTextAsset

        Dictionary<string, string> ReadFromTextAsset(string path, LangCode lang)
        {
            Localization[] localizations = Resources.LoadAll<Localization>(path);
            for (int i = 0; i < localizations.Length; i++)
            {
                //Debug.Log("Localization " + localizations[i].name + " " + localizations[i].language + " " + language + " " + (localizations[i].language == language));
                if (localizations[i].langCode == lang)
                {
                    return ReadFromTextAsset(localizations[i].asset, lang);
                }
            }
            return null;
        }

        private static Dictionary<string, string> ReadFromTextAsset(TextAsset textAsset, LangCode lang)
        {
            Dictionary<string, string> result;
            ReadFromAsset(textAsset, out result);
            return result;
        }

        private static int ReadFromAsset(TextAsset asset, out Dictionary<string, string> result)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            result = dictionary;

            if (asset == null) return 0;

            string[] lines = asset.text.Split('\n');

            if (lines.Length < 1) return 0;

            for (int i = 0; i < lines.Length; i++)
            {
                string[] cells = lines[i].Split('\t');
                if (cells.Length < 2) continue;
                cells[1] = cells[1].Replace("\\n", "\n");
                //SetString(cells[0], cells[1]);
                dictionary.Add(cells[0], cells[1]);
                //Debug.Log ("Set " + cells [0] + " -> " + cells [1]);
            }

            result = dictionary;
            return 1;
        }

        #endregion

    }
}