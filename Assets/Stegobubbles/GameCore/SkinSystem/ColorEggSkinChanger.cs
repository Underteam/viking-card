﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorEggSkinChanger : MonoBehaviour {

    public int idMaterial = 0;
    public Material mat;
    public Texture2D[] skins;
    public Color testColor;

    [System.Serializable]
    public class ColorScheme
    {
        public string _color1;
        public string _color2;
    }

    public List<ColorScheme> schemes;

    public class iColor
    {
        public string colorString;
        public Color color;
    }
    public int currentSkin = 0;
    public int currentScheme = 0;

    public bool onStart = false; 
    public bool useRandom = false;

    public int GetRandomSkin() { return Random.Range(0, skins.Length); }
    public int GetRandomScheme() { return Random.Range(0, schemes.Count); }

    private Renderer _renderer;
    private new Renderer renderer { get { if (_renderer == null) _renderer = GetComponentInChildren<Renderer>(); return _renderer;  } set { _renderer = value; } }

    private void Start()
    {
        if (onStart)
        {
            if (useRandom) currentSkin = Random.Range(0, skins.Length);
            SetSkin();
        }
    }

    [EditorButton]
    public void SetSkin()
    {
        currentSkin = useRandom ? GetRandomSkin() : currentSkin;
        currentScheme = useRandom ? GetRandomScheme() : currentScheme;
        SetSkin(currentSkin, currentScheme);
    }

    public void SetSkin(int idTex, int idScheme)
    {
        if (renderer)
        {
            Material newMat = new Material(mat);
            newMat.SetTexture("_MaskTex", skins[idTex]);
            newMat.SetColor("_Color", HexToColor(schemes[idScheme]._color1));
            newMat.SetColor("_MaskColor", HexToColor(schemes[idScheme]._color2));
            SetMaterial(renderer, idMaterial, newMat);

        }
    }

    #region Materials

    public static int GetIDMaterial(Material[] mats, string name)
    {
        for (int i = 0; i < mats.Length; i++)
        {
            if (mats[i].name.Equals(name)) return i;
        }
        return 0;
    }

    public static void SetMaterial(Renderer renderer, int idMaterial, Material newMaterial)
    {
        if (Application.isPlaying)
        {
            Material[] mats = renderer.materials;
            mats[idMaterial] = newMaterial;
            renderer.materials = mats;
        }
        else
        {
            Material[] mats = renderer.sharedMaterials;
            mats[idMaterial] = newMaterial;
            renderer.sharedMaterials = mats;
        }
    }

    #endregion

    #region HexColor

    string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }

    #endregion
}
