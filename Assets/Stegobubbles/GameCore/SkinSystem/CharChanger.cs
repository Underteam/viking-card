﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharChanger : MonoBehaviour, GameCore.ShopSystem.IShopSelectable {

    public GameObject[] models;

    public int[] skinCounts;
    public bool changeMaterial = false;

    public int currentCharacter = 0;

    #region IShopSelectable

    public int currentID { get { return currentCharacter; } set { SetSkin(value); /*Set(value);*/ } }

    [EditorButton]
    public void Next()
    {
        //SetSkin(++currentCharacter);
        currentID++;
    }

    [EditorButton]
    public void Previous()
    {
        //SetSkin(--currentCharacter);
        currentID--;
    }

    public void Set(int id)
    {
        //SetSkin(id);
        currentID = id;
    }

    #endregion

    [EditorButton]
    public void SetRandom()
    {
        SetSkin(Random.Range(0, models.Length));
    }

    [EditorButton]
    public void SetSkin(int id)
    {
        // Зацикливаем индекс
        currentCharacter = CicleIndex(id, models.Length);
        // ID модели и скина
        int idModel = 0; int idSkin = 0;
        GetID(currentCharacter, out idModel, out idSkin);
        //Debug.Log(string.Format("currentCharacter={0} | idModel={1} | idSkin={2}", currentCharacter, idModel, idSkin));
        // Применяем в персонажу
        for (int i = 0; i < models.Length; i++)
        {
            // Включение/выключение модели
            models[i].SetActive(i == idModel);
            // Смена материала
            if (changeMaterial && i == idModel)
            {
                models[i].GetComponent<SkinChanger>().SetSkin(idSkin);
            }
        }
    }

	private void GetID(int id, out int idModel, out int idSkin)
    {
        idModel = 0; idSkin = 0;
        int count = 0;
        for (int i = 0; i < models.Length; i++)
        {
            for (int j = 0; j < skinCounts[i]; j++)
            {
                if (count == id)
                {
                    idModel = i;
                    idSkin = j;
                }
                count++;
            }
        }
    }

    public int CicleIndex(int id, int length)
    {
        if (id >= length) id = 0;
        if (id < 0) id = length - 1;
        return id;
    }

    #if UNITY_EDITOR

    [EditorButton]
    void TestSkins()
    {
        for (int i = 0; i < 15; i++)
        {
            int idModel = 0;
            int idSkin = 0;
            GetID(i, out idModel, out idSkin);
            Debug.Log(string.Format("m={0} s={1}", idModel, idSkin));
        }
    }

    #endif
}
