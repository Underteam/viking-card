﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinChanger : MonoBehaviour {

    public int idMaterial = 0;
    public Material darkSkin;
    public Material[] skins;
    public int currentSkin = 0;

    public bool onStart = false; 
    public bool useRandom = false;

    public string checkPrefsString = "skin{0}";
    public bool checkPrefs = false;

    public bool GetStatus() { return PlayerPrefs.GetInt(string.Format(checkPrefsString, currentSkin), 0) == 1; }
    public void SetStatus(bool status) { PlayerPrefs.SetInt(string.Format(checkPrefsString, currentSkin), status == true ? 1 : 0); PlayerPrefs.Save(); }

    [EditorButton]
    public void Lock()
    {
        SetStatus(false);
        SetSkin();
    }

    [EditorButton]
    public void Unlock()
    {
        SetStatus(true);
        SetSkin();
    }

    private Renderer _renderer;
    private new Renderer renderer { get { if (_renderer == null) _renderer = GetComponentInChildren<Renderer>(); return _renderer;  } set { _renderer = value; } }

    private void Start()
    {
        if (onStart)
        {
            if (useRandom) currentSkin = Random.Range(0, skins.Length);
            SetSkin();
        }
    }

    public void SetSkin(int id)
    {
        if (renderer)
        {
            if (checkPrefs)
            {
                if (GetStatus())
                {
                    SetSkin(skins[id]);
                }
                else
                {
                    SetDark();
                }
            }
            else
            {
                SetSkin(skins[id]);
            }
            
        }
    }

    public void SetSkin(Material newSkin)
    {
        if (Application.isPlaying)
        {
            Material[] mats = renderer.materials;
            mats[idMaterial] = newSkin;
            renderer.materials = mats;
        }
        else
        {
            Material[] mats = renderer.sharedMaterials;
            mats[idMaterial] = newSkin;
            renderer.sharedMaterials = mats;
        }
    }

    [EditorButton]
    public void SetSkin()
    {
        SetSkin(currentSkin);
    }

    [EditorButton]
    public void SetDark()
    {
        SetSkin(darkSkin);
    }

}
