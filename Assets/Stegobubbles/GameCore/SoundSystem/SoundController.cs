﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    private static SoundController _instance;
    public static SoundController instance { get { if (_instance == null) _instance = FindObjectOfType<SoundController>(); return _instance; } set { _instance = value; } }

    public static float musicVolume { get { return PlayerPrefs.GetFloat("MusicVolume", 0.5f); } set { PlayerPrefs.SetFloat("MusicVolume", value); } }
    public static float soundVolume { get { return PlayerPrefs.GetFloat("SoundVolume", 0.5f); } set { PlayerPrefs.SetFloat("SoundVolume", value); } }

    [System.Serializable]
    public class Sound
    {
        public string key;

        public enum Type { sound, music }
        public Type type = Type.sound;

        [Range(0, 1)]
        public float volume = 1;
        public AudioClip audioClip;
        public AudioSource audioSource;

        public float GetVolume()
        {
            switch (type)
            {
                case Type.sound: return volume * soundVolume;
                case Type.music: return volume * musicVolume;
                default: break;
            }
            return 0;
        }

        public void UpdateVolume()
        {
            if (audioSource)
            {
                audioSource.volume = GetVolume();
            }
        }
    }

    #region Current Volume

    [Range(0, 1)]
    public float currentMusicValue = 0.5f;
    private float old_currentMusicValue = 0;
    [Range(0, 1)]
    public float currentSoundValue = 0.5f;
    private float old_currentSoundValue = 0;

    private void OnValidate()
    {
        if (Mathf.Approximately(currentMusicValue, old_currentMusicValue) == false)
        {
            old_currentMusicValue = currentMusicValue;
            musicVolume = currentMusicValue;
            ReValueAllSounds();
        }
        if (Mathf.Approximately(currentSoundValue, old_currentSoundValue) == false)
        {
            old_currentSoundValue = currentMusicValue;
            soundVolume = currentSoundValue;
            ReValueAllSounds();
        }
    }

    #endregion

    private void Awake()
    {
        currentMusicValue = musicVolume;
        currentSoundValue = soundVolume;
    }

   
    // Общий audioSource
    public AudioSource commonSource;
    // Звуки
    public List<Sound> sounds;
    
    // Проигрывание по ключу
    public void Play(string key)
    {
        bool findKey = false;
        for (int i = 0; i < sounds.Count; i++)
        {
            if (sounds[i].key.Equals(key) && sounds[i].audioClip != null)
            {
                findKey = true;
                if (sounds[i].audioSource != null)
                {
                    sounds[i].audioSource.volume = sounds[i].GetVolume();
                    sounds[i].audioSource.PlayOneShot(sounds[i].audioClip);
                }
                else
                {
                    commonSource.volume = sounds[i].GetVolume();
                    commonSource.PlayOneShot(sounds[i].audioClip);
                }
                
            }
        }
        if (findKey == false)
        {
            Debug.Log(string.Format("SoundController Play Key '{0}' not found!", key));
        }
    }

    // Обновление громкостей всех звуков
    public void ReValueAllSounds()
    {
        for (int i = 0; i < sounds.Count; i++)
        {
            sounds[i].UpdateVolume();
        }
    }
}
