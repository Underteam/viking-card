﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MySound : MonoBehaviour {

    public bool isButton = false;
    public Button button;
    private bool added = false;

    public string key;

    private void OnEnable()
    {
        if (isButton)
        {
            if (button == null) button = GetComponent<Button>();
            if (button && added == false)
            {
                added = true;
                button.onClick.AddListener(()=>SoundController.instance.Play(key));
            }
        }
    }



}
