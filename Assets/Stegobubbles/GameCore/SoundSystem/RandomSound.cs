﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSound : MonoBehaviour {

    public AudioClip[] sounds;
    public AudioSource source;

    private void Awake()
    {
        if (source == null) source = GetComponent<AudioSource>();
    }

    [EditorButton]
    public void Play(int id = -1)
    {
        id = GetID(id);
        if (source)
        {
            if (source.isPlaying) return;

            if (sounds[id])
            {
                source.clip = sounds[id];
                source.Play();
            }
        }
    }

    public int GetID(int id = -1) { if (id == -1) id = Random.Range(0, sounds.Length); return id; }

    /// <summary>
    /// Get Audio (id == -1 - it's random sound)
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public AudioClip GetAudio(int id = -1) { return sounds[GetID(id)]; }
}
