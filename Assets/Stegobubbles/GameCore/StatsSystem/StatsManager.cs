using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.StatsSystem
{
    public class StatsManager : MonoBehaviour
    {
        public List<StatData> stats = new List<StatData>();
        private Dictionary<string, StatData> cacheStats = new Dictionary<string, StatData>();

        public bool updateValues = false;
        public bool initOnEnable = false;

        private void OnEnable()
        {
            if (initOnEnable) Init();
        }

        [EditorButton]
        public void Add(int id, int count)
        {
            stats[id].current += count;
        }

        [EditorButton]
        public void Remove(int id, int count)
        {
            stats[id].current -= count;
        }

        [EditorButton]
        public void Init(bool setMax = false)
        {
            for (int i = 0; i < stats.Count; i++)
            {
                stats[i].Init(setMax);
            }
        }

        private void FixedUpdate()
        {
            if (updateValues)
            {
                for (int i = 0; i < stats.Count; i++)
                {
                    stats[i].Update();
                }
            }
        }

        // �������� ��������
        public float GetValue(string key)
        {
            if (cacheStats.ContainsKey(key)) { return cacheStats[key].current; }
            else
            {
                for (int i = 0; i < stats.Count; i++)
                {
                    if (stats[i].key.Equals(key))
                    {
                        cacheStats.Add(key, stats[i]);
                        return stats[i].current;
                    }
                }
            }
            return -1;
        }

        // ���������� �������� (true - ������, false - ��������)
        public bool SetValue(string key, float value)
        {
            if (cacheStats.ContainsKey(key)) { cacheStats[key].current = value; return true; }
            else
            {
                for (int i = 0; i < stats.Count; i++)
                {
                    if (stats[i].key.Equals(key))
                    {
                        cacheStats.Add(key, stats[i]);
                        stats[i].current = value;
                        return true;
                    }
                }
            }
            return false;
        }

        // ��������� ����� �� �����
        public StatData GetStatData(string key) { for (int i = 0; i < stats.Count; i++) { if (stats[i].key.Equals(key)) return stats[i]; } return null; }

        [EditorButton]
        void TestSaveStats()
        {
            SaveStats("TestSavesStats");
        }

        [EditorButton]
        void TestLoadStats()
        {
            SaveStats("TestSavesStats");
        }

        public void SaveStats(string saveDataKey, string separator = ":")
        {
            string result = "";
            for (int i = 0; i < stats.Count; i++)
            {
                if (i > 0 && i < stats.Count) result += separator[0];
                result += string.Format("{0}={1}", stats[i].key, stats[i].current);
            }
            Debug.Log(result);
            //PlayerPrefs.SetString(saveDataKey, result);
        }

        public void LoadStats(string saveDataKey, string separator = ":")
        {
            string[] keys = PlayerPrefs.GetString(saveDataKey, "").Split(separator[0]);
            Dictionary<string, float> result = new Dictionary<string, float>();
            for (int i = 0; i < keys.Length; i++)
            {
                if (keys[i] != "")
                {
                    string[] data = keys[i].Split('=');
                    if (data.Length == 2) result.Add(data[0], float.Parse(data[1]));
                }
            }

            foreach (var item in result)
            {
                Debug.Log(string.Format("{0}={1}", item.Key, item.Value));
            }
        }

        //public int Score;

        //public List<int> counterKilled;
        //public List<int> counterGathered;

        //private PlayerController playerController;

    }
}