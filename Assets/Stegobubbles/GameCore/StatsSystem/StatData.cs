﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.StatsSystem
{
    public class StatData : MonoBehaviour
    {
        public string key;
        public bool use = true;

        public float min = 0;
        public float max = 100;
        private float _current;
        public float current { get { return _current; } set { _current = Mathf.Clamp(value, min, max); UpdateValues(); } }

        public enum RegenType { none, up, down, condition }
        public RegenType regenType = RegenType.none;

        public bool regenPerSecond = false;
        public float regenDown = -3f;
        public float regenUp = 11f;
        public bool regenCondition { get; set; }
        public bool startCondition = false;

        public StatBar bar;



        #region ActionOnValue

        public bool useActionsOnValue = false;
        private bool ActionsOnValueActive = false;
        public List<ActionOnValue> actionsOnValue;

        void CheckActionsOnValue()
        {
            for (int i = 0; i < actionsOnValue.Count; i++)
            {
                actionsOnValue[i].value = current;
            }
        }

        [System.Serializable]
        public class ActionOnValue
        {
            public enum TypeAction { zero, min, max, value }
            public TypeAction typeAction = TypeAction.zero;

            public enum TypeCompare { equality, less, more }
            public TypeCompare typeCompare = TypeCompare.equality;

            private float _value;
            public float value { get { return _value; } set { _value = value; if (Check()) action.Invoke(); } }
            public float valueMax;
            public float valueMin;
            public float valueValue;

            public UnityEngine.Events.UnityEvent action;

            bool Check()
            {
                switch (typeAction)
                {
                    case TypeAction.zero: if (Mathf.Approximately(_value, 0)) return true; break;
                    case TypeAction.min: if (_value <= valueMin) return true; break;
                    case TypeAction.max: if (_value >= valueMax) return true; break;
                    case TypeAction.value:
                        {
                            switch (typeCompare)
                            {
                                case TypeCompare.equality: if (Mathf.Approximately(_value, valueValue)) return true; break;
                                case TypeCompare.less: if (_value <= valueValue) return true; break;
                                case TypeCompare.more: if (_value >= valueValue) return true; break;
                                default:
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        }

        #endregion


        public void Init(bool setMax = false)
        {
            if (setMax) current = max;
            regenCondition = startCondition;

            bar.SetMax((int)this.max);
            bar.SetCurrent((int)this.current);

            if (useActionsOnValue) ActionsOnValueActive = true;
            //Debug.Log(string.Format("{0} inited!", key));
        }

        public void ChangeRegenType(int id)
        {
            regenType = (RegenType)id;
        }

        public void UpdateValues()
        {
            if (use == false) return;

            if (bar) bar.SetCurrent((int)this.current);

            if (useActionsOnValue && ActionsOnValueActive) CheckActionsOnValue();
        }

        public void Update()
        {
            //if (MyGame.instance.gameState != GameState.ingame) return;
            if (use == false) return;
            if (regenType != RegenType.none) UpdateRegen(regenType);
        }

        void UpdateRegen(RegenType regenType)
        {
            switch (regenType)
            {
                case RegenType.up:
                    if (current < max) current += regenUp * Time.fixedDeltaTime;
                    break;
                case RegenType.down:
                    if (current > min) current += regenDown * Time.fixedDeltaTime;
                    break;
                case RegenType.condition:
                    UpdateRegen(regenCondition ? RegenType.up : RegenType.down);
                    break;
                default:
                    break;
            }

        }


        public void SetMax()
        {
            current = max;
        }


        //public void ChangeHealth(float amount)
        //{
        //    Debug.Log("ChangeHealth " + amount);
        //    MonoBehaviour.print("upd " + amount + "  " + this.healthCurrent);
        //    if (amount < 0f)
        //        this.redEffectAnimator.SetTrigger("AnimateRed");
        //    else if (amount > 0f)
        //    {
        //        this.redEffectAnimator.SetTrigger("AnimateGreen");
        //    }
        //    this.healthCurrent += amount;
        //    this.UpdateHealth();
        //}

        //public void ApplyDamage(float amount)
        //{
        //    this.lastAttack = 0f;
        //    Debug.Log("DAMAGE");
        //    float num = amount;
        //    //if (this.useResistance)
        //    //    num *= 1f - this.resistanceCurrent;
        //    this.ChangeHealth(num);
        //}

        //public void ChangeEnergy(float amount)
        //{
        //    if (this.useEnergy)
        //    {
        //        this.energyCurrent += amount;
        //        this.UpdateEnergy();
        //    }
        //}
    }
}