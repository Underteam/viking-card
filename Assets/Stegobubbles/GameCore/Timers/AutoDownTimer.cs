﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoDownTimer : MonoBehaviour {
    public float delay = 1;
    public float time = 5;
    public Slider slider;
    public Image image;

    public bool actionOnStart = false;
    public UnityEngine.Events.UnityEvent actionOnEnable;
    public UnityEngine.Events.UnityEvent action;
    

    private void OnEnable()
    {
        actionOnEnable.Invoke();
        if (actionOnStart)
        {
            Action();
        }
    }

    public void Action()
    {
        if (slider) LeanTween.value(gameObject, (float value)=>{ slider.value = value; } , 1, 0, time).setDelay(delay).setOnComplete(()=> { action.Invoke(); });
        if (image) LeanTween.value(gameObject, (float value) => { image.fillAmount = value; }, 1, 0, time).setDelay(delay).setOnComplete(() => { action.Invoke(); });
    }

    public void Stop()
    {
        LeanTween.cancel(gameObject);
    }

    public void ResetTimer()
    {
        if (slider) slider.value = 1;
        if (image) image.fillAmount = 1;
    }
}
