﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownTimer : MonoBehaviour {

    public string prefsKey;

    public float timeMax = 20;
    private float currentTime = 0;
    private int lastTime = 0;

    public bool active = false;
    public bool isDown = true;
    public bool cyclical = true;
    public bool forceUpdate = false;

    public UnityEngine.Events.UnityEvent onTimeEnd;

    public int time { get { return PlayerPrefs.GetInt(prefsKey, (int)timeMax); } set { PlayerPrefs.SetInt(prefsKey, value); PlayerPrefs.Save(); } }

    private string timeString { get { int minutes = 0; int seconds = 0; ToTime((int)(isDown ? currentTime : (timeMax - currentTime)), out minutes, out seconds); return string.Format("{0:00}:{1:00}", minutes, seconds); } }

    public Text text;
    public Image image;
    public Slider slider;

    public void UpdateValue()
    {
        UpdateText();
        UpdateImage();
        UpdateSlider();
    }
    public void UpdateText() { if (text) text.text = active ? timeString : ""; }
    public void UpdateImage() { if (image) image.fillAmount = active ? GetCurrentValue() : 0; }
    public void UpdateSlider() { if (slider) slider.value = active ? GetCurrentValue() : 0; }

    private float GetCurrentValue() { return isDown ? (currentTime / timeMax) : (1f - currentTime / timeMax); }
    public float timeStepToSave = 5;

    private void Awake()
    {
        LoadTime();
        UpdateValue();
    }

    void LoadTime()
    {
        currentTime = (float)time;

        if (Mathf.Approximately(currentTime, 0) && cyclical == false)
        {
            currentTime = timeMax;
            Debug.Log(string.Format("Таймер {0} пуст!", prefsKey), this);
        }
    }

    IEnumerator SaveTime()
    {
        WaitForSeconds wfs = new WaitForSeconds(timeStepToSave);
        while (true)
        {
            yield return wfs;
            time = (int)currentTime;
        }
    }

    public void StartTimer(bool setMax = false)
    {
        if (setMax)
        {
            currentTime = timeMax; 
        }
        else
        {
            LoadTime();
        }
        active = true;
    }

    public void FinishTimer(bool forceAction)
    {
        if (forceAction)
        {
            currentTime = 0;
            time = (int)timeMax;
        }
        else
        {
            active = false;
            time = (int)timeMax;
        }
        UpdateValue();
    }

    private void Start()
    {
        StartCoroutine(SaveTime());
    }

    private void Update()
    {
        if (active)
        {
            if (currentTime > 0)
            {
                currentTime -= Time.deltaTime;

                if (lastTime != (int)currentTime || forceUpdate)
                {
                    UpdateValue();
                }
                lastTime = (int)currentTime;
            }
            else
            {
                if (cyclical)
                {
                    currentTime = timeMax;
                }
                else
                {
                    active = false;
                    time = (int)timeMax;
                }
                if (onTimeEnd != null) onTimeEnd.Invoke();
                UpdateValue();
            }
        }
    }

    [EditorButton]
    void SetMaxTime(int minutes, int seconds)
    {
        timeMax = ToSecond(minutes, seconds);
    }

    [EditorButton]
    void TestTime(int count)
    {
        for (int i = 0; i < count; i++)
        {
            int h = Random.Range(0, 2);
            int m = Random.Range(0, 60);
            int s = Random.Range(0, 60);
            Debug.Log(string.Format("m{0}s{1} = s{2}", m, s, ToSecond(m, s)));
        }
    }

    public static int ToSecond(int minutes, int seconds)
    {
        return (minutes * 60) + seconds;
    }

    public static void ToTime(int value, out int minutes, out int seconds)
    {
        minutes = Mathf.FloorToInt(value / 60f);
        seconds = Mathf.FloorToInt(value - minutes * 60);
    }

}
