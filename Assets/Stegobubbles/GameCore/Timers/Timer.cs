﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameCore
{
    [RequireComponent(typeof(Text))]
    public class Timer : MonoBehaviour
    {

        public Text text;
        public float timeMax = 2;
        public float timeCurrent = 0;

        public bool isWork = false;
        public bool isLooped = false;

        public UnityEvent eventTimeOut;
        private string currentTime = "";
        private string lastTime = "";

        private void Awake()
        {
            if (text == null) GetComponent<Text>();
        }

        public void SetTimer(float time, bool start = false, bool isLooped = false)
        {
            timeCurrent = timeMax = time;
            this.isLooped = isLooped;
            isWork = start;
        }

        private void Update()
        {
            if (isWork)
            {
                if (timeCurrent <= 0)
                {
                    if (isLooped)
                    {
                        timeCurrent = timeMax;
                    }
                    else
                    {
                        timeCurrent = 0;
                        isWork = false;
                    }
                    eventTimeOut.Invoke();
                }
                else
                {
                    timeCurrent -= Time.deltaTime;
                    currentTime = string.Format("{0:0}", timeCurrent);
                    if (!currentTime.Equals(lastTime))
                    {
                        text.text = currentTime;
                    }
                    lastTime = currentTime;
                }
            }
        }

        public void DeActive()
        {
            isWork = false;
            text.text = "";
        }
    }
}