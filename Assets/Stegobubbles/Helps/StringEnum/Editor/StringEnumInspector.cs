﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(StringEnum))]
public class StringEnumInspector : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) { return EditorGUIUtility.singleLineHeight * 2f; }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var r = new Rect(position.xMin, position.yMin, position.width, EditorGUIUtility.singleLineHeight);
        r = new Rect(r.xMin, r.yMax, r.width, r.height);
        var valueProp = property.FindPropertyRelative("_value");
        var valuesProp = property.FindPropertyRelative("values");

        string[] list = GetList(property, "values");
        int selectId = EditorGUI.Popup(r, label.text, GetID(valueProp.stringValue, list), list);
        if (list.Length > 0)
        {
            valueProp.stringValue = list[selectId];
        }
        
    }

    int GetID(string value, string[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].Equals(value)) return i;
        }
        return 0;
    }

    string[] GetList(SerializedProperty property, string name)
    {
        int size = property.FindPropertyRelative(name + ".Array.size").intValue;
        List<string> result = new List<string>();
        for (int i = 0; i < size; i++)
        {
            var prop = property.FindPropertyRelative(string.Format("{0}.Array.data[{1}]", name, i));
            result.Add(prop.stringValue);
        }
        return result.ToArray();
    }

}