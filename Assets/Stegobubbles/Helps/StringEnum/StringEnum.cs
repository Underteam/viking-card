﻿using UnityEngine;

[System.Serializable()]
public class StringEnum : Object
{
    [SerializeField]
    private string _value;
    public string Value { get { return _value; } set { _value = value; } }
    public string[] values;

    public int GetID(string value, string[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            if (list[i].Equals(value)) return i;
        }
        return 0;
    }

}