﻿using UnityEngine;

[System.Serializable()]
public struct IntRange
{
    [SerializeField()]
    private int _maxValue;
    [SerializeField()]
    private int _value;
    public int MaxValue{ get { return _maxValue; } set { _maxValue = value; _value = Mathf.Clamp(_value, 0, _maxValue); } }
    public int Value { get { return _value; } set { _value = Mathf.Clamp(value, 0, _maxValue); } }
}

[System.Serializable()]
public struct FloatRange
{
    [SerializeField()]
    private float _maxValue;
    [SerializeField()]
    private float _value;
    public float MaxValue { get { return _maxValue; } set { _maxValue = value; _value = Mathf.Clamp(_value, 0, _maxValue); } }
    public float Value { get { return _value; } set { _value = Mathf.Clamp(value, 0, _maxValue); } }
}