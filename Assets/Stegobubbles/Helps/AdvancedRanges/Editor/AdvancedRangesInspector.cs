﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(IntRange))]
public class IntRangeInspector : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) { return EditorGUIUtility.singleLineHeight * 2f; }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var r = new Rect(position.xMin, position.yMin, position.width, EditorGUIUtility.singleLineHeight);
        var maxProp = property.FindPropertyRelative("_maxValue");
        EditorGUI.PropertyField(r, maxProp, new GUIContent("MaxValue"));
        r = new Rect(r.xMin, r.yMax, r.width, r.height);
        var valueProp = property.FindPropertyRelative("_value");
        //valueProp.intValue = Mathf.Clamp(EditorGUI.IntField(r, "Value", valueProp.intValue), 0, maxProp.intValue);
        valueProp.intValue = EditorGUI.IntSlider(r, "Value", valueProp.intValue, 0, maxProp.intValue);
    }

}

[CustomPropertyDrawer(typeof(FloatRange))]
public class FloatRangeInspector : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) { return EditorGUIUtility.singleLineHeight * 2f; }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var r = new Rect(position.xMin, position.yMin, position.width, EditorGUIUtility.singleLineHeight);
        var maxProp = property.FindPropertyRelative("_maxValue");
        EditorGUI.PropertyField(r, maxProp, new GUIContent("MaxValue"));
        r = new Rect(r.xMin, r.yMax, r.width, r.height);
        var valueProp = property.FindPropertyRelative("_value");
        //valueProp.floatValue = Mathf.Clamp(EditorGUI.FloatField(r, "Value", valueProp.floatValue), 0, maxProp.floatValue);
        valueProp.floatValue = EditorGUI.Slider(r, "Value", valueProp.floatValue, 0, maxProp.floatValue);
    }
}