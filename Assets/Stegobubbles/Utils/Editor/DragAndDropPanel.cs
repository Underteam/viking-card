﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MainUtils
{
    public class DragAndDropPanel
    {
        #if UNITY_EDITOR
        private Color color;
        private GUIStyle boxStyle;
        public System.Type[] types = new System.Type[] { typeof(UnityEditor.MonoScript) };
        private string text;
        public System.Action<Object[]> Add;
        public System.Action<int> Remove;

        public DragAndDropPanel(string text, GUIStyle boxStyle, Color color, System.Type[] types = null)
        {
            this.text = text;
            this.boxStyle = boxStyle;
            if (types != null) this.types = types;
            this.color = color;
        }

        public void OnGUI()
        {
            Event evt = Event.current;
            Rect drop_area = GUILayoutUtility.GetRect(0.0f, 50.0f, GUILayout.ExpandWidth(true));
            this.boxStyle.alignment = TextAnchor.MiddleCenter;
            GUI.color = color;
            GUI.Box(drop_area, text, this.boxStyle);
            GUI.color = Color.white;

            switch (evt.type)
            {
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    {
                        if (!drop_area.Contains(evt.mousePosition))
                            return;

                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                        if (evt.type == EventType.DragPerform)
                        {
                            DragAndDrop.AcceptDrag();

                            Object[] filtered = FilterResourcesForImport(DragAndDrop.objectReferences, types);
                            Debug.Log(string.Format("Определено объектов: {0}!", filtered.Length));
                            // Типы обрабатываются внутри
                            Add.Invoke(filtered);
                        }
                        break;
                    }
            }
        }

        // Фильтр ресурсов для импорта
        public static Object[] FilterResourcesForImport(Object[] resources, System.Type[] types)
        {
            List<Object> tempList = new List<Object>();

            foreach (Object resource in resources)
            {
                string resourcePath = AssetUtils.GetAssetPath(resource);
                //Debug.Log(resource.GetType());
                // Проверяем, является ли объект основным ассетом и добавляем в очередь все его вложенные ассеты
                if (AssetUtils.IsMainAsset(resource) && AssetUtils.HasSubAssets(resource))
                {
                    Object[] subAssets = FilterResourcesForImport(AssetUtils.GetSubAssets(resource), types);

                    foreach (Object a in subAssets) tempList.Add(a);
                }
                // Проверяем тип по фильтру
                else if (CheckType(resource, types))
                {
                    tempList.Add(resource);
                }
                // Добавляем объекты в папке
                else if (AssetUtils.IsDirectory(resourcePath))
                {

                    Object[] subAssets = FilterResourcesForImport(AssetUtils.GetDirectoryAssets(resourcePath, true), types);
                    //List<Object> subAssets = new List<Object>(FilterResourcesForImport(AssetUtils.GetDirectoryAssets(resourcePath), types));

                    foreach (Object a in subAssets) tempList.Add(a);
                }
            }

            return tempList.ToArray();
        }

        // Проверяем тип объекта по фильтру типов
        public static bool CheckType(Object obj, System.Type[] types)
        {
            for (int i = 0; i < types.Length; i++)
            {
                if (obj.GetType().Equals(types[i])) return true;
            }
            return false;
        }
        #endif
    }
}