﻿using System;
using System.IO;
using UnityEngine;
using System.Diagnostics;
using UnityEditor;
using Debug = UnityEngine.Debug;

public class BuildPlayerRun
{
    static string adbLocation;
    static string bundleIdent = PlayerSettings.applicationIdentifier;

    [MenuItem("Stegobubbles/Build/PushRun To Android %i")]
    public static void PushToAndroid()
    {
        string apkLocation = EditorPrefs.GetString("APK location");
        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(apkLocation))
            apkLocation = EditorUtility.OpenFilePanel("Find APK", Environment.CurrentDirectory, "apk");

        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(apkLocation))
        {
            Debug.LogError("Cannot find .apk file.");
            return;
        }
        EditorPrefs.SetString("APK location", apkLocation);

        adbLocation = EditorPrefs.GetString("Android debug bridge location");
        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(adbLocation))
            adbLocation = EditorUtility.OpenFilePanel("Android debug bridge", Environment.CurrentDirectory, "exe");
        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(adbLocation))
        {
            Debug.LogError("Cannot find adb.exe.");
            return;
        }
        EditorPrefs.SetString("Android debug bridge location", adbLocation);

        ProcessStartInfo info = new ProcessStartInfo
        {
            FileName = adbLocation,
            Arguments = string.Format("install -r \"{0}\"", apkLocation),
            WorkingDirectory = Path.GetDirectoryName(adbLocation),
        };
        Process adbPushProcess = Process.Start(info);
        if (adbPushProcess != null)
        {
            adbPushProcess.EnableRaisingEvents = true;
            adbPushProcess.Exited += RunApp;
        }
        else
        {
            Debug.LogError("Error starting adb");
        }
    }

    [MenuItem("Stegobubbles/Build/Run On Android")]
    public static void RunApp()
    {
        RunApp(null, null);
    }

    public static void RunApp(object o, EventArgs args)
    {
        ProcessStartInfo info = new ProcessStartInfo
        {
            FileName = adbLocation,
            Arguments = string.Format("shell am start -n " + bundleIdent + "/com.unity3d.player.UnityPlayer"), //"/com.unity3d.player.UnityPlayerNativeActivity"),
            WorkingDirectory = Path.GetDirectoryName(adbLocation),
        };

        Process.Start(info);
    }

    [MenuItem("Stegobubbles/Build/Change APK to push")]
    public static void ChangeAPK()
    {
        PlayerPrefs.SetString("APK location", EditorUtility.OpenFilePanel("Find APK", Environment.CurrentDirectory, "apk"));
    }
}

//public class BuildPlayerRun : MonoBehaviour
//{

//    [MenuItem("Stegobubbles/Build/Push To Android")]
//    public static void PushToAndroid()
//    {
//        string apkLocation = EditorPrefs.GetString("APK location");
//        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(apkLocation))
//            apkLocation = EditorUtility.OpenFilePanel("Find APK", Environment.CurrentDirectory, "apk");
//        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(apkLocation))
//        {
//            Debug.LogError("Cannot find .apk file.");
//            return;
//        }
//        EditorPrefs.SetString("APK location", apkLocation);

//        string adbLocation = EditorPrefs.GetString("Android debug bridge location");
//        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(adbLocation))
//            adbLocation = EditorUtility.OpenFilePanel("Android debug bridge", Environment.CurrentDirectory, "exe");
//        if (string.IsNullOrEmpty(apkLocation) || !File.Exists(adbLocation))
//        {
//            Debug.LogError("Cannot find adb.exe.");
//            return;
//        }
//        EditorPrefs.SetString("Android debug bridge location", adbLocation);
//        Debug.Log("!!! " + adbLocation + " " + EditorPrefs.GetString("AndroidSdkRoot"));

//        ProcessStartInfo info = new ProcessStartInfo
//        {
//            FileName = adbLocation,
//            Arguments = string.Format("install -r \"{0}\"", apkLocation),
//            WorkingDirectory = Path.GetDirectoryName(adbLocation),
//        };
//        Process.Start(info);
//    }
//}