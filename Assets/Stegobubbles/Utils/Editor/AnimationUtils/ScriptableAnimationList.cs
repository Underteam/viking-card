﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MainUtils
{
    #if UNITY_EDITOR
    [CreateAssetMenu(fileName = "AnimationList", menuName = "Stegobubbles/AnimationList", order = 1)]
    #endif
    public class ScriptableAnimationList : ScriptableObject
    {

        public List<MyAnim> list;

        public string[] GetPathes()
        {
            List<string> result = new List<string>();
            for (int i = 0; i < list.Count; i++) result.Add(list[i].path);
            return result.ToArray();
        }

        public string[] GetNames()
        {
            List<string> result = new List<string>();
            for (int i = 0; i < list.Count; i++) result.Add(list[i].name);
            return result.ToArray();
        }

        public System.Type[] GetTypes()
        {
            List<System.Type> result = new List<System.Type>();
            for (int i = 0; i < list.Count; i++) result.Add((AssetUtils.GetAsset(list[i].path) as AnimationClip).GetType());
            return result.ToArray();
        }

        public void Add(Object[] resources)
        {
            foreach (Object resource in resources) this.Add(resource);
        }

        public void Add(Object resource)
        {
            if (resource is AnimationClip)
            {
                string path = AssetUtils.GetAssetPath(resource);
                // Добавляем если ещё нет такого
                if (!Contains(path)) list.Add(new MyAnim("Имя " + list.Count, path));
            }
        }

        public void RemoveAt(int id)
        {
            list.RemoveAt(id);
        }

        public bool Contains(string path)
        {
            for (int i = 0; i < list.Count; i++)
                if (list[i].path.Equals(path)) return true;
            return false;
        }
    }

    [System.Serializable]
    public class MyAnim
    {
        public string name;
        public string path;

        public MyAnim(string name, string path)
        {
            this.name = name;
            this.path = path;
        }
    }
}