﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MainUtils
{
    [CustomEditor(typeof(ScriptableAnimationList))]
    public class ScriptableAnimationListEditor : Editor
    {

        ScriptableAnimationList _target;
        DragAndDropPanel ddp;

        protected void OnEnable()
        {
            if (_target == null) _target = (ScriptableAnimationList)target;
            if (ddp == null)
            {
                ddp = new DragAndDropPanel("Add Class (Drop here)", new GUIStyle(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).box), Color.gray, new System.Type[] { typeof(AnimationClip) });
            }

            if (ddp != null)
            {
                ddp.Add += _target.Add;
            }
        }

        protected void OnDisable()
        {
            ddp.Add -= _target.Add;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (ddp != null)
            {
                ddp.OnGUI();
                DrawObjectList(_target.GetNames(), _target.GetPathes());
            }
            else
            {
                GUILayout.Label("Need FilterList!");
            }


        }

        void DrawObjectList(string[] names, string[] pathes)
        {

            if (GUILayout.Button("ClearList"))
            {
                _target.list.Clear();
            }
            GUILayout.BeginVertical(EditorStyles.helpBox);
            if (pathes.Length > 0)
            {
                Object tempObj;
                for (int i = 0; i < pathes.Length; i++)
                {
                    tempObj = AssetUtils.GetAsset(pathes[i]);
                    DrawObject(names[i], tempObj, i);
                }
            }
            else
            {
                GUILayout.Label("List is Empty!");
            }
            GUILayout.EndVertical();
        }

        void DrawObject(string nameObj, Object obj, int id)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(nameObj);
            EditorGUILayout.ObjectField(obj, obj.GetType(), true);
            if (GUILayout.Button("X", GUILayout.Width(20)))
            {
                _target.RemoveAt(id);
            }
            GUILayout.EndHorizontal();
        }

    }
}