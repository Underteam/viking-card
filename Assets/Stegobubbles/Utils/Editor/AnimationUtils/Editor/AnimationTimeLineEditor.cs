﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MainUtils
{
    public class AnimationTimeLineEditor : EditorWindow
    {
        [MenuItem("AnimationTimeLineEditor", menuItem = "Stegobubbles/Utils/AnimationTimeLineEditor")]
        static void Open()
        {
            GetWindow<AnimationTimeLineEditor>();
        }

        [System.Serializable]
        public class AnimCharacter
        {
            public int selectAnimation = -1;
            public bool useAnimationList = false;
            // Анимируемый объект
            public Animator animObject;
            // Анимация
            public AnimationClip animClip;
            // Значение
            public float value;
        }
        public List<AnimCharacter> animCharecterList = new List<AnimCharacter>();
        public ScriptableAnimationList animationList;
        private Vector2 scroll = Vector2.zero;

        void OnGUI()
        {
            animationList = (ScriptableAnimationList)EditorGUILayout.ObjectField("Список анимаций", animationList, typeof(ScriptableAnimationList), true);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+", GUILayout.Width(20))) animCharecterList.Add(new AnimCharacter());
            GUILayout.EndHorizontal();
            scroll = GUILayout.BeginScrollView(scroll);
            for (int i = 0; i < animCharecterList.Count; i++)
            {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                DrawItem(animCharecterList[i], i);
                GUILayout.EndVertical();
            }
            GUILayout.EndScrollView();
            Repaint();
        }

        public void DrawItem(AnimCharacter item, int id)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(id.ToString(), EditorStyles.boldLabel);
            if (GUILayout.Button("-", GUILayout.Width(20))) animCharecterList.Remove(item);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Персонаж: ", GUILayout.Width(110));
            item.animObject = (Animator)EditorGUILayout.ObjectField(item.animObject, typeof(UnityEngine.Animator), true, GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            
            GUILayout.BeginHorizontal();
            item.useAnimationList = GUILayout.Toggle(item.useAnimationList, "");
            if (item.useAnimationList) item.useAnimationList = animationList == null ? false : true;
            GUILayout.Label("Анимация: ", GUILayout.Width(110));
            if (item.useAnimationList)
            {
                int oldIndex = item.selectAnimation;
                item.selectAnimation = EditorGUILayout.Popup(item.selectAnimation, animationList.GetNames());
                if (oldIndex != item.selectAnimation)
                {
                    item.animClip = (AnimationClip)AssetDatabase.LoadAssetAtPath(animationList.list[item.selectAnimation].path, typeof(AnimationClip));
                }
            }
            else
            {
                item.animClip = (AnimationClip)EditorGUILayout.ObjectField(item.animClip, typeof(UnityEngine.AnimationClip), true);
            }
            GUILayout.EndHorizontal();

            EditorGUILayout.Space();

            if (item.animObject == null || item.animClip == null)
            {
                GUILayout.Label("Укажите персонажа и анимацию");
                EditorGUILayout.Space();
                EditorGUI.BeginDisabledGroup(true);

            }

            GUILayout.BeginHorizontal();
            float oldValue = item.value;
			item.value = EditorGUILayout.Slider(new GUIContent("Время"), item.value, 0, item.animClip ? item.animClip.length : 0, GUILayout.Width(300));
            GUILayout.EndHorizontal();

            if (item.animObject == null || item.animClip == null)
            {
                EditorGUI.EndDisabledGroup();
            }

            //--------------------------------------------------------------------

            if (item.animObject != null && item.animClip != null && oldValue != item.value)
            {
                item.animClip.SampleAnimation(item.animObject.gameObject, item.value);
            }
        }

    }
}