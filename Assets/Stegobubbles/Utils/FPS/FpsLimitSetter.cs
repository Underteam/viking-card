﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore
{
    public class FpsLimitSetter : MonoBehaviour
    {

        public bool useLimit = false;
        public int defaultLimit = 60;

        private void Awake()
        {
            if (useLimit) SetFPSLimit(defaultLimit);
        }

        [EditorButton]
        void Set30() { SetFPSLimit(30); }

        [EditorButton]
        void Set60() { SetFPSLimit(60); }

        [EditorButton]
        void Set120() { SetFPSLimit(120); }

        [EditorButton]
        void SetFPSLimit(int count)
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = count;
        }
    }
}