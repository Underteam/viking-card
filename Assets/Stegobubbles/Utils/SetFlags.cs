﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

public class SetFlags : MonoBehaviour {

    public Object file;

    [EditorButton]
    public void Set_HideFlags_DontSave()
    {
        file.hideFlags = HideFlags.DontSave;
    }

    [EditorButton]
    public void Set_HideFlags_None()
    {
        file.hideFlags = HideFlags.None;
    }
}


#endif