﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DC.View
{
	public class Card3D : MonoBehaviour
    {
		private Model.Card _card;
		public Model.Card card
        {
			get { return _card; }
			set {
				//Debug.LogWarning ("Assign " + (_card == null ? "null" : _card.coord.ToString ()) + " -> " + (value == null ? "null" : value.coord.ToString ()));
				_card = value;
				UpdateCard ();
			}
		}

		public Image image;
		public Text lblHealth;
		public Text lblShield;
		public Text lblAttack;
		public Text lblName;
		public GameObject keyIcon;

        public Image shield;
        public Image background;
        public bool offLight = false;
        public bool useLootColor = false;
        public ColorByLevel colorLight;
        public GameObject arrows;

        public ParticleSystem fx_Heal;
        public ParticleSystem fx_Poison;
        public ParticleSystem fx_Lights;
        public ParticleSystem fx_Golds;

        public enum FXType { heal, poison, light, golds }

        public void PlayFX(FXType fxType, float delayDestroy = 2)
        {
            ParticleSystem ps = null;
            switch (fxType)
            {
                case FXType.heal:
                    ps = fx_Heal;
                    break;
                case FXType.poison:
                    ps = fx_Poison;
                    break;
                case FXType.light:
                    ps = fx_Lights;
                    break;
                case FXType.golds:
                    ps = fx_Golds;
                    break;
                default:
                    break;
            }

            if (ps != null)
            {
                ps.gameObject.SetActive(true);
                ps.Play();

                if (delayDestroy > 0)
                {
                    LeanTween.cancel(ps.gameObject);
                    LeanTween.delayedCall(delayDestroy, () => { if (ps != null) ps.gameObject.SetActive(false); });
                }
            }
        }

        [HideInInspector]
        public bool showCardName = true;

        public bool showMaxHealth = false;

        public System.Action<Card3D> onClick;

        public void UpdateCard()
        {

            if (image == null)
                Debug.LogError("SR destroyed " + gameObject, gameObject);

            if (_card == null)
                Debug.Log("Card is null", this);

            image.sprite = _card.image;

            lblShield.text = "0";
            lblAttack.text = "0";

            if (_card is Model.Animate)
            {
                lblHealth.text = "" + (_card as Model.Animate).health;
                if (showMaxHealth) lblHealth.text = string.Format("{0}/{1}", (_card as Model.Animate).health, (_card as Model.Animate).maxHealth);
            }
            else
            {
                lblHealth.text = "";
            }

            if (_card is Model.Player)
            {
                if ((_card as Model.Player).shield != null)
                {
                    lblShield.text = "" + (_card as Model.Player).shield.shield;
                    if (shield)
                    {
                        shield.enabled = true;
                        //Debug.Log((_card as Model.Player).lastShield);
                        shield.sprite  = CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Shield, (_card as Model.Player).lastShield).customImage;
                    }
                }
                else
                {
                    if (shield) shield.enabled = false;
                }
                //if ((_card as Model.Player).blaster != null) lblAttack.text = "" + (_card as Model.Player).blaster.damage;
                lblAttack.text = "";
                if ((_card as Model.Player).numKeys > 0) keyIcon.SetActive(true);
                else keyIcon.SetActive(false);

                Canvas canvas = GetComponentInChildren<Canvas>();
                if (canvas)
                {
                    canvas.sortingOrder = 2;
                }

                if ((card as Model.Player).health == 0)
                {
                    lblHealth.text = "";
                    lblShield.text = "";


                    if (LeanTween.isTweening(background.gameObject) == false)
                    {
                        if (SoundController.instance) SoundController.instance.Play("PlayerDeath");
                        if (shield && shield.enabled) LeanTween.color(shield.rectTransform, Color.black, 1.5f).setDelay(1);
                        LeanTween.color(image.rectTransform, Color.black, 1.5f).setDelay(1);
                        LeanTween.color(background.GetComponent<RectTransform>(), Color.black, 1).setDelay(1.7f).setOnComplete(() => { GameController3D.instance.onLoose.Invoke(); });
                    }
                }

                // Свечение
                if (useLootColor && colorLight != null)
                {
                    colorLight.SetColor(CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Player).lightColor);
                }
            }
            else
            {
                lblShield.text = "";
                lblAttack.text = "";
                keyIcon.SetActive(false);
            }

            if (_card is Model.Shield)
            {
                lblShield.text = "" + (_card as Model.Shield).shield;
                if (GameController3D.instance.game.player.shieldX2)
                {
                    lblShield.text = "" + (_card as Model.Shield).shield + " x2";
                }

                // Свечение
                if (useLootColor && colorLight != null)
                {
                    colorLight.SetColor(CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Healer).lightColor);
                }
            }

            if (_card is Model.Healer)
            {
                lblHealth.text = "" + (_card as Model.Healer).heal;
                if (GameController3D.instance.game.player.healX2)
                {
                    lblHealth.text = "" + (_card as Model.Healer).heal + " x2";
                }

                // Свечение
                if (useLootColor && colorLight != null)
                {
                    colorLight.SetColor(CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Healer).lightColor);
                }
            }

            if (_card is Model.Blaster)
            {
                lblAttack.text = "" + (_card as Model.Blaster).damage;
            }

            if (card is Model.Barrel)
            {
                //lblShield.text = "" + (_card as Model.Barrel).amount;

                //if ((card as Model.Barrel).amount > 0)
                //{
                //    image.sprite = CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Barrel).image;
                //}
                //else
                //{
                //    image.sprite = CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Barrel).customImage;
                //    PlayFX(View.Card3D.FXType.golds, false);
                //}

                // Свечение
                if (useLootColor && colorLight != null)
                {
                    colorLight.SetColor(CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Barrel).lightColor);
                }
            }

            if (card is Model.Trap)
            {
                //if ((card as Model.Trap).active)
                //    lblAttack.text = "" + (_card as Model.Trap).damage;
                //else
                //    lblAttack.text = "";
                if ((card as Model.Trap).active)
                {
                    lblHealth.text = "" + (_card as Model.Trap).damage;
                    image.sprite = CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Trap).image;
                }
                else
                {
                    lblHealth.text = "";
                    image.sprite = CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Trap).customImage;
                }

                // Свечение
                if (useLootColor && colorLight != null)
                {
                    colorLight.SetColor(CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Trap).lightColor);
                }
            }

            if (card is Model.Bomb)
            {
                //lblAttack.text = "" + (_card as Model.Bomb).damage;
                lblHealth.text = "" + (_card as Model.Bomb).damage;
                // Свечение
                if (useLootColor && colorLight != null)
                {
                    colorLight.SetColor(CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Bomb).lightColor);
                }
            }

            if (_card is Model.Money)
            {
                lblHealth.text = "" + (_card as Model.Money).money;
                // Свечение
                if (useLootColor && colorLight != null)
                {
                    colorLight.SetColor(CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Money).lightColor);
                }

                PlayFX(FXType.light, -1);
            }

            if (showCardName)
            {
                lblName.text = _card.name;
            }


            if (colorLight != null)
            {
                if (_card is Model.Animate && _card is Model.Player)
                {
                    colorLight.image.enabled = true;
                    colorLight.SetColor(0);
                }
                else if (_card is Model.Animate && _card is Model.Shield == false
                    && _card is Model.Money == false
                    && _card is Model.Bomb == false
                    && _card is Model.Trap == false
                    && _card is Model.Barrel == false)
                {
                    colorLight.image.enabled = true;
                    colorLight.SetColor((_card as Model.Animate).health);
                }
                else if (offLight)
                {
                    colorLight.image.enabled = false;
                }
            }
        }

        public void Arrows(bool active, float time)
        {
            if (arrows == null) return;
            List<Transform> list = new List<Transform>(arrows.GetComponentsInChildren<Transform>(true));
            list.Remove(arrows.transform);

            if (active)
            {
                List<Coord> testCoord = new List<Coord>();
                testCoord.Add(new Coord(card.coord.x - 1, card.coord.y));
                testCoord.Add(new Coord(card.coord.x, card.coord.y + 1));
                testCoord.Add(new Coord(card.coord.x + 1, card.coord.y));
                testCoord.Add(new Coord(card.coord.x, card.coord.y - 1));

                arrows.SetActive(true);

                for (int i = 0; i < testCoord.Count; i++)
                {
                    //Debug.Log(".");
                    if (GameController3D.instance.game.ValidateCoord(testCoord[i].x, testCoord[i].y))
                    {
                        //Debug.Log("!");
                        //if (GameController3D.instance.game.ValidateMove(testCoord[i].x, testCoord[i].y) == false)
                        {
                            //Debug.Log("!!");
                            
                            int id = i;
                            RectTransform rt = list[id].GetComponent<RectTransform>();
                            if (LeanTween.isTweening(rt) == false)
                            {
                                list[id].gameObject.SetActive(true);
                                LeanTween.alpha(rt, 0, 0.01f).setOnComplete(() => LeanTween.alpha(rt, 1, time).setOnComplete(() => LeanTween.alpha(rt, 0, time).setDelay(time)));
                            }
                        }
                    }
                }

            }
            else
            {
                arrows.SetActive(false);
                for (int i = 0; i < list.Count; i++)
                {
                    list[i].gameObject.SetActive(false);
                }
            }
        }

		void OnMouseDown ()
        {
			if (onClick != null)
				onClick (this);
		}

		public IEnumerator Disappear (float delay = 0)
        {
			yield return new WaitForSeconds (delay);

			float scaleFactor = 1;
			Vector3 startScale = transform.localScale;
			Vector3 scale = startScale;

			while (scaleFactor > 0)
            {
				scaleFactor -= 4 * Time.deltaTime;
				if (scaleFactor < 0) scaleFactor = 0;
				scale = scaleFactor * startScale;
				transform.localScale = scale;

				yield return null;
			}

			Destroy (gameObject);
		}

		public IEnumerator Appear (Vector3 endScale, float delay = 0)
        {
			yield return new WaitForSeconds (delay);

			float scaleFactor = 0;
			Vector3 startScale = transform.localScale;
			Vector3 scale = startScale;

			while (scaleFactor < 1)
            {
				scaleFactor += 4 * Time.deltaTime;
				if (scaleFactor > 1) scaleFactor = 1;
				scale = scaleFactor * endScale;
				transform.localScale = scale;

				yield return null;
			}
		}

		public void Move (Vector3 pos, float delay = 0)
        {
			StopCoroutine ("MoveCoroutine");
			StartCoroutine (MoveCoroutine (pos, delay));
		}

		private bool moving;
		private bool stop;
		private IEnumerator MoveCoroutine (Vector3 pos, float delay = 0)
        {
			if (moving) {
				stop = true;
				while (stop)
					yield return null;
			}

			moving = true;

			yield return new WaitForSeconds (delay);

			while (!stop && Vector3.Distance(transform.position, pos) > 0.01f) {

				transform.position = Vector3.Lerp (transform.position, pos, 10 * Time.deltaTime);

				yield return null;
			}

			transform.position = pos;

			stop = false;

			moving = false;
		}

        public void ChangeSpriteNow (Sprite s)
        {
            image.sprite = s;
        }

        public void ChangeSprite (Sprite s)
        {
			StartCoroutine (ChangeSpriteCoroutine (s));
		}

		public IEnumerator ChangeSpriteCoroutine (Sprite s)
        {
			float targetYRot = 90;

			float yRot = 0;

			while (Mathf.Abs (targetYRot - yRot) > 0.1f) {

				yRot = Mathf.Lerp (yRot, targetYRot, 20 * Time.deltaTime);
				Vector3 rot = transform.rotation.eulerAngles;
				rot.y = yRot;
				transform.rotation = Quaternion.Euler (rot);

				yield return null;
			}

			image.sprite = s;

			targetYRot = 0;

			while (Mathf.Abs (targetYRot - yRot) > 0.1f) {

				yRot = Mathf.Lerp (yRot, targetYRot, 20 * Time.deltaTime);
				Vector3 rot = transform.rotation.eulerAngles;
				rot.y = yRot;
				transform.rotation = Quaternion.Euler (rot);

				yield return null;
			}
		}
	}
}