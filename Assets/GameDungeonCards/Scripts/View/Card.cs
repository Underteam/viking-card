﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DC.View {
	
	public class Card : MonoBehaviour {

		private Model.Card _card;
		public Model.Card card {
			get {
				return _card;
			}

			set {
				//Debug.LogWarning ("Assign " + (_card == null ? "null" : _card.coord.ToString ()) + " -> " + (value == null ? "null" : value.coord.ToString ()));
				_card = value;
				UpdateCard ();
			}
		}

		public SpriteRenderer image;

		public TextMesh lblHealth;

		public TextMesh lblShield;

		public TextMesh lblAttack;

		public TextMesh lblName;

		public GameObject keyIcon;

		public System.Action<Card> onClick;

		// Use this for initialization
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}

		public void UpdateCard () {

			if (image == null)
				Debug.LogError ("SR destroyed " + gameObject, gameObject);

			if (_card == null)
				Debug.Log ("Card is null", this);
			
			image.sprite = _card.image;

			lblShield.text = "0";
			lblAttack.text = "0";

			if (_card is Model.Animate) {
				lblHealth.text = "" + (_card as Model.Animate).health;
			} else {
				lblHealth.text = "";
			}

			if (_card is Model.Player) {
				if ((_card as Model.Player).shield != null) lblShield.text = "" + (_card as Model.Player).shield.shield;
				if ((_card as Model.Player).blaster != null) lblAttack.text = "" + (_card as Model.Player).blaster.damage;
				if ((_card as Model.Player).numKeys > 0) keyIcon.SetActive (true);
				else keyIcon.SetActive (false);
			} else {
				lblShield.text = "";
				lblAttack.text = "";
				keyIcon.SetActive (false);
			}

			if (_card is Model.Shield) {
				lblShield.text = "" + (_card as Model.Shield).shield;
			}

			if (_card is Model.Healer) {
				lblHealth.text = "" + (_card as Model.Healer).heal;
			}

			if (_card is Model.Blaster) {
				lblAttack.text = "" + (_card as Model.Blaster).damage;
			}

			if (card is Model.Barrel) {
				lblShield.text = "" + (_card as Model.Barrel).amount;
			}

			if (card is Model.Trap) {
				if ((card as Model.Trap).active)
					lblAttack.text = "" + (_card as Model.Trap).damage;
				else
					lblAttack.text = "";
			}

			lblName.text = _card.name;
		}

		void OnMouseDown () {
		
			if (onClick != null)
				onClick (this);
		}

		public IEnumerator Disappear (float delay = 0) {
		
			yield return new WaitForSeconds (delay);

			float scaleFactor = 1;
			Vector3 startScale = transform.localScale;
			Vector3 scale = startScale;

			while (scaleFactor > 0) {
			
				scaleFactor -= 2 * Time.deltaTime;
				if (scaleFactor < 0) scaleFactor = 0;
				scale = scaleFactor * startScale;
				transform.localScale = scale;

				yield return null;
			}

			Destroy (gameObject);
		}

		public IEnumerator Appear (Vector3 endScale, float delay = 0) {
		
			yield return new WaitForSeconds (delay);

			float scaleFactor = 0;
			Vector3 startScale = transform.localScale;
			Vector3 scale = startScale;

			while (scaleFactor < 1) {

				scaleFactor += 2 * Time.deltaTime;
				if (scaleFactor > 1) scaleFactor = 1;
				scale = scaleFactor * endScale;
				transform.localScale = scale;

				yield return null;
			}
		}

		public void Move (Vector3 pos, float delay = 0) {
		
			StopCoroutine ("MoveCoroutine");
			StartCoroutine (MoveCoroutine (pos, delay));
		}

		private bool moving;
		private bool stop;
		private IEnumerator MoveCoroutine (Vector3 pos, float delay = 0) {

			if (moving) {
				stop = true;
				while (stop)
					yield return null;
			}

			moving = true;

			yield return new WaitForSeconds (delay);

			while (!stop && Vector3.Distance(transform.position, pos) > 0.01f) {

				transform.position = Vector3.Lerp (transform.position, pos, 5 * Time.deltaTime);

				yield return null;
			}

			transform.position = pos;

			stop = false;

			moving = false;
		}

		public void ChangeSprite (Sprite s) {
		
			StartCoroutine (ChangeSpriteCoroutine (s));
		}

		public IEnumerator ChangeSpriteCoroutine (Sprite s) {
		
			float targetYRot = 90;

			float yRot = 0;

			while (Mathf.Abs (targetYRot - yRot) > 0.1f) {

				yRot = Mathf.Lerp (yRot, targetYRot, 10 * Time.deltaTime);
				Vector3 rot = transform.rotation.eulerAngles;
				rot.y = yRot;
				transform.rotation = Quaternion.Euler (rot);

				yield return null;
			}

			image.sprite = s;

			targetYRot = 0;

			while (Mathf.Abs (targetYRot - yRot) > 0.1f) {

				yRot = Mathf.Lerp (yRot, targetYRot, 10 * Time.deltaTime);
				Vector3 rot = transform.rotation.eulerAngles;
				rot.y = yRot;
				transform.rotation = Quaternion.Euler (rot);

				yield return null;
			}
		}
	}
}