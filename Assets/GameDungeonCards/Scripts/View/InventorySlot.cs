﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC
{
    public class InventorySlot : MonoBehaviour
    {
        public int ind;

        private Vector3 myScale;

        public SpriteRenderer rend;

        private Collider2D col;

        public TextMesh lblIndex;

        // Use this for initialization
        void Start()
        {
            myScale = transform.localScale;

            rend = GetComponent<SpriteRenderer>();

            col = GetComponent<Collider2D>();
        }

        public IEnumerator Disappear(float delay = 0)
        {
            yield return new WaitForSeconds(delay);

            float scaleFactor = 1;
            Vector3 startScale = transform.localScale;
            Vector3 scale = startScale;

            while (scaleFactor > 0)
            {

                scaleFactor -= 2 * Time.deltaTime;
                if (scaleFactor < 0) scaleFactor = 0;
                scale = scaleFactor * startScale;
                transform.localScale = scale;

                yield return null;
            }

            Destroy(gameObject);
        }

        public IEnumerator Appear(float delay = 0)
        {
            yield return new WaitForSeconds(delay);

            float scaleFactor = 0;
            Vector3 startScale = transform.localScale;
            Vector3 scale = startScale;

            while (scaleFactor < 1)
            {

                scaleFactor += 2 * Time.deltaTime;
                if (scaleFactor > 1) scaleFactor = 1;
                scale = scaleFactor * myScale;
                transform.localScale = scale;

                yield return null;
            }
        }

        public void Hide()
        {
            rend.enabled = false;
            col.enabled = false;
            gameObject.SetActive(false);
        }

        public void Show()
        {
            rend.enabled = true;
            col.enabled = true;
            gameObject.SetActive(true);
        }

        public void Droped()
        {
            GameController.instance.CardDroped(ind, transform.position);

            Destroy(gameObject);
        }
    }
}