﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DC.View {
	
	public class GameField3D : MonoBehaviour {

		private Dictionary<Model.Card, View.Card3D> cards = new Dictionary<Model.Card, View.Card3D>();

		private Card3D[,] field;

		public Vector2 cellSize;

		private Model.GameField game;

		public Card3D cardPrefab;
        public Transform cardParent;

		public System.Action<Card3D> onCardClicked;

		private Vector3 shift;

        public Vector3 offset;

        [Header("UI")]
        public bool showCardNames = true;
        public Text textMoney;
        public Text textStep;

        public void UpdateMoney(int value)
        {
            if (textMoney) textMoney.text = value.ToString();
        }

        public void Clear()
        {
            if (this.game == null) return;

            foreach (var card in cards.Values)
            {
                Destroy(card.gameObject);
            }
            cards.Clear();
            field = new Card3D[game.sizeX, game.sizeY];
        }

		public void Init (Model.GameField game)
        {
			this.game = game;

			foreach (var card in cards.Values) {
				Destroy (card.gameObject);
			}

			cards.Clear ();

			field = new Card3D[game.sizeX, game.sizeY];

			shift = new Vector3 (-(game.sizeX / 2f - 0.5f) * cellSize.x, -(game.sizeY / 2f - 1.0f) * cellSize.y, 0) + offset;

			for (int y = 0; y < game.sizeY; y++) {
				for (int x = 0; x < game.sizeX; x++) {
					Card3D card = (Card3D)Instantiate (cardPrefab);
                    if (cardParent) card.transform.parent = cardParent;
                    card.gameObject.name = "Card " + x + " " + y;
					card.gameObject.SetActive (true);
					card.card = game.field [x, y]; //Debug.Log ("Instantiate card " + x + " " + y + " " + game.field [x, y].coord.x + " " + game.field [x, y].coord.y);
					card.onClick = OnCardClicked;
					card.transform.position = new Vector3 (x * cellSize.x, y * cellSize.y, 0) + shift;
					cards.Add (card.card, card);
					field [x, y] = card;
                }
			}

			UpdateState ();

            // Обновляем деньги
            UpdateMoney(game.player.money);
        }

        public void Init(Model.GameField game, ref List<AnimationDescription> animations)
        {
            this.game = game;

            foreach (var card in cards.Values)
            {
                Destroy(card.gameObject);
            }

            cards.Clear();

            field = new Card3D[game.sizeX, game.sizeY];

            shift = new Vector3(-(game.sizeX / 2f - 0.5f) * cellSize.x, -(game.sizeY / 2f - 1.0f) * cellSize.y, 0) + offset;

            animations = new List<AnimationDescription>();

            for (int y = 0; y < game.sizeY; y++)
            {
                for (int x = 0; x < game.sizeX; x++)
                {
                    Card3D card = (Card3D)Instantiate(cardPrefab);
                    if (cardParent) card.transform.parent = cardParent;
                    card.gameObject.name = "Card " + x + " " + y;
                    card.gameObject.SetActive(true);
                    card.card = game.field[x, y]; //Debug.Log ("Instantiate card " + x + " " + y + " " + game.field [x, y].coord.x + " " + game.field [x, y].coord.y);
                    card.onClick = OnCardClicked;
                    card.transform.position = new Vector3(x * cellSize.x, y * cellSize.y, 0) + shift;
                    cards.Add(card.card, card);
                    field[x, y] = card;
                    int _x = x;
                    int _y = y;
                    animations.Add(new AnimationDescription(AnimationDescription.Type.CardAppear, new List<object>() { card.card, game.field[x, y].coord }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { game.time_Delay }));
                }
            }

            UpdateState();

            // Обновляем деньги
            UpdateMoney(game.player.money);
        }

        public void UpdateState ()
        {
			for (int y = 0; y < game.sizeY; y++) {
				for (int x = 0; x < game.sizeX; x++) {
					if (field[x, y] == null) continue;
					field [x, y].card = game.field [x, y];
					cards [game.field [x, y]] = field [x, y];
				}
			}

			foreach (var card in cards.Values) {
				if (card == null) {
					int i = 0;
					foreach (var kvp in cards)
						if (kvp.Value == null)
							Debug.LogError (i + " Card is null " + kvp.Key + " " + cards.Values.Count);
						else
							i++;
				}
				card.UpdateCard ();
			}
		}

		public void PreUpdateState ()
        {
			foreach (var card in cards.Values) {
                card.showCardName = showCardNames;
                card.UpdateCard ();
			}
		}

		public void OnCardClicked (Card3D card)
        {
			if (onCardClicked != null)
				onCardClicked (card);

            // Обновляем деньги
            UpdateMoney(game.player.money);

        }

		public Card3D GetCard(Coord coord)
        {
			return field [coord.x, coord.y];
		}

		public Vector3 WorldCoord (Coord coord)
        {
			return new Vector3 (coord.x * cellSize.x, coord.y * cellSize.y, 0) + shift;
		}

		public Coord FieldCoord (Vector3 pos)
        {
			pos -= shift;

			int x = (int)(0.5f + pos.x / cellSize.x);
			int y = (int)(0.5f + pos.y / cellSize.y);

			if (pos.x < -cellSize.x/2f) x = (int)(-0.5f + pos.x / cellSize.x);
			if (pos.y < -cellSize.y/2f) y = (int)(-0.5f + pos.y / cellSize.y);

			Debug.Log (pos + " x " + cellSize + " -> " + x + " " + y);

			return new Coord (x, y);
		}

		public Card3D CreateCard (Model.Card card, Coord pos)
        {
			int x = card.coord.x;
			int y = card.coord.y;

			Card3D vCard = (Card3D)Instantiate (cardPrefab);
            if (cardParent) vCard.transform.parent = cardParent;
            vCard.gameObject.name = "Card " + x + " " + y;
			vCard.gameObject.SetActive (true);
			vCard.card = card;
			vCard.onClick = OnCardClicked;
			vCard.transform.position = WorldCoord(pos);
			vCard.transform.localScale = Vector3.zero;
			if (field [x, y] != null) {
				Debug.LogError ("Destroy " + field [x, y]);
				Destroy (field [x, y].gameObject);
			}
			field[x, y] = vCard;
			cards.Add (card, vCard);

			return vCard;
		}

		public void RemoveCard(Coord coord)
        {
			cards.Remove (field [coord.x, coord.y].card);
			field [coord.x, coord.y] = null;
		}

		public void RemoveCard(Card card)
        {
			cards.Remove (card.card);
			field [card.card.coord.x, card.card.coord.y] = null;
		}

		public void SwapCards(Coord c1, Coord c2)
        {
			Card3D card = field [c1.x, c1.y];
			field [c1.x, c1.y] = field [c2.x, c2.y];
			field [c2.x, c2.y] = card;

			field [c1.x, c1.y].name = "Card " + c1.x + " " + c1.y;
			field [c2.x, c2.y].name = "Card " + c2.x + " " + c2.y;
		}

		public void MoveCard (Card3D card, Coord from, Coord to)
        {
			field [from.x, from.y] = null;
			field [to.x, to.y] = card;
			card.name = "Card " + to.x + " " + to.y;
		}

		public void ChangeCard(Coord coord, Model.Card card)
        {
			Card3D vCard = field [coord.x, coord.y];
			Model.Card key = null;
			foreach (var kvp in cards)
				if (kvp.Value == vCard)
					key = kvp.Key;

			if (key != null) {
				cards.Remove (key);
				cards.Add (card, vCard);
			} else {
				Debug.LogError ("Not found");
			}
		}

		public bool IsOutOfBounds (Coord coord)
        {
			int x = coord.x;
			int y = coord.y;
			return !(x >= 0 && x < game.sizeX && y >= 0 && y < game.sizeY);
		}

        public View.Card3D GetPlayerCard()
        {
            for (int y = 0; y < field.GetLength(0); y++)
            {
                for (int x = 0; x < field.GetLength(1); x++)
                {
                    if (field[x, y].card.type == CardTypeDC.CardType.Player) return field[x, y];
                }
            }
            return null;
        }
    }
}