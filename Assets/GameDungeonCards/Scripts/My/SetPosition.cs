﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosition : MonoBehaviour {

    public Vector3[] positions;

	public void Set(int id)
    {
        transform.position = positions[Mathf.Clamp(id, 0, positions.Length)];
    }
}
