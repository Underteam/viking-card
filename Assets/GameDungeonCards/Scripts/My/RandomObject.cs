﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomObject : MonoBehaviour {

    public GameObject[] objects;

    [EditorButton]
    public void Activate ()
    {
        //Debug.Log("RandomObject Acivated");
        if (objects.Length > 0)
        {
            int id = Random.Range(0, objects.Length);
            //Debug.Log(id);
            for (int i = 0; i < objects.Length; i++)
            {
                if (i < objects.Length)
                {
                    objects[i].SetActive(i == id);
                    ParticleSystem ps = objects[i].GetComponentInChildren<ParticleSystem>();
                    if (ps) ps.Play();
                }
            }
        }
    }

    [EditorButton]
    public void DeActivate()
    {
        //Debug.Log("RandomObject DeAcivated");
        for (int i = 0; i < objects.Length; i++)
        {
            if (i < objects.Length)
            {
                objects[i].SetActive(false);
            }
        }
    }
}
