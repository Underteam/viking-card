﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorByLevel : MonoBehaviour {

    [System.Serializable]
	public struct ColorRange
    {
        public Vector2Int range;
        public Color color;
    }

    public List<ColorRange> list;
    public Image image;


    public int testID = 0;
    public Color color;
    public bool useAlpha = false;
    public float alpha = 50;

    [EditorButton]
    public void GetColor(int id)
    {
        color = GetColorByID(id);
        if (useAlpha) color.a = alpha / 255;
    }

    [EditorButton]
    public void SetColor()
    {
        image.color = color;
    }

    public void SetColor(int id)
    {
        GetColor(id);
        image.color = color;
    }

    public void SetColor(Color c)
    {
        color = c;
        SetColor();
    }

    public Color GetColorByID(int id)
    {
        id = Mathf.Clamp(id, 0, list[list.Count - 1].range.y);
        for (int i = 0; i < list.Count; i++)
        {
            if (id >= list[i].range.x && id <= list[i].range.y) return list[i].color;
        }

        return new Color(0, 0, 0, 0); //Color.white;
    }
}
