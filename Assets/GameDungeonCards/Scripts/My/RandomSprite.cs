﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomSprite : MonoBehaviour {

    public Image image;
    public Sprite[] sprites;
    public GameObject[] effects;

	void Start ()
    {
        if (image == null) image = GetComponent<Image>();
        if (image)
        {
            int id = Random.Range(0, sprites.Length);
            image.sprite = sprites[id];
            for (int i = 0; i < effects.Length; i++)
            {
                if (i < sprites.Length)
                {
                    effects[i].SetActive(i == id);
                }
            }
        }
    }
}
