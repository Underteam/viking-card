﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using GameCore;
using GameCore.StatsSystem;

public class MyGame : GameCore.Game
{

    private static MyGame _instance;
    public static MyGame instance { get { if (_instance == null) _instance = FindObjectOfType<MyGame>(); return _instance; } }

    public RectTransform fader;

    public GameObject[] objectsToStart;

    public GameObject BtnFinishContinue;
    public GameObject BtnAction;

    // Возрождался ли уже игрок
    public bool revived { get; set; }

    private void Awake()
    {

    }

    public void AddShield()
    {
        if (IAPNoAds.noAds)
        {
            DC.GameController3D.instance.game.AddShield();
        }
    }


    #region Base

    public override void StartGame(bool newGame)
    {
        //// Выключаем 
        //for (int i = 0; i < objectsToStart.Length; i++)
        //{
        //    if (objectsToStart[i]) objectsToStart[i].SetActive(false);
        //}
        //for (int i = 0; i < objectsToStart.Length; i++)
        //{
        //    if (objectsToStart[i]) objectsToStart[i].SetActive(true);
        //}

        // Сбрасываем счет
        //GameCore.GameManager.instance.SetScore(0);
        // Текущий левел

        int score = 0;
        int step = 0;
        if (DC.GameController3D.instance.game != null && DC.GameController3D.instance.game.player != null)
        {
            score = DC.GameController3D.instance.game.player.money;
            step = DC.GameController3D.instance.game.step;
        }

        DC.GameController3D.instance.StartNewGame();

        if (newGame)
        {
            // Сбрасываем возрождение
            revived = false;
            showFinish = false;
        }
        else if (newGame == false)
        {
            DC.GameController3D.instance.game.player.money = score;
            DC.GameController3D.instance.game.step = step;
            Debug.Log(string.Format("Score={0} | Step={1}", DC.GameController3D.instance.game.player.money, DC.GameController3D.instance.game.step));
        }

        // Обновляем деньги
        DC.GameController3D.instance.gameField.UpdateMoney(DC.GameController3D.instance.game.player.money);

        Time.timeScale = 1;
        StateManager.instance.ChangeState(StateManager.States.Game);
    }

    public override void RestartGame()
    {
        GameCore.GameManager.instance.StartGame(true);
        StateManager.instance.ChangeState(StateManager.States.Game);
    }

    public override void ContinueGame()
    {
        revived = true;
        GameManager.instance.StartGame(false);
        StateManager.instance.ChangeState(StateManager.States.Game);
        //GameManager.instance.ResumeGame();
    }

    // Победа
    public override void Win()
    {
        // Инициализируем попап на победу
        GameManager.instance.panelWin.GetComponent<PanelFinish>().Init(PanelFinish.State.Win);
        // Меняем состояние
        StateManager.instance.ChangeState(StateManager.States.Finish);

        //if (currentLevel == LevelManager.instance.lastOpennedLevel) LevelManager.instance.lastOpennedLevel++;
    }

    public bool showFinish { get; set; }

    // Проигрыш
    public override void Loose()
    {
        Debug.Log("Loose");

        DC.GameController3D.instance.StopGame();
        Camera.main.GetComponent<SetPosition>().Set(1);
        GameManager.instance.panelGame.Hide();


        if (revived == false)
        {
            GameManager.instance.panelContinue.Show();
        }

        if (showFinish) //GameManager.instance.panelContinue == null || GameManager.instance.panelContinue && GameManager.instance.panelContinue.gameObject.activeInHierarchy == false)
        {
            //Debug.Log("!!!!! SHOW FINISH");
            // Инициализируем попап на проигрыш
            PanelFinish panelFinish = GameManager.instance.panelFinish; //panelLoose.GetComponent<PanelFinish>();
            if (panelFinish)
            {
                panelFinish.Init(PanelFinish.State.Loose);
                panelFinish.InitScore(GameManager.instance.score, GameManager.instance.GetHighScore());
                GameManager.instance.panelLoose.Show();
            }
            revived = false;
        }

        //// Выключаем игровые объекты
        //for (int i = 0; i < objectsToStart.Length; i++)
        //{
        //    if (objectsToStart[i]) objectsToStart[i].SetActive(false);
        //}

        //// Инициализируем попап на проигрыш
        //GameManager.instance.panelLoose.GetComponent<PanelFinish>().Init(PanelFinish.State.Loose);
        //GameManager.instance.panelLoose.GetComponent<PanelFinish>().InitScore(GameManager.instance.score, GameManager.instance.GetHighScore());
        //// Меняем состояние
        //StateManager.instance.ChangeState(StateManager.States.Finish);
    }

 

    #endregion


    #region Game



    #endregion // Game

    #region Ads

    public override bool useAdsAction { get { return StateManager.instance.previousState.state == StateManager.States.NewGame || StateManager.instance.previousState.state == StateManager.States.RestartGame; } }

    public override bool AddItem(string itemName, int count = 1)
    {
        //Debug.Log(string.Format("MyGame Add item id{0} ({1})", itemName, count));
        switch (itemName)
        {
            case "AdsHealX2":
                {
                    DC.GameController3D.instance.game.player.healX2 = true;
                    DC.GameController3D.instance.UpdateState();
                    //Debug.Log("AdsHealX2");
                    Time.timeScale = 1;
                    return true;
                }
            case "AdsShieldX2":
                {
                    DC.GameController3D.instance.game.player.shieldX2 = true;
                    DC.GameController3D.instance.UpdateState();
                    //Debug.Log("AdsShieldX2");
                    Time.timeScale = 1;
                    return true;
                }
            default:
                break;
        }

        // Деньги
        //{
        //Debug.Log("Add Gold!");
        //GameCore.ShopSystem.Shop.AddMoney(GameCore.GameManager.instance.rewardCount, true);
        //}


        return false;
    }

    #endregion
}


