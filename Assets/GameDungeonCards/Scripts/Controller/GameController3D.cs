﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC
{
    public class GameController3D : MonoBehaviour
    {
        private static GameController3D _instance;
        public static GameController3D instance { get { if (_instance == null) _instance = FindObjectOfType<GameController3D>(); return _instance; } set { _instance = value; } }

        public bool debugLog = false;

        public View.GameField3D gameField;
        public Vector2Int gameFieldSize = new Vector2Int(3, 3);

        /*private*/ public Model.GameField game;

        private Model.Player player;

        public List<InventorySlot> slots;

        public HackPanel miniGame;

        public UnityEngine.Events.UnityEvent onLoose;

        void Awake()
        {
            instance = this;
        }

        void Start()
        {
            //StartNewGame();
        }

        [EditorButton]
        public void StartNewGame()
        {
            player = (Model.Player)CardsDatabase.instance.GetPlayerCard();

            //game = new Model.GameField(3, 3, ref player);
            game = new Model.GameField(gameFieldSize.x, gameFieldSize.y, ref player);

            List<AnimationDescription> animations = new List<AnimationDescription>();
            //gameField.Init(game, ref animations);
            gameField.Init(game);

            gameField.onCardClicked = OnCardClicked;

            for (int i = 0; i < slots.Count; i++)
            {
                slots[i].ind = i;
                slots[i].Hide();
            }

            //View.Card3D card3D = gameField.GetCard(new Coord(1, 1));
            //card3D.Arrows(true, 0.3f);
            gameField.GetPlayerCard().Arrows(true, 0.3f);
        }

        public void StopGame()
        {
            gameField.Clear();
        }

        public void PreUpdateState()
        {
            gameField.PreUpdateState();

            for (int i = 0; i < slots.Count; i++)
            {
                if (game.GetCardInSlot(i) == null) continue;
                slots[i].lblIndex.text = "" + game.GetCardInSlot(i).GetIndex();
                slots[i].rend.sprite = CardsDatabase.instance.GetCardDescription(game.GetCardInSlot(i).index).image;
            }
        }

        public void UpdateState()
        {
            game.UpdateState();

            gameField.UpdateState();

            for (int i = 0; i < slots.Count; i++)
            {
                if (game.GetCardInSlot(i) == null) continue;
                slots[i].lblIndex.text = "" + game.GetCardInSlot(i).GetIndex();
                slots[i].rend.sprite = CardsDatabase.instance.GetCardDescription(game.GetCardInSlot(i).index).image;
            }
        }

        public void OnCardClicked(View.Card3D card)
        {
            if (debugLog) Debug.Log("Card clicked " + card.name + " " + (card.card != null ? (card.card.coord.x + " " + card.card.coord.y) : " False"), card);

            // Начинаем новую игру если кликаем по умершему игроку
            //if (card.card is Model.Player && (card.card as Model.Player).health == 0) StartNewGame();

            if (waitingForAnimationsEnd) return;

            List<AnimationDescription> animations = game.CellClicked(card.card.coord.x, card.card.coord.y);
            //Debug.Log("animations count = " + animations.Count);
            if (animations.Count > 0) StartCoroutine(PlayAnimations(animations));

            GameCore.GameManager.instance.score = player.money;
        }

        private bool waitingForAnimationsEnd;

        private IEnumerator PlayAnimations(List<AnimationDescription> animations)
        {
            waitingForAnimationsEnd = true;

            PreUpdateState();

            for (int i = 0; i < animations.Count; i++)
            {
                switch (animations[i].type)
                {
                    case AnimationDescription.Type.RemoveCard:
                        {
                            Coord coord = (Coord)animations[i].data[1];
                            View.Card3D vCard = gameField.GetCard(coord);
                            if (debugLog) Debug.Log("RemoveCard " + coord + " " + (vCard != null), vCard);
                            gameField.RemoveCard(coord);
                            Destroy(vCard.gameObject);
                            break;
                        }
                    case AnimationDescription.Type.Delay:
                        {
                            if (debugLog) Debug.Log("Delay " + (float)animations[i].data[0]);
                            yield return new WaitForSeconds((float)animations[i].data[0]);
                            break;
                        }
                    case AnimationDescription.Type.CardDisappear:
                        {
                            Coord coord = (Coord)animations[i].data[1];
                            View.Card3D vCard = gameField.GetCard(coord);
                            if (debugLog) Debug.Log("CardDisappear " + coord, vCard);
                            gameField.RemoveCard(coord);
                            StartCoroutine(vCard.Disappear());
                            break;
                        }
                    case AnimationDescription.Type.CardAppear:
                        {
                            Model.Card card = (Model.Card)animations[i].data[0];
                            Coord coord = (Coord)animations[i].data[1];
                            View.Card3D vCard = gameField.CreateCard(card, coord);
                            if (debugLog) Debug.Log("CardAppear " + coord, vCard);
                            StartCoroutine(vCard.Appear(Vector3.one));
                            break;
                        }
                    case AnimationDescription.Type.CardMove:
                        {
                            Coord coord1 = (Coord)animations[i].data[1];
                            Coord coord2 = (Coord)animations[i].data[2];
                            Model.Card card = (Model.Card)animations[i].data[0];
                            if (gameField.IsOutOfBounds(coord1))
                            {
                                View.Card3D vCard = gameField.GetCard(card.coord);
                                if (debugLog) Debug.Log("CardMove " + card.coord, vCard);
                                gameField.MoveCard(vCard, coord2, coord2);
                                vCard.Move(gameField.WorldCoord(coord2));
                            }
                            else
                            {
                                View.Card3D vCard = gameField.GetCard(coord1);
                                if (debugLog) Debug.Log("CardMove " + coord1 + " -> " + coord2, vCard);
                                gameField.MoveCard(vCard, coord1, coord2);
                                vCard.Move(gameField.WorldCoord(coord2));
                            }

                            break;
                        }
                    case AnimationDescription.Type.MoveToSlot:
                        {
                            Coord coord = (Coord)animations[i].data[1];
                            View.Card3D vCard = gameField.GetCard(coord);
                            if (debugLog) Debug.Log("MoveToSlot " + coord, vCard);
                            int slotInd = (int)animations[i].data[2];
                            Vector3 pos = slots[slotInd].transform.position;
                            vCard.name += " MTS";
                            vCard.Move(pos);
                            break;
                        }
                    case AnimationDescription.Type.ShowSlot:
                        {
                            int slotInd = (int)animations[i].data[0];
                            if (debugLog) Debug.Log("ShowSlot " + slotInd);
                            slots[slotInd].Show();
                            break;
                        }
                    case AnimationDescription.Type.HideSlot:
                        {
                            int slotInd = (int)animations[i].data[0];
                            if (debugLog) Debug.Log("HideSlot " + slotInd);
                            slots[slotInd].Hide();
                            break;
                        }
                    case AnimationDescription.Type.ChangeCard:
                        {
                            Model.Card card = (Model.Card)animations[i].data[0];
                            if (debugLog) Debug.Log("ChangeCard " + card.coord + " " + card);
                            View.Card3D vCard = gameField.GetCard(card.coord);
                            //yield return vCard.ChangeSpriteCoroutine (card.image);
                            vCard.ChangeSprite(card.image);
                            gameField.ChangeCard(card.coord, card);
                            break;
                        }
                    case AnimationDescription.Type.MiniGame:
                        {
                            Model.Chest chest = (Model.Chest)animations[i].data[0];
                            Coord coord = (Coord)animations[i].data[1];
                            miniGame.Run((b) =>
                            {
                                miniGameRunning = false;
                                List<AnimationDescription> toAdd = ChestHacked(chest, b);
                                animations.AddRange(toAdd);
                                if (debugLog) Debug.Log("Added animations:");
                                for (int j = 0; j < toAdd.Count; j++)
                                    Debug.Log(j + " " + toAdd[j].type);
                                //UnityEditor.EditorApplication.isPaused = true;
                            });
                            yield return MiniGame();
                            break;
                        }
                    case AnimationDescription.Type.OpenChest:
                        {
                            Model.Card card = (Model.Card)animations[i].data[0];
                            if (debugLog) Debug.Log("OpenChest " + card.coord + " " + card);
                            View.Card3D vCard = gameField.GetCard(card.coord);
                            vCard.ChangeSpriteNow((Sprite)animations[i].data[2]);
                            vCard.PlayFX(View.Card3D.FXType.golds, 1);
                            break;
                        }
                }
            }

            if (debugLog) Debug.Log("Update State");

            UpdateState();

            waitingForAnimationsEnd = false;
        }

        private bool miniGameRunning;
        private IEnumerator MiniGame()
        {
            miniGameRunning = true;

            while (miniGameRunning) yield return null;
        }

        private List<AnimationDescription> ChestHacked(Model.Chest chest, bool success)
        {
            return game.ChestHacked(chest, success);
        }

        [EditorButtonAttribute]
        public void Print()
        {
            game.Print();
        }

        public void CardDroped(int fromSlot, Vector3 pos)
        {
            if (waitingForAnimationsEnd) return;

            Coord coord = gameField.FieldCoord(pos);

            if (debugLog) Debug.Log("Card from slot " + fromSlot + " droped at " + coord);

            List<AnimationDescription> animations = game.UseItemFromInventoryOnCell(fromSlot, coord);

            StartCoroutine(PlayAnimations(animations));
        }
    }
}