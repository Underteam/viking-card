﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC
{
    public class CardsDatabase : MonoBehaviour
    {
        private static CardsDatabase _instance;
        public static CardsDatabase instance { get { if (_instance == null) _instance = FindObjectOfType<CardsDatabase>(); return _instance; } set { _instance = value; } }

        public CardData player;

        public List<CardData> enemies;

        public List<CardData> loot;

        public CardData money;

        private List<CardData> allCards = new List<CardData>();

        void Awake()
        {
            if (instance == null) instance = this;
        }

        // Use this for initialization
        void Start()
        {
            allCards.Add(player);
            allCards.AddRange(enemies);
            allCards.AddRange(loot);
            allCards.Add(money);
        }

        public int GetIndex(CardDescription card, List<CardData> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].data.Equals(card)) return i;
            }
            return 0;
        }

        public CardDescription GetCardDescription(int i)
        {
            return allCards[i].data;
        }

        public CardDescription GetCardDescriptionByType(CardTypeDC.CardType cardType, string itemName = null)
        {
            for (int i = 0; i < allCards.Count; i++)
            {
                if (allCards[i].data.type == cardType)
                {
                    if (string.IsNullOrEmpty(itemName) || allCards[i].data.name.Equals(itemName))
                    {
                        return allCards[i].data;
                    }
                }
            }

            return null;
        }

       
        public Model.Card GetEnemyCard(int i, int amount = -1)
        {
            if (enemies == null || i < 0 || enemies.Count < 1 || i >= enemies.Count)
                return null;

            CardDescription cd = enemies[i].data;

            Model.Enemy card = new Model.Enemy(new Coord(-1, -1));
            card.name = cd.name;
            card.image = cd.image;

            card.health = Random.Range(cd.min, cd.max + 1);
            if (amount >= 0) card.health += amount;
            // Максимальное здоровье мобов
            card.health = Mathf.Clamp(card.health, 1, 12);
            if (card.health == 12)
            {
                if (Random.Range(0, 100) % 2 == 0) card.health -= Random.Range(0, 4);
            }

            //if (amount < 0) card.health = Random.Range(cd.min, cd.max + 1);
            //else card.health = amount;
            //card.index = allCards.IndexOf(cd);
            card.index = GetIndex(cd, allCards);
            return card;
        }

        public int GetLootByPercent()
        {
            float total = 0;
            for (int i = 0; i < loot.Count; i++)
            {
                if (loot[i] != null) total += ((float)loot[i].data.percent / 100f);
            }

            //Debug.Log(total);
            float randomPoint = Random.value * total;

            for (int i = 0; i < loot.Count; i++)
            {
                if (randomPoint < loot[i].data.percent / 100f)
                {
                    return i;
                }
                else
                {
                    randomPoint -= loot[i].data.percent / 100f;
                }
            }
            return loot.Count - 1;
        }
            

        public Model.Card GetLootCard(int i, int amount = -1)
        {
            if (loot == null || i < 0 || loot.Count < 1 || i >= loot.Count)
                return null;

            CardDescription cd = loot[i].data;

            //Model.Card card = GetCardModel(cd.type);

            switch (cd.type)
            {
                case CardTypeDC.CardType.Shield:
                    {
                        Model.Shield card = new Model.Shield(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        if (amount < 0) card.shield = Random.Range(cd.min, cd.max + 1);
                        else card.shield = amount;
                        card.itemName = cd.name;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Healer:
                    {
                        Model.Healer card = new Model.Healer(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        if (amount < 0) card.heal = Random.Range(cd.min, cd.max + 1);
                        else card.heal = amount;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Blaster:
                    {
                        Model.Blaster card = new Model.Blaster(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        if (amount < 0) card.damage = Random.Range(cd.min, cd.max + 1);
                        else card.damage = amount;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Chest:
                    {
                        Model.Chest card = new Model.Chest(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Key:
                    {
                        Model.Key card = new Model.Key(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Tesla:
                    {
                        Model.Tesla card = new Model.Tesla(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        if (amount < 0) card.damage = Random.Range(cd.min, cd.max + 1);
                        else card.damage = amount;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Barrel:
                    {
                        Model.Barrel card = new Model.Barrel(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        if (amount < 0) card.amount = Random.Range(cd.min, cd.max + 1);
                        else card.amount = amount;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Trap:
                    {
                        Model.Trap card = new Model.Trap(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        if (amount < 0) card.damage = Random.Range(cd.min, cd.max + 1);
                        else card.damage = amount;
                        card.active = true;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
                case CardTypeDC.CardType.Bomb:
                    {
                        Model.Bomb card = new Model.Bomb(new Coord(-1, -1));
                        card.name = cd.name;
                        card.image = cd.image;
                        if (amount < 0) card.damage = Random.Range(cd.min, cd.max + 1);
                        else card.damage = amount;
                        //card.index = allCards.IndexOf(cd);
                        card.index = GetIndex(cd, allCards);
                        return card;
                        //break;
                    }
            }

            return null;
        }

        //public Model.Card GetCardModel(CardType cardType)
        //{
        //    switch (cardType)
        //    {
        //        case CardType.Player: return new Model.Player(new Coord(-1, -1));
        //        case CardType.Enemy: return new Model.Enemy(new Coord(-1, -1));
        //        case CardType.Shield: return new Model.Shield(new Coord(-1, -1));
        //        case CardType.Healer: return new Model.Healer(new Coord(-1, -1));
        //        case CardType.Blaster: return new Model.Blaster(new Coord(-1, -1));
        //        case CardType.Chest: return new Model.Chest(new Coord(-1, -1));
        //        case CardType.Money: return new Model.Money(new Coord(-1, -1));
        //        case CardType.Key: return new Model.Key(new Coord(-1, -1));
        //        case CardType.Tesla: return new Model.Tesla(new Coord(-1, -1));
        //        case CardType.Barrel: return new Model.Barrel(new Coord(-1, -1));
        //        case CardType.Trap: return new Model.Trap(new Coord(-1, -1));
        //        case CardType.Bomb: return new Model.Bomb(new Coord(-1, -1));
        //        default:
        //            break;
        //    }
        //    return null;
        //}

        public Model.Card GetMoneyCard(int n)
        {
            Model.Money card = new Model.Money(new Coord(-1, -1));
            card.name = money.data.name;
            card.image = money.data.image;
            card.money = n;
            card.index = allCards.IndexOf(money);
            return card;
        }

        public Model.Card GetPlayerCard()
        {
            Model.Player card = new Model.Player(new Coord(-1, -1));
            card.name = player.data.name;
            card.image = player.data.image;
            card.health = Random.Range(player.data.min, player.data.max + 1);
            card.maxHealth = card.health;
            card.index = allCards.IndexOf(player);
            return card;
        }
    }
}