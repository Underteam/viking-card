﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC
{
    [System.Serializable]
    public class CardDescription
    {
        public string name;
        public CardTypeDC.CardType type;
        public Sprite image;
        public int min;
        public int max;
        [Range(0, 100)]
        public int percent;
        // Цвет света
        public Color lightColor = new Color(0, 0, 0, 0);
        // Дополнительное изображение
        public Sprite customImage;
    }

    public class CardTypeDC
    {
        public enum CardType
        {
            Player,
            Enemy,
            Shield,
            Healer,
            Blaster,
            Chest,
            Money,
            Key,
            Tesla,
            Barrel,
            Trap,
            Bomb,
        }
    }

    public class CardData : MonoBehaviour
    {
        public CardDescription data;
    }
}