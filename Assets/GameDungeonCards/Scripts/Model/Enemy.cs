﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class Enemy : Animate {

		public Enemy (Coord coord) : base(coord)
        {
			type = CardTypeDC.CardType.Enemy;
		}

		public override void Apply (Card another)
        {
            if (another is Player)
            {
                int dmg = health;
                Shield shield = (another as Player).shield;
                if (shield != null)
                {
                    int shieldReduce = Mathf.Min(shield.shield, dmg);
                    dmg -= shieldReduce;
                    shield.shield -= shieldReduce;
                }

                int healthReduce = Mathf.Min((another as Player).health, dmg);

                (another as Player).health -= healthReduce;

                // Добавляем монеты за убийство врага, если игрок остался жив
                if ((another as Player).health > 0)
                {
                    (another as Player).money += health; //healthReduce;
                    if (SoundController.instance) SoundController.instance.Play("EnemyDeath");
                }
            }
		}

		public override bool Empty ()
		{
			return health <= 0;
		}

		public override int GetIndex ()
		{
			return health;
		}

		public override void AddToIndex (int am)
		{
			health += am;
			Debug.Log ("Add " + am + " -> " + health);
			if (health < 0) health = 0;
		}

		public override int GetUsefulness ()
		{
			return -1;
		}
	}
}
