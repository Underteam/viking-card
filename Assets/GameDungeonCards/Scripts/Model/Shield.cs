﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class Shield : Inanimate {

		public int shield;
        public string itemName;

		public Shield (Coord coord) : base(coord) {

			type = CardTypeDC.CardType.Shield;
		}

		public override void Apply (Card another) {

            if (another is Model.Player)
            {
                if ((another as Model.Player).shield == null || (another as Model.Player).shield.shield < shield)
                    (another as Player).lastShield = itemName;
            }

		}

		public override bool Empty ()
		{
			return shield <= 0;
		}

		public override int GetIndex ()
		{
			return shield;
		}

		public override void AddToIndex (int am)
		{
			shield += am;

			if (shield < 0) shield = 0;
		}

		public override int GetUsefulness ()
		{
			return 1;
		}
	}
}