﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {

	public class Blaster : Inanimate {

		public int damage;

		public Blaster (Coord coord) : base(coord) {

			type = CardTypeDC.CardType.Blaster;
		}

		public override void Apply (Card another) {

			if (another is Enemy) {

				int toReduce = Mathf.Min (damage, (another as Animate).health);
				(another as Animate).health -= toReduce;
				damage -= toReduce;
			}
		}

		public override bool Empty ()
		{
			return damage <= 0;
		}

		public override int GetIndex ()
		{
			return damage;
		}

		public override void AddToIndex (int am)
		{
			damage += am;

			if (damage < 0) damage = 0;
		}

		public override int GetUsefulness ()
		{
			return 1;
		}
	}
}
