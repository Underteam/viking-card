﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class Barrel : Card {

		public int amount;

		public Barrel (Coord coord) : base(coord) {

			type = CardTypeDC.CardType.Barrel;
		}

		public override void NextTurn () {

			if (amount > 2)
				amount--;
		}

		public Card Open (int currUsefulness)
        {
			if (Random.Range (-5, 6) < currUsefulness) {
				Card card = CardsDatabase.instance.GetEnemyCard (Random.Range (0, CardsDatabase.instance.enemies.Count), amount);
				return card;
			} else {
                if (Random.Range(0, 100) % 3 == 0) return CardsDatabase.instance.GetMoneyCard(Random.Range(CardsDatabase.instance.money.data.min, CardsDatabase.instance.money.data.max + 1));
                //Card card = CardsDatabase.instance.GetLootCard(Random.Range(0, CardsDatabase.instance.loot.Count), amount);
                //int cnt = 100;
                //while (card is Barrel && cnt-- > 0)
                //{
                //    card = CardsDatabase.instance.GetLootCard(Random.Range(0, CardsDatabase.instance.loot.Count), amount);
                //}
                Card card = CardsDatabase.instance.GetLootCard(CardsDatabase.instance.GetLootByPercent());
                int cnt = 100;
                while (card is Barrel && cnt-- > 0)
                {
                    card = CardsDatabase.instance.GetLootCard(CardsDatabase.instance.GetLootByPercent());
                }
                if (card is Barrel)
                {
                    card = CardsDatabase.instance.GetLootCard(Random.Range(0, CardsDatabase.instance.loot.Count), amount);
                    cnt = 100;
                    while (card is Barrel && cnt-- > 0)
                    {
                        card = CardsDatabase.instance.GetLootCard(Random.Range(0, CardsDatabase.instance.loot.Count), amount);
                    }
                }
                return card;
			} 
		}
	}
}
