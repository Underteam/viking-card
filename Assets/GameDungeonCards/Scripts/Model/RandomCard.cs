﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class RandomCard {

        public static Card GetRandomCard(int currUsefulness)
        {
            if (Random.Range(-5, 6) < currUsefulness)
            {
                return CardsDatabase.instance.GetEnemyCard(Random.Range(0, CardsDatabase.instance.enemies.Count));
            }
            else
            {
                if (Random.Range(0, 100) % 3 == 0) return CardsDatabase.instance.GetMoneyCard(Random.Range(CardsDatabase.instance.money.data.min, CardsDatabase.instance.money.data.max + 1));
                return CardsDatabase.instance.GetLootCard(Random.Range(0, CardsDatabase.instance.loot.Count));
            }
        }


        public static Card GetRandomCard(int currUsefulness, int step)
        {
            int healthModiff = 0;
            if (step % 5 == 0) healthModiff = (step / 5);

            //Debug.Log(string.Format("health = {0} | step = {1} | currUsefulness = {2}", healthModiff, step, currUsefulness));

            if (Random.Range(-5, 6) < currUsefulness)
            {
                return CardsDatabase.instance.GetEnemyCard(Random.Range(0, CardsDatabase.instance.enemies.Count), healthModiff);
            }
            else
            {
                if (Random.Range(0, 100) % 3 == 0) return CardsDatabase.instance.GetMoneyCard(Random.Range(CardsDatabase.instance.money.data.min, CardsDatabase.instance.money.data.max + 1 + healthModiff));
                return CardsDatabase.instance.GetLootCard(CardsDatabase.instance.GetLootByPercent());
                //return CardsDatabase.instance.GetLootCard(Random.Range(0, CardsDatabase.instance.loot.Count));
            }
        }
    }
}
