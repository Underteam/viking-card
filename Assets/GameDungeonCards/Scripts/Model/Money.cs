﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class Money : Inanimate {

		public Money (Coord coord) : base(coord) {

			type = CardTypeDC.CardType.Money;
		}

		public int money;

        public override void Apply(Card another)
        {
            if (another is Player)
            {
                (another as Player).money += money;
                if (SoundController.instance) SoundController.instance.Play("Money");
            }
        }

        public override int GetUsefulness ()
		{
			return 1;
		}
	}
}
