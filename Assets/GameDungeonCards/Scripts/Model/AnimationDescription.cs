﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC
{
    //Описание анимации (уменьшение карточки, поворот карточки, перемещение карточки, и т.д.)
    public class AnimationDescription
    {

        public enum Type
        {
            RemoveCard,
            CardDisappear,
            CardAppear,
            CardMove,
            Delay,
            MoveToSlot,
            ShowSlot,
            HideSlot,
            ChangeCard,
            MiniGame,
            OpenChest,
        }

        public Type type;

        public List<object> data;

        //public Model.Card card;

        //public Coord coord1;

        //public Coord coord2;

        //public float floatParam;

        //public AnimationDescription (Type type, Model.Card card = null, Coord c1 = default(Coord), Coord c2 = default(Coord), float fp = 0)
        public AnimationDescription(Type type, List<object> data)
        {

            this.type = type;
            this.data = data;
            //this.card = card;
            //this.coord1 = c1;
            //this.coord2 = c2;
            //this.floatParam = fp;
        }
    }

}