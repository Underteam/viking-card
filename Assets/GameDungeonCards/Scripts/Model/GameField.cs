﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class GameField {

		public class Field {
			
			public Card[,] field;
		
			private int sizeX;
			private int sizeY;

			public Field(int sx, int sy) {

				sizeX = sx;
				sizeY = sy;

				field = new Card[sx, sy];
			}

			public Card this[int i1, int i2] {

				get {
					if (i1 < 0 || i1 >= sizeX || i2 < 0 || i2 >= sizeY) Debug.LogError ("Index out of range " + i1 + " " + i2 + " " + sizeX + " " + sizeY);
					return field [i1, i2];
				}

				set {
					//Debug.Log ("Assign " + i1 + " " + i2 + " (" + (field [i1, i2] == null ? "null" : field [i1, i2].coord.ToString()) + ") -> (" + (value == null ? "null" : value.coord.ToString()) + ")");
					field [i1, i2] = value;
					if (field [i1, i2] != null) field [i1, i2].coord.Set (i1, i2);
					//Debug.Log ("After: " + field [i1, i2].coord);
				}
			}

			/*public Card this[Coord coord] {

				get {
					return this [coord.x, coord.y];
				}

				set {
					this [coord.x, coord.y] = value;
				}
			}//*/
		}

		public Field field;

		public Player player;

        public int step = 0;

		public int sizeX { get; private set; }
		public int sizeY { get; private set; }

		private List<InventorySlot> slots;

        public float time_Delay = 0.1f;
        public float time_Move = 1f;

		private class MoveDirection {

			public int dx;
			public int dy;

			public MoveDirection (int x1, int y1, int x2, int y2) {

				dx = Mathf.Clamp(x2-x1, -1, 1);
				dy = Mathf.Clamp(y2-y1, -1, 1);

				//Debug.Log ("Move dir " + x1 + " " + y1 + " " + x2 + " " + y2 + " -> " + (x2-x1) + " " + (y2-y1) + " " + dx + " " + dy);
			}
		}

		public GameField (int sizeX, int sizeY, ref Player player) {
		
			if (sizeX < 3) sizeX = 3;
			if (sizeY < 3) sizeY = 3;
			if (sizeX > 11) sizeX = 11;
			if (sizeY > 11) sizeY = 11;

			this.sizeX = sizeX;
			this.sizeY = sizeY;

			field = new Field(sizeX, sizeY);

			int x = sizeX / 2;
			int y = sizeY / 2;

			if (player == null) {
				player = new Player (new Coord(x, y));
			}

			this.player = player;

			field [x, y] = player;
			//player.coord.Set(x, y);

			for(y = 0; y < sizeY; y++) {
				for (x = 0; x < sizeX; x++) {
					if (field [x, y] == null) {
						CalculateStatistics ();
						field [x, y] = RandomCard.GetRandomCard (usefulness, 0);
					}
				}
			}

            slots = new List<InventorySlot>();
            slots.Add(new InventorySlot(InventorySlot.Type.Blaster));
            slots.Add(new InventorySlot(InventorySlot.Type.Healer));
            slots.Add(new InventorySlot(InventorySlot.Type.Universal));

            if (IAPNoAds.noAds) AddShield();
        }

        public void AddShield()
        {
            Shield shield = CardsDatabase.instance.GetLootCard(6) as Shield;
            shield.shield = 10;
            slots[1].card = shield;
            shield.Apply(player);
            player.shield = (Shield)slots[1].card;
        }

		public void UpdateState ()
        {
			for (int x = 0; x < sizeX; x++) {
				for (int y = 0; y < sizeY; y++) {
					if(field[x, y] != null) field [x, y].NextTurn ();
				}
			}
		}

		public List<AnimationDescription> UseItemFromInventoryOnCell (int invSlot, Coord coord) {

            Debug.LogWarning("Use item");

			List<AnimationDescription> animations = new List<AnimationDescription> ();

			Card card = slots [invSlot].card;

			if (coord.y < 0) {
				if (invSlot == 2) {
					int ind = (int)coord.x;
					if (ind < 0) ind = 0;
					if (ind >= slots.Count)	ind = slots.Count - 1;
					Debug.Log (invSlot + " -> " + ind + " " + (card is Blaster) + " " + (card is Shield));
					if ((ind == 0 && card is Blaster) || (ind == 1 && card is Shield)) {
						slots [ind].card = slots [invSlot].card;
						slots [invSlot].card = null;

						player.blaster = (Blaster)slots [0].card;
						player.shield = (Shield)slots [1].card;

						animations.Add (new AnimationDescription (AnimationDescription.Type.ShowSlot, new List<object>(){ ind }));
						animations.Add (new AnimationDescription (AnimationDescription.Type.HideSlot, new List<object> (){ invSlot }));
					}
				}

				return animations;
			}

			if (invSlot == 2 && !(card is Healer)) return animations;

			if (card is Blaster) {
			
				if (!ValidateCoord (coord.x, coord.y))
					return animations;//если указаны неправельные координаты - выходим

				if (!ValidateMove (coord.x, coord.y))
					return animations;//если движение на эту клетку запрещено - выходим
			}

			Card useOn = field [coord.x, coord.y];

			int am = useOn.GetIndex ();//столько добавить денег

			card.Apply (useOn);

			if (card.Empty ()) {
				slots [invSlot].card = null;
				animations.Add (new AnimationDescription (AnimationDescription.Type.HideSlot, new List<object> (){ invSlot }));
			}

			if (useOn.Empty ()) {
				//field [coord.x, coord.y] = CardsDatabase.instance.GetMoneyCard (am);
				//animations.Add (new AnimationDescription (AnimationDescription.Type.ChangeCard, new List<object> (){ field [coord.x, coord.y] }));
				animations.AddRange (CellClicked (coord.x, coord.y));
			}

			return animations;
		}

		public List<AnimationDescription> ChestHacked (Chest chest, bool success) {
		
			List<AnimationDescription> animations = new List<AnimationDescription> ();

			if (success) {
				CalculateStatistics ();
				Card card = chest.Open (usefulness);
				field [chest.coord.x, chest.coord.y] = card;
				animations.Add (new AnimationDescription (AnimationDescription.Type.ChangeCard, new List<object> (){ card }));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

			} else {

				player.health -= 1;
				if (player.health <= 0) {
					int x = player.coord.x;
					int y = player.coord.y;
					for (int j = 0; j < sizeY; j++) {
						for (int i = 0; i < sizeX; i++) {
							if (i == x && j == y) continue;
							if (field [i, j] == null) continue;
							animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){field[i, j], field[i, j].coord}));
						}
					}

					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

					animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, new Coord (sizeX / 2, sizeY / 2)}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

					MoveCard (x, y, sizeX / 2, sizeY / 2);

				} else {
				
					animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){chest, chest.coord}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

					animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, chest.coord}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

					RefillAnimations (chest.coord.x, chest.coord.y, animations);
				}
			}

			return animations;
		}

		public List<AnimationDescription> CellClicked (int x, int y) {

			//Debug.Log ("Cell clicked (" + x + ", " + y + ") " + ValidateCoord (x, y) + " " + ValidateMove (x, y));

			List<AnimationDescription> animations = new List<AnimationDescription> ();


			if (!ValidateCoord (x, y))
				return animations;//если указаны неправельные координаты - выходим
			
			if (!ValidateMove (x, y))
            {
                // Показываем стрелки
                GameController3D.instance.gameField.GetPlayerCard().Arrows(true, 0.3f);

                return animations;//если движение на эту клетку запрещено - выходим
            }
				
                       

			//MoveDirection moveDir = new MoveDirection (player.coord.x, player.coord.y, x, y);//направление движения игрока

			Card card = field [x, y];

            bool useInventorySlots = false;

			if (card is Blaster) {
			
				if (slots[0].card == null) {

					slots [0].card = card;

					animations.Add (new AnimationDescription (AnimationDescription.Type.MoveToSlot, new List<object>(){card, card.coord, 0}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.ShowSlot, new List<object>(){0}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.RemoveCard, new List<object>(){card, card.coord}));

				} else if (slots[2].card == null) {
				
					slots [2].card = card;

					animations.Add (new AnimationDescription (AnimationDescription.Type.MoveToSlot, new List<object>(){card, card.coord, 2}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.ShowSlot, new List<object>(){2}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.RemoveCard, new List<object>(){card, card.coord}));

				} else if (slots[0].card is Blaster && (slots [0].card as Blaster).damage < (card as Blaster).damage) {
					
					slots [0].card = card;

					animations.Add (new AnimationDescription (AnimationDescription.Type.MoveToSlot, new List<object>(){card, card.coord, 0}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.ShowSlot, new List<object>(){0}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.RemoveCard, new List<object>(){card, card.coord}));

				} else if (slots[2].card is Blaster && (slots [2].card as Blaster).damage < (card as Blaster).damage) {
				
					slots [2].card = card;

					animations.Add (new AnimationDescription (AnimationDescription.Type.MoveToSlot, new List<object>(){card, card.coord, 2}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.ShowSlot, new List<object>(){2}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.RemoveCard, new List<object>(){card, card.coord}));
				
				} else {
				
					animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){card, card.coord}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));
				}

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));

			}
            else if (card is Shield)
            {
                if (player.shieldX2)
                {
                    (card as Shield).shield *= 2;
                    player.shieldX2 = false;
                    Debug.Log("shieldX2 activated!");
                }

                if (SoundController.instance) SoundController.instance.Play("TakeItem");

                if (slots[1].card == null)
                {
                    slots[1].card = card;

                    if (useInventorySlots) animations.Add(new AnimationDescription(AnimationDescription.Type.MoveToSlot, new List<object>() { card, card.coord, 1 }));
                    if (useInventorySlots) animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.ShowSlot, new List<object>() { 1 }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.RemoveCard, new List<object>() { card, card.coord }));

                }
                else if (useInventorySlots && slots[2].card == null)
                {
                    slots[2].card = card;

                    animations.Add(new AnimationDescription(AnimationDescription.Type.MoveToSlot, new List<object>() { card, card.coord, 2 }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.ShowSlot, new List<object>() { 2 }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.RemoveCard, new List<object>() { card, card.coord }));

                }
                else if (slots[1].card is Shield && (slots[1].card as Shield).shield < (card as Shield).shield)
                {
                    slots[1].card = card;

                    if (useInventorySlots) animations.Add(new AnimationDescription(AnimationDescription.Type.MoveToSlot, new List<object>() { card, card.coord, 1 }));
                    if (useInventorySlots) animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.ShowSlot, new List<object>() { 1 }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.RemoveCard, new List<object>() { card, card.coord }));

                }
                else if (useInventorySlots && slots[2].card is Shield && (slots[2].card as Shield).shield < (card as Shield).shield)
                {
                    slots[2].card = card;

                    animations.Add(new AnimationDescription(AnimationDescription.Type.MoveToSlot, new List<object>() { card, card.coord, 2 }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.ShowSlot, new List<object>() { 2 }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.RemoveCard, new List<object>() { card, card.coord }));

                }
                else
                {
                    animations.Add(new AnimationDescription(AnimationDescription.Type.CardDisappear, new List<object>() { card, card.coord }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                }

                animations.Add(new AnimationDescription(AnimationDescription.Type.CardMove, new List<object>() { player, player.coord, card.coord }));
                animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));

                
                card.Apply(player);
                
            }
            else if (card is Healer)
            {
                if (SoundController.instance) SoundController.instance.Play("TakeHeal");
                if (player.healX2)
                {
                    (card as Healer).heal *= 2;
                    player.healX2 = false;
                    Debug.Log("healX2 activated!");
                }
                if (useInventorySlots)
                {
                    if (slots[2].card == null)
                    {
                        slots[2].card = card;

                        animations.Add(new AnimationDescription(AnimationDescription.Type.MoveToSlot, new List<object>() { card, card.coord, 2 }));
                        animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                        animations.Add(new AnimationDescription(AnimationDescription.Type.ShowSlot, new List<object>() { 2 }));
                        animations.Add(new AnimationDescription(AnimationDescription.Type.RemoveCard, new List<object>() { card, card.coord }));
                    }
                    else if (slots[2].card is Healer && (slots[2].card as Healer).heal < (card as Healer).heal)
                    {
                        slots[2].card = card;

                        animations.Add(new AnimationDescription(AnimationDescription.Type.MoveToSlot, new List<object>() { card, card.coord, 2 }));
                        animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                        animations.Add(new AnimationDescription(AnimationDescription.Type.ShowSlot, new List<object>() { 2 }));
                        animations.Add(new AnimationDescription(AnimationDescription.Type.RemoveCard, new List<object>() { card, card.coord }));
                    }
                    else
                    {
                        card.Apply(player);

                        animations.Add(new AnimationDescription(AnimationDescription.Type.CardDisappear, new List<object>() { card, card.coord }));
                        animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                    }
                } else
                {
                    card.Apply(player);
                    GameController3D.instance.gameField.GetPlayerCard().PlayFX(View.Card3D.FXType.heal);

                    animations.Add(new AnimationDescription(AnimationDescription.Type.CardDisappear, new List<object>() { card, card.coord }));
                    animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
                }

                animations.Add(new AnimationDescription(AnimationDescription.Type.CardMove, new List<object>() { player, player.coord, card.coord }));
                animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));

                
            }
            else if(card is Enemy)
            {
				card.Apply (player);

				if (player.shield != null && player.shield.shield <= 0) {
					player.shield = null;
					slots [1].card = null;
					animations.Add (new AnimationDescription (AnimationDescription.Type.HideSlot, new List<object> (){ 1 }));
				}

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){card, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

			}
            else if (card is Money)
            { 
				card.Apply (player);

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){card, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

			}
            else if (card is Chest)
            { 
				if (player.numKeys > 0) {
					player.numKeys--;
					CalculateStatistics ();
					Card newCard = (card as Chest).Open (usefulness);
                    field [card.coord.x, card.coord.y] = newCard;
					animations.Add (new AnimationDescription (AnimationDescription.Type.ChangeCard, new List<object> (){ newCard }));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object> (){ time_Delay }));
				} else {
					animations.Add (new AnimationDescription (AnimationDescription.Type.MiniGame, new List<object> (){ card, card.coord }));
				}
				return animations;
			}
            else if (card is Key)
            { 
				card.Apply (player);

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){card, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));

			}
            else if(card is Tesla)
            {
				animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){card, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));

				RefillAnimations (x, y, animations);

				Tesla tesla = card as Tesla;

				bool addDelay = false;

				for (int i = player.coord.x-1; i <= player.coord.x+1; i++) {
					int j = player.coord.y;
					if (!ValidateCoord (i, j)) continue;
					if (tesla.HasEffectOn (field [i, j])) {
						Debug.Log ("Apply to " + field [i, j]);
						field [i, j].AddToIndex (-tesla.GetIndex ());
						if (field [i, j].Empty ()) {
							Card newCard = CardsDatabase.instance.GetMoneyCard(tesla.GetIndex());
							field [i, j] = newCard;
							animations.Add (new AnimationDescription (AnimationDescription.Type.ChangeCard, new List<object> (){ newCard }));
							addDelay = true;
						}
					}
				}

				for (int j = player.coord.y-1; j <= player.coord.y+1; j++) {
					int i = player.coord.x;
					if (!ValidateCoord (i, j)) continue;
					if (tesla.HasEffectOn (field [i, j])) {
						Debug.Log ("Apply to " + field [i, j]);
						field [i, j].AddToIndex (-tesla.GetIndex ());
						if (field [i, j].Empty ()) {
							Card newCard = CardsDatabase.instance.GetMoneyCard(tesla.GetIndex());
							field [i, j] = newCard;
							animations.Add (new AnimationDescription (AnimationDescription.Type.ChangeCard, new List<object> (){ newCard }));
							addDelay = true;
						}
					}
				}

				if(addDelay) animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object> (){ 0.5f }));

				return animations;
			}
            else if (card is Barrel)
            {
                if (SoundController.instance) SoundController.instance.Play("OpenChest");

                CalculateStatistics ();
				Card newCard = (card as Barrel).Open (usefulness);
                field [card.coord.x, card.coord.y] = newCard;
				//Debug.Log ("New card is " + newCard + " " + newCard.coord);

                animations.Add(new AnimationDescription(AnimationDescription.Type.OpenChest, new List<object>() { card, card.coord, CardsDatabase.instance.GetCardDescriptionByType(CardTypeDC.CardType.Barrel).customImage }));
                animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { 1f }));
                animations.Add (new AnimationDescription (AnimationDescription.Type.ChangeCard, new List<object> (){ newCard }));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object> (){ time_Delay }));

				return animations;
			}
            else if(card is Trap)
            {
				card.Apply (player);

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){card, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, card.coord}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));
			}
            else if (card is Bomb)
            {
                if (SoundController.instance) SoundController.instance.Play("TakePoison");
                card.Apply(player);
                GameController3D.instance.gameField.GetPlayerCard().PlayFX(View.Card3D.FXType.poison);

                animations.Add(new AnimationDescription(AnimationDescription.Type.CardDisappear, new List<object>() { card, card.coord }));
                animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));

                animations.Add(new AnimationDescription(AnimationDescription.Type.CardMove, new List<object>() { player, player.coord, card.coord }));
                animations.Add(new AnimationDescription(AnimationDescription.Type.Delay, new List<object>() { time_Delay }));
            }
            else {

				Debug.LogError ("Not implimented!");
			}

            // Шаги
            step++;
            Debug.Log("CurrentStep = " + step);

            //if (useInventorySlots)
            {
                player.blaster = (Blaster)slots[0].card;
                player.shield = (Shield)slots[1].card;
            }

			RefillAnimations (x, y, animations);
			
			return animations;
		}

		private void RefillAnimations (int x, int y, List<AnimationDescription> animations) {
		
			MoveDirection moveDir = new MoveDirection (player.coord.x, player.coord.y, x, y);//направление движения игрока

			Card card = field [x, y];

			int px = player.coord.x;
			int py = player.coord.y;
			MoveCard (px, py, x, y);//перемещаем игрока

			if (player.health <= 0) {

				for (int j = 0; j < sizeY; j++) {
					for (int i = 0; i < sizeX; i++) {
						if (i == x && j == y) continue;
						if (field [i, j] == null) continue;
						animations.Add (new AnimationDescription (AnimationDescription.Type.CardDisappear, new List<object>(){field[i, j], field[i, j].coord}));
					}
				}

				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){player, player.coord, new Coord (sizeX / 2, sizeY / 2)}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

				MoveCard (x, y, sizeX / 2, sizeY / 2);

			} else {

				//перемещаем карты в соответствии с ходом игрока
				card = GetPrev (px, py, ref moveDir, false);//карта за игроком
				/*if (card == null) {//если за игроком - граница поля, то ищем карту против часовой стрелки
					Debug.LogWarning ("Player on border");
					Coord c = GetCounterclockwiseCoord (px, py);
					card = field [c.x, c.y];
					moveDir = new MoveDirection (c.x, c.y, px, py);//новое направление движения
				}//*/

				int cx = card.coord.x;
				int cy = card.coord.y;

				while (card != null) {
					cx = card.coord.x;
					cy = card.coord.y;
					animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){card, card.coord, new Coord (px, py)}));
					animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));
					MoveCard (cx, cy, px, py);//перемещаем карту
					card = GetPrev (cx, cy, ref moveDir, true);//следующая карта
					px = cx;
					py = cy;
				}

				//Debug.Log ("Create card at " + cx + " " + cy);
				CalculateStatistics ();
				field [cx, cy] = RandomCard.GetRandomCard(usefulness, step);
				//field [cx, cy].coord.Set (cx, cy);

				animations.Add (new AnimationDescription (AnimationDescription.Type.CardAppear, new List<object>(){field [cx, cy], new Coord (cx, cy)}));
				animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){ time_Delay }));

				//animations.Add (new AnimationDescription (AnimationDescription.Type.CardMove, new List<object>(){field [cx, cy], new Coord(-1, sizeY-1), new Coord (cx, cy)}));
				//animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));
				//animations.Add (new AnimationDescription (AnimationDescription.Type.Delay, new List<object>(){0.5f}));
			}
		}

		//private int playerPower;
		//private int numUsefulItems;
		//private int numEnemies;
		//private int numDoubtfulItems;
		private int usefulness = 0;
		public void CalculateStatistics () {
		
			usefulness = 0;
			for (int x = 0; x < sizeX; x++) {
				for (int y = 0; y < sizeY; y++) {
					if (field [x, y] == null) continue;
					usefulness += field [x, y].GetUsefulness ();
				}
			}
			usefulness += 3;
		}

		private void MoveCard(int x1, int y1, int x2, int y2) {
		
			//Debug.Log ("Move card (" + x1 + ", " + y1 + ") -> (" + x2 + ", " + y2 + ")");

			Card c = field [x1, y1];
			field [x1, y1] = null;
			if(field [x2, y2] != null) field [x2, y2].coord.Set (-1, -1);
			field [x2, y2] = c;
			c.coord.Set (x2, y2);
		}

		//возвращает координату, следующую за указанной при движении против часовой стрелки (указанная координата должна лежать на границе поля)
		private Coord GetCounterclockwiseCoord(int x, int y) {
		
			Coord res = new Coord(x, y);

			if (x == 0) {

				if (y == 0)
					res.Set(res.x + 1, res.y);
				else
					res.Set(res.x, res.y - 1);
				
			} else if (x == sizeX - 1) {
			
				if (y == sizeY - 1)
					res.Set(res.x - 1, res.y);
				else
					res.Set(res.x, res.y + 1);
				
			} else if (y == 0) {

				if (x == sizeX - 1)
					res.Set(res.x, res.y + 1);
				else
					res.Set(res.x + 1, res.y);
				
			} else {
			
				if (x == 0)
					res.Set(res.x, res.y - 1);
				else
					res.Set(res.x - 1, res.y);
			}

			return res;
		}

		//возвращает координату, следующую за указанной при движении по часовой стрелке (указанная координата должна лежать на границе поля)
		private Coord GetClockwiseCoord(int x, int y) {

			Coord res = new Coord(x, y);

			if (x == 0) {

				if (y == sizeY-1)
					res.Set(res.x + 1, res.y);
				else
					res.Set(res.x, res.y + 1);

			} else if (x == sizeX - 1) {

				if (y == 0)
					res.Set(res.x - 1, res.y);
				else
					res.Set(res.x, res.y - 1);

			} else if (y == 0) {

				if (x == 0)
					res.Set(res.x, res.y + 1);
				else
					res.Set(res.x - 1, res.y);

			} else {

				if (x == sizeX-1)
					res.Set(res.x, res.y - 1);
				else
					res.Set(res.x + 1, res.y);
			}

			return res;
		}

		private void RefillField (MoveDirection moveDir) {
		

		}

		public bool ValidateMove(int x, int y) {
		
			int px = player.coord.x;
			int py = player.coord.y;

			int dx = Mathf.Abs (px - x);
			int dy = Mathf.Abs (py - y);

			if (dx > 1)
				return false;

			if (dy > 1)
				return false;

			if (dx == 0 && dy == 0)
				return false;

			if (dx == 1 && dy == 1)
				return false;

			return true;
		}

		public bool ValidateCoord(int x, int y) {
		
			if(x < 0 || x >= sizeX || y < 0 || y >= sizeY) return false;

			return true;
		}

		private Card GetNext(int x, int y, MoveDirection dir) {
		
			int nx = x + dir.dx;
			int ny = y + dir.dy;

			if (ValidateCoord (nx, ny))
				return field [nx, ny];
			
			return null;
		}

		private Card GetPrev(int x, int y, ref MoveDirection dir, bool allowNull) {

			int nx = x - dir.dx;
			int ny = y - dir.dy;

			if (ValidateCoord (nx, ny))
				return field [nx, ny];

			if (allowNull) return null;

			bool clockwise = false;

			if (dir.dx > 0) {
				if (y == 0) {
					clockwise = true;
				}
			} else if (dir.dx < 0) {
				if (y == sizeY - 1) {
					clockwise = true;
				}
			} else if (dir.dy > 0) {
				if (x == sizeX - 1) {
					clockwise = true;
				}
			} else {
				if (x == 0) {
					clockwise = true;
				}
			}

			Coord c;
			if (clockwise) {
				c = GetClockwiseCoord (x, y);
			} else {
				c = GetCounterclockwiseCoord (x, y);
			}

			dir = new MoveDirection (c.x, c.y, x, y);//новое направление движения

			return field [c.x, c.y];
		}

		public int NumCards () {

			return sizeX * sizeY;
		}

		public Card GetCardInSlot(int ind) {
		
			return slots [ind].card;
		}

		public void Print () {
		
			for (int y = 0; y < sizeY; y++) {
				for (int x = 0; x < sizeX; x++) {
					Debug.Log (x + " " + y + " -> " + field [x, y].coord.x + " " + field [x, y].coord.y + " : " + field [x, y].name);
				}
			}
		}
	}
}
