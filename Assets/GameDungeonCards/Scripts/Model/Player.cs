﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {

	public class Player : Animate {

		public Shield shield;

		public Blaster blaster;

		public int numKeys;
        public string lastShield;

        public bool healX2 = false;
        public bool shieldX2 = false;

        private int _money;
        public int money {
            get { return _money; }
            set { _money = value; }
        }

		public Player (Coord coord) : base(coord) {

			type = CardTypeDC.CardType.Player;
		}
	}
}