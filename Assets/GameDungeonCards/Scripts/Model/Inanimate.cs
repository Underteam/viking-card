﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {

	public class Inanimate : Card {

		public Inanimate (Coord coord) : base(coord) {
		}
	}
}