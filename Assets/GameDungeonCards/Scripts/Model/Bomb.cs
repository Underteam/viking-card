﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class Bomb : Card {

		public int damage;

		public int cntdwn;

		public Bomb (Coord coord) : base(coord) {

			type = CardTypeDC.CardType.Bomb;
		}

		public bool HasEffectOn(Card card) {

			if (card is Enemy) return true;
			if (card is Blaster) return true;
			if (card is Shield) return true;
			if (card is Healer) return true;

			return false;
		}

        public override void Apply(Card another)
        {
            if (another is Player)
            {
                (another as Player).health -= damage;
                if ((another as Player).health < 0)
                    (another as Player).health = 0;
            }
        }

        public override int GetIndex ()
		{
			return damage;
		}

		public override void NextTurn ()
		{
			cntdwn--;
		}

		public override int GetUsefulness ()
		{
			return -1;
		}
	}
}
