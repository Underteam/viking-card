﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DC.Model {
	
	public class Card {

		//private Coord _coord;
		public Coord coord;
		/*{ 
			get {
				return _coord;
			}

			private set {
				Debug.Log ("Set card coord " + _coord + " -> " + value);
				_coord = value;
			}
		}//*/

		public string name;

		public CardTypeDC.CardType type;

		public Sprite image;

		public int index;

		public Card (Coord coord) {
		
			this.coord = coord;
		}

		public virtual void Apply (Card another) {

		}

		public virtual bool Empty () {
		
			return false;
		}

		public virtual int GetIndex () {

			return 0;
		}

		public virtual void AddToIndex (int am) {
		
		}

		public virtual void NextTurn () {
		
		}

		public virtual int GetUsefulness () {
		
			return 0;
		}
	}
}
