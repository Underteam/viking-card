﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class AdsController : MonoBehaviour {

	public List<AdsProvider> providers;

	private static AdsController instance;

	private delegate void OnUpdate();

	public AdsSettings adsSettings;

	private bool ready;

	public Action onVideoFinished;

	public bool enableLogs = false;

	private bool noAds;

	public GameObject dummyads;

	public DummyAd banner;

	public DummyAd fullbanner;

	private Action<bool> dummyCallback;

	public static AdsController Instance()
    {

        if (instance == null)
        {
            GameObject go = new GameObject("AdsController");
			instance = go.AddComponent<AdsController>();
        }

        return instance;
    }

    void Awake()
    {
		SceneManager.sceneLoaded += (Scene arg0, LoadSceneMode arg1) => {
			OnLevelWasLoaded2 (arg0.buildIndex);
		};

        if (instance != null && instance != this) {
			DestroyImmediate(this.gameObject);
			return;
		}
        else instance = this;

		DontDestroyOnLoad (instance.gameObject);

		//showing = new Dictionary<AdsProvider.AdType, bool> ();
    }

	// Use this for initialization
	void Start () {
	    
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		if(!ready) StartCoroutine (InitializeCoroutine ());

		#if UNITY_EDITOR
		if(dummyads) dummyads.SetActive(true);
		if(banner) banner.gameObject.SetActive(false);
		if(fullbanner) fullbanner.gameObject.SetActive(false);
		#endif
	}
		
	private IEnumerator InitializeCoroutine () {
	
		float timer = 30;
		while (adsSettings != null && !adsSettings.ready && timer > 0) {
			timer -= Time.unscaledDeltaTime;
			yield return null;
		}

		for (int i = 0; i < providers.Count; i++)
			providers [i].SetSettings (adsSettings);

		for (int i = 0; i < providers.Count; i++)
			providers [i].Init ();

		//OnLevelWasLoaded2 (Application.loadedLevel);
		OnLevelWasLoaded2 (SceneManager.GetActiveScene().buildIndex);

		ready = true;

		//if (!noAds) ShowAd (AdsProvider.AdType.banner);
	}

	// Update is called once per frame
	void Update () {

		if (!ready) return;

		if (shouldShowBanner && IsAdReady (AdsProvider.AdType.banner)) {
			shouldShowBanner = false;
			ShowAd (AdsProvider.AdType.banner);
		}
		
		for (int i = 0; i < providers.Count; i++)
			if (!providers [i].enabled)
				continue;
			else if (providers [i].update != null)
				providers [i].update ();
	}

	//private Dictionary<AdsProvider.AdType, bool> showing = new Dictionary<AdsProvider.AdType, bool> ();

	public void NoAds () {
	
		noAds = true;
		HideBanner ();
	}

	public bool IsReady () {
	
		return ready;
	}

	public bool IsNoAds () {
	
		return noAds;
	}

	private int count = 0;

	public void ShowAd(AdsProvider.AdType type, System.Action<bool> onFinish = null) {

		if (noAds && type != AdsProvider.AdType.unskipablevideo) {
			if (onFinish != null) onFinish (true);
			return;
		}

		Logger.Instance ().Log ("Show ad " + type + " " + ready);

		if (!ready) return;

		#if UNITY_EDITOR
		dummyCallback = onFinish;
		if(type == AdsProvider.AdType.banner) {
			if(banner != null) banner.Show(0);
		} else if(type == AdsProvider.AdType.unskipablevideo) {
			if(fullbanner != null) fullbanner.Show(5);
		} else {
			if(fullbanner != null) fullbanner.Show(0);
		}
		return;
		#endif

		//if (!showing.ContainsKey (type)) showing.Add (type, false);
			
		if(enableLogs) Logger.Instance().Log ("ShowAd " + type + " " + " " + (onFinish == null));

		if (count % 2 == 0) {
			for (int i = 0; i < providers.Count; i++) {
				if (!providers [i].enabled) continue;
				if (!providers [i].onEven) continue;
				if (UnityEngine.Random.Range(0f, 100f) < providers[i].chance && providers [i].IsAdReady (type)) {
					if(enableLogs) Logger.Instance().Log("Show " + providers [i].provider);
					providers [i].ShowAd (type, onFinish);
					count++;
					return;
				}
			}
		} else {
			for (int i = 0; i < providers.Count; i++) {
				if (!providers [i].enabled) continue;
				if (!providers [i].onOdd) continue;
				if (UnityEngine.Random.Range(0f, 100f) < providers[i].chance && providers [i].IsAdReady (type)) {
					if(enableLogs) Logger.Instance().Log("Show " + providers [i].provider);
					providers [i].ShowAd (type, onFinish);
					count++;
					return;
				}
			}
		}

		/*for (int i = 0; i < providers.Count; i++) {
			if (!providers [i].enabled) continue;
			if (UnityEngine.Random.Range(0f, 100f) < providers[i].chance && providers [i].IsAdReady (type)) {
				Logger.Instance().Log("Show " + providers [i].provider);
				//showing [type] = true;
				//providers [i].ShowAd (type, (b) => {showing [type] = false; if(onFinish != null) onFinish(b);});
				providers [i].ShowAd (type, onFinish);
				return;
			}
		}//*/

		for (int i = 0; i < providers.Count; i++) {
			if (!providers [i].enabled) continue;
			if (providers [i].IsAdReady (type)) {
				if(enableLogs) Logger.Instance().Log("Show " + providers [i].provider);
				providers [i].ShowAd (type, onFinish);
				count++;
				return;
			}
		}

		if(enableLogs) Logger.Instance ().Log ("Failed " + IsAdReady (type));
	}

	public bool IsAdReady(AdsProvider.AdType type) {

		if (!ready) return false;

		for (int i = 0; i < providers.Count; i++) {
			if (!providers [i].enabled) continue;
			if (providers [i].IsAdReady (type))
				return true;
		}

		return false;
	}

	public bool IsBannerShowing() {
	
		#if UNITY_EDITOR
		if(banner != null) return banner.gameObject.activeSelf;
		#endif

		for (int i = 0; i < providers.Count; i++) {
			if (providers [i].IsBannerShowing ())
				return true;
		}

		return false;
	}

	public void HideBanner () {
	
		for (int i = 0; i < providers.Count; i++)
			providers [i].HideBanner ();

		#if UNITY_EDITOR
		if(banner != null) banner.gameObject.SetActive(false);
		#endif

		shouldShowBanner = false;
	}

	void OnLevelWasLoaded2 (int level) {

		if(enableLogs) Logger.Instance ().Log ("Level was loaded " + adsSettings.ShouldShowTopBanner (level) + " " + adsSettings.ShouldShowInterstitial (level));

		if (adsSettings.ShouldShowTopBanner(level))
			ShowTopBanner ();
		else
			HideBanner ();

		if(adsSettings.ShouldShowInterstitial(level))
			TryToShowLargeBanner();
	}

	public void TryToShowLargeBanner(bool useOwnAds = true) {

		float rnd = UnityEngine.Random.Range (0.0f, 1.0f);
		if(enableLogs)  Logger.Instance ().Log ("Rnd " + rnd);
		if (rnd < 0.3f && IsAdReady (AdsProvider.AdType.interstitial))
			ShowAd (AdsProvider.AdType.interstitial);
		else if (IsAdReady (AdsProvider.AdType.skipablevideo))
			ShowAd (AdsProvider.AdType.skipablevideo);
		else if (IsAdReady (AdsProvider.AdType.interstitial))
				ShowAd (AdsProvider.AdType.interstitial);
			
	}

	public void TryToShowVideoBanner() {

		if (IsAdReady (AdsProvider.AdType.unskipablevideo))
			ShowAd (AdsProvider.AdType.unskipablevideo, (b) => {
                Debug.Log("VideoFinished " + b);
				if(b && onVideoFinished != null) onVideoFinished();
			});
	}

	public void TryToShowVideoBannerSkippable(bool useOwnAds = true) {

		if (IsAdReady (AdsProvider.AdType.skipablevideo))
			ShowAd (AdsProvider.AdType.skipablevideo);
	}

	private bool shouldShowBanner;

	public void ShowTopBanner() {

		shouldShowBanner = true;
	}

	public void DummyAdClosed (bool b) {
	
		if (dummyCallback != null)
			dummyCallback (b);
	}
}
