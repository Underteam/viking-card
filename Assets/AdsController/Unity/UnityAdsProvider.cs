﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityAdsProvider : AdsProvider {

	public int unityAdsGameID = 61740;

	public string unityAdsDefaultZone = "video";

	public string unityAdsRewardedZone = "rewardedVideo";

	private System.Action<bool> onVideoFinished;

	private bool showing = false;

	public override void Init ()
	{

		if (adsSettings != null) {
			unityAdsGameID = adsSettings.GetValue<int> ("UNITYADSGAMEID", unityAdsGameID);
			unityAdsDefaultZone = adsSettings.GetValue<string> ("UADEFAULTZONE", unityAdsDefaultZone);
			unityAdsRewardedZone = adsSettings.GetValue<string> ("UAREWARDEDZONE", unityAdsRewardedZone);
			chance = adsSettings.GetValue<float> (name + "CHANCE", chance);
		}

		UnityAdsSettings settings = (UnityAdsSettings)Resources.Load("UnityAdsSettings");

		if (settings != null) {
			settings.androidGameId = unityAdsGameID.ToString ();
			settings.iosGameId = unityAdsGameID.ToString ();
		}
			
		UnityAdsHelper.Initialize();

		inited = true;

		Logger.Instance().Log ("Unity inited");

	}

	public override void ShowAd (AdType type, System.Action<bool> onFinish = null)
	{

		if (!inited) {
			Init ();
			if(!inited) return;
		}

		if (showing)
			return;

		Logger.Instance().Log ("Try show unity");

		if (type == AdType.skipablevideo && UnityAdsHelper.IsReady (unityAdsDefaultZone)) {
			Logger.Instance().Log ("Ready");
			UnityAdsHelper.onFinished = BannerClosed;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd (unityAdsDefaultZone);
			showing = true;
		
		} else if (type == AdType.unskipablevideo && UnityAdsHelper.IsReady (unityAdsRewardedZone)) {
			Logger.Instance().Log ("Ready");
			onVideoFinished = onFinish;
			UnityAdsHelper.onFinished = VideoFinished;
			UnityAdsHelper.onSkipped = BannerClosed;
			UnityAdsHelper.ShowAd(unityAdsRewardedZone);
			showing = true;
		} else
			Logger.Instance().Log ("Not ready");

	}

	public override bool IsAdReady(AdType type) {

		if (showing)
			return false;
		
		if (type == AdType.skipablevideo) {
			#if UNITY_EDITOR
			return true;
			#else
			return UnityAdsHelper.IsReady (unityAdsDefaultZone);
			#endif
		}

		if (type == AdType.unskipablevideo) {
			#if UNITY_EDITOR
			return true;
			#else
			return UnityAdsHelper.IsReady (unityAdsRewardedZone);
			#endif
		}

		return false;
	}

	private void VideoFinished() {

		showing = false;
		if(onVideoFinished != null) onVideoFinished(true);
	}

	private void BannerClosed() {

		showing = false;
		if(onVideoFinished != null) onVideoFinished(false);
	}
}
