﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoBanner : MonoBehaviour {

	void Awake () {
	
		DontDestroyOnLoad (this);
	}

	// Use this for initialization
	void Start () {
	
		StartCoroutine (CheckCoroutine ());

		SceneManager.sceneLoaded += (Scene arg0, LoadSceneMode arg1) => {

			StartCoroutine (CheckCoroutine ());
		};
	}

	private IEnumerator CheckCoroutine () {
	
		while (!AdsController.Instance ().IsReady ())
			yield return null;

		if (!AdsController.Instance ().IsNoAds ())
			AdsController.Instance ().ShowAd (AdsProvider.AdType.banner);
	}
}
