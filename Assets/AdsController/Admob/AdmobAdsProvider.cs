﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class AdmobAdsProvider : AdsProvider {

	// These ad units are configured to always serve test ads.
	#if UNITY_ANDROID
	public string bannerId = "ca-app-pub-3940256099942544/6300978111";
	#elif UNITY_IPHONE
	public string bannerId = "ca-app-pub-3940256099942544/2934735716";
	#else
	public string bannerId = "unexpected_platform";
	#endif

	// These ad units are configured to always serve test ads.
	#if UNITY_ANDROID
	public string interstitialId = "ca-app-pub-3940256099942544/1033173712";
	#elif UNITY_IPHONE
	public string interstitialId = "ca-app-pub-3940256099942544/4411468910";
	#else
	public string interstitialId = "unexpected_platform";
	#endif

	#if UNITY_ANDROID
	public string rewardedVideoId = "ca-app-pub-3940256099942544/5224354917";
	#elif UNITY_IPHONE
	public string rewardedVideoId = "ca-app-pub-3940256099942544/1712485313";
	#else
	public string rewardedVideoId = "unexpected_platform";
	#endif

	private BannerView bannerView;
	private InterstitialAd interstitial;
	private NativeExpressAdView nativeExpressAdView;
	private RewardBasedVideoAd rewardBasedVideo;

	private Action<bool> onFinish;

	private bool showing;

	#if !UNITY_EDITOR
	private bool bannerLoaded;
	#endif

	private bool requestInterstitial;

	private bool requestRewardedVideo;

	private bool shouldShowBanner;

	[SerializeField]private bool bannerAtTop = false;
	[SerializeField]private bool smartBanner = false;

	public bool useBanner = true;

	public override void Init() {

		if (adsSettings != null) {

			bannerId = adsSettings.GetValue<string> ("ADMOBBANNERID", bannerId);
			interstitialId = adsSettings.GetValue<string> ("ADMOBINTERSTITIALID", interstitialId);
			rewardedVideoId = adsSettings.GetValue<string> ("ADMOBREWARDEDVIDEOID", rewardedVideoId);
			bannerAtTop = adsSettings.GetValue<bool> ("BANNERATTOP", bannerAtTop);
			smartBanner = adsSettings.GetValue<bool> ("SMARTBANNER", smartBanner);
			chance = adsSettings.GetValue<float> (name + "CHANCE", chance);
		}

		if (useBanner) RequestBanner();

		RequestInterstitial ();

		RequestRewardBasedVideo ();

		update += Update;

		inited = true;

		//MobileAds.Initialize (interstitialId);
	}

	public override bool IsBannerShowing ()
	{
		return shouldShowBanner;
	}

	public override void HideBanner ()
	{
		shouldShowBanner = false;
		if(bannerView != null) bannerView.Hide ();
	}

	// Returns an ad request with custom ad targeting.
	private AdRequest CreateAdRequest()
	{
		return new AdRequest.Builder()
			//.AddTestDevice(AdRequest.TestDeviceSimulator)
			//.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
			//.AddKeyword("game")
			//.SetGender(Gender.Male)
			//.SetBirthday(new DateTime(1985, 1, 1))
			//.TagForChildDirectedTreatment(false)
			//.AddExtra("color_bg", "9B30FF")
				.Build();
	}

	private void RequestBanner()
	{
		// Clean up banner ad before creating a new one.
		if (this.bannerView != null)
		{
			this.bannerView.Destroy();
		}

		// Create a 320x50 banner at the top of the screen.
		AdPosition position = AdPosition.Bottom;
		if (bannerAtTop) position = AdPosition.Top;
		AdSize size = AdSize.Banner;
		if (smartBanner) size = AdSize.SmartBanner;

		this.bannerView = new BannerView(bannerId, size, position);

		// Register for ad events.
		this.bannerView.OnAdLoaded += this.HandleAdLoaded;
		this.bannerView.OnAdFailedToLoad += this.HandleAdFailedToLoad;
		this.bannerView.OnAdOpening += this.HandleAdOpened;
		this.bannerView.OnAdClosed += this.HandleAdClosed;
		this.bannerView.OnAdLeavingApplication += this.HandleAdLeftApplication;

		// Load a banner ad.
		this.bannerView.LoadAd(this.CreateAdRequest());

		this.bannerView.Hide ();
	}

	private void RequestInterstitial()
	{
		Logger.Instance().Log ("Request interstitial");

		requestInterstitial = false;

		// Clean up interstitial ad before creating a new one.
		if (this.interstitial != null)
		{
			this.interstitial.Destroy();
		}

		// Create an interstitial.
		this.interstitial = new InterstitialAd(interstitialId);

		// Register for ad events.
		this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
		this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
		this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
		this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
		this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;

		// Load an interstitial ad.
		this.interstitial.LoadAd(this.CreateAdRequest());
	}

	private void RequestNativeExpressAdView()
	{
		// These ad units are configured to always serve test ads.
		#if UNITY_EDITOR
		string nativeAdId = "unused";
		#elif UNITY_ANDROID
		string nativeAdId = "ca-app-pub-3940256099942544/1072772517";
		#elif UNITY_IPHONE
		string nativeAdId = "ca-app-pub-3940256099942544/2562852117";
		#else
		string nativeAdId = "unexpected_platform";
		#endif

		// Clean up native express ad before creating a new one.
		if (this.nativeExpressAdView != null)
		{
			this.nativeExpressAdView.Destroy();
		}

		// Create a 320x150 native express ad at the top of the screen.
		this.nativeExpressAdView = new NativeExpressAdView(
			nativeAdId,
			new AdSize(320, 150),
			AdPosition.Top);

		// Register for ad events.
		this.nativeExpressAdView.OnAdLoaded += this.HandleNativeExpressAdLoaded;
		this.nativeExpressAdView.OnAdFailedToLoad += this.HandleNativeExpresseAdFailedToLoad;
		this.nativeExpressAdView.OnAdOpening += this.HandleNativeExpressAdOpened;
		this.nativeExpressAdView.OnAdClosed += this.HandleNativeExpressAdClosed;
		this.nativeExpressAdView.OnAdLeavingApplication += this.HandleNativeExpressAdLeftApplication;

		// Load a native express ad.
		this.nativeExpressAdView.LoadAd(this.CreateAdRequest());
	}

	private void RequestRewardBasedVideo()
	{

		requestRewardedVideo = false;

		// Get singleton reward based video ad reference.
		this.rewardBasedVideo = RewardBasedVideoAd.Instance;

		// RewardBasedVideoAd is a singleton, so handlers should only be registered once.
		this.rewardBasedVideo.OnAdLoaded += this.HandleRewardBasedVideoLoaded;
		this.rewardBasedVideo.OnAdFailedToLoad += this.HandleRewardBasedVideoFailedToLoad;
		this.rewardBasedVideo.OnAdOpening += this.HandleRewardBasedVideoOpened;
		this.rewardBasedVideo.OnAdStarted += this.HandleRewardBasedVideoStarted;
		this.rewardBasedVideo.OnAdRewarded += this.HandleRewardBasedVideoRewarded;
		this.rewardBasedVideo.OnAdClosed += this.HandleRewardBasedVideoClosed;
		this.rewardBasedVideo.OnAdLeavingApplication += this.HandleRewardBasedVideoLeftApplication;

		this.rewardBasedVideo.LoadAd(this.CreateAdRequest(), rewardedVideoId);
	}

	public override void ShowAd(AdType type, Action<bool> onFinish = null) {

		if (!inited)
		{
			Init();
			if (!inited) return;
		}

		if (type == AdType.skipablevideo)
			return;

		if (type == AdType.banner) {
			shouldShowBanner = true;
			if(bannerView != null) bannerView.Show ();
		}

		if (showing)
			return;

		waitingForAdEnd = true;

		storedVolume = AudioListener.volume;
		storedTScale = Time.timeScale;
		AudioListener.volume = 0;
		Time.timeScale = 0.001f;

		this.onFinish = onFinish;

		if (type == AdType.interstitial) {
			if (interstitial.IsLoaded())
			{
				showing = true;
				interstitial.Show();
			}
		}

		if (type == AdType.unskipablevideo) {
			if (rewardBasedVideo.IsLoaded()) {
				showing = true;
				rewardBasedVideo.Show();
			}
		}
	}

	public override void HideAd(AdType type) {

		if (type == AdType.banner) {
			shouldShowBanner = false;
			if(bannerView != null) bannerView.Hide ();    
		}
	}

	public override bool IsAdReady(AdType type) {

		if (showing)
			return false;

		#if UNITY_EDITOR
		return false;
		#else
		if (type == AdType.banner) return bannerLoaded;
		if (type == AdType.interstitial) return interstitial.IsLoaded();
		if (type == AdType.skipablevideo) return false;
		if (type == AdType.unskipablevideo) return rewardBasedVideo.IsLoaded();
		return false;
		#endif
	}

	private float requestInterstitialTimer = 2;
	private float requestRewardedVideoTimer = 2;

	//protected override void _Update () {
	void Update() {

		if (requestInterstitial) {
			requestInterstitialTimer -= Time.deltaTime;
			if (requestInterstitialTimer <= 0)
				RequestInterstitial ();
		}

		if (requestRewardedVideo) {
			requestRewardedVideoTimer -= Time.deltaTime;
			if (requestRewardedVideoTimer <= 0)
				RequestRewardBasedVideo ();
		}
	}

	void OnDestroy() {

	}

	#region Banner callback handlers

	private void HandleAdLoaded(object sender, EventArgs args)
	{
		#if !UNITY_EDITOR
		bannerLoaded = true;
		#endif
		if (shouldShowBanner)
		if(bannerView != null) bannerView.Show ();
		else
			if(bannerView != null) bannerView.Hide ();
		//print("HandleAdLoaded event received");
	}

	private void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		#if !UNITY_EDITOR
		bannerLoaded = false;
		#endif
		//print("HandleFailedToReceiveAd event received with message: " + args.Message);
	}

	private void HandleAdOpened(object sender, EventArgs args)
	{
		//print("HandleAdOpened event received");
	}

	private void HandleAdClosed(object sender, EventArgs args)
	{
		#if !UNITY_EDITOR
		bannerLoaded = false;
		#endif
		//print("HandleAdClosed event received");
	}

	private void HandleAdLeftApplication(object sender, EventArgs args)
	{
		//print("HandleAdLeftApplication event received");
	}

	#endregion

	#region Interstitial callback handlers

	private void HandleInterstitialLoaded(object sender, EventArgs args)
	{
		//Logger.Instance().Log ("HandleInterstitialLoaded event received");
	}

	private void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		requestInterstitial = true;
		requestInterstitialTimer = 2;
		//Logger.Instance().Log ("HandleInterstitialFailedToLoad event received with message: " + args.Message);
	}

	private void HandleInterstitialOpened(object sender, EventArgs args)
	{
		//Logger.Instance().Log ("HandleInterstitialOpened event received");
	}

	private void HandleInterstitialClosed(object sender, EventArgs args)
	{
		showing = false;
		requestInterstitial = true;
		//Logger.Instance().Log("HandleInterstitialClosed event received " + requestInterstitial);

		AudioListener.volume = storedVolume;
		Time.timeScale = storedTScale;

		if(this.onFinish != null) this.onFinish(true);
	}

	private void HandleInterstitialLeftApplication(object sender, EventArgs args)
	{
		//Logger.Instance().Log ("HandleInterstitialLeftApplication event received");
	}

	#endregion

	#region Native express ad callback handlers

	private void HandleNativeExpressAdLoaded(object sender, EventArgs args)
	{
		//print("HandleNativeExpressAdAdLoaded event received");
	}

	private void HandleNativeExpresseAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		//print("HandleNativeExpressAdFailedToReceiveAd event received with message: " + args.Message);
	}

	private void HandleNativeExpressAdOpened(object sender, EventArgs args)
	{
		//print("HandleNativeExpressAdAdOpened event received");
	}

	private void HandleNativeExpressAdClosed(object sender, EventArgs args)
	{
		//print("HandleNativeExpressAdAdClosed event received");
	}

	private void HandleNativeExpressAdLeftApplication(object sender, EventArgs args)
	{
		//print("HandleNativeExpressAdAdLeftApplication event received");
	}

	#endregion

	#region RewardBasedVideo callback handlers

	private bool waitingForAdEnd;
	private float storedVolume;
	private float storedTScale;

	private void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoLoaded event received");
	}

	private void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		requestRewardedVideo = true;
		requestRewardedVideoTimer = 2;
		//Logger.Instance().Log ("HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
	}

	private void HandleRewardBasedVideoOpened(object sender, EventArgs args)
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoOpened event received");
	}

	private void HandleRewardBasedVideoStarted(object sender, EventArgs args)
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoStarted event received");
	}

	private void HandleRewardBasedVideoClosed(object sender, EventArgs args)
	{
		showing = false;
		requestRewardedVideo = true;
		//Logger.Instance().Log ("HandleRewardBasedVideoClosed event received");

		if (!waitingForAdEnd)
			return;

		AudioListener.volume = storedVolume;
		Time.timeScale = storedTScale;

		waitingForAdEnd = false;

		if(this.onFinish != null) this.onFinish(false);
	}

	private void HandleRewardBasedVideoRewarded(object sender, Reward args)
	{
		string type = args.Type;
		double amount = args.Amount;
		//Logger.Instance().Log ("HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);

		if (!waitingForAdEnd)
			return;

		waitingForAdEnd = false;

		AudioListener.volume = storedVolume;
		Time.timeScale = storedTScale;

		if(this.onFinish != null) this.onFinish(true);
	}

	private void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
	{
		//Logger.Instance().Log ("HandleRewardBasedVideoLeftApplication event received");
	}

	#endregion
}
