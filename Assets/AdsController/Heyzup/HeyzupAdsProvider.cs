﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Heyzap;

public class HeyzupAdsProvider : AdsProvider {

	public string appId = "bdaa5c5b8497bad5dca61125dd1861ec";

	private bool showing;

	public bool testMenu;

	private System.Action<bool> onFinish;

	public override void Init ()
	{

        #if UNITY_EDITOR
        inited = true;
		return;
		#else
		if (adsSettings != null) {
			appId = adsSettings.GetValue<string> ("HEYZUPGAMEID", appId);
			//unityAdsDefaultZone = adsSettings.GetValue<string> ("UADEFAULTZONE", unityAdsDefaultZone);
			//unityAdsRewardedZone = adsSettings.GetValue<string> ("UAREWARDEDZONE", unityAdsRewardedZone);
			chance = adsSettings.GetValue<float> (name + "CHANCE", chance);
		}

		HeyzapAds.Start (appId, HeyzapAds.FLAG_NO_OPTIONS);

		HZVideoAd.Fetch ();
		HZIncentivizedAd.Fetch ();

		if(testMenu) HeyzapAds.ShowMediationTestSuite ();

		inited = true;


		#endif

		//Logger.Instance().Log ("Heyzup inited 1");

		HZInterstitialAd.AdDisplayListener interstitialListener = delegate(string state, string tag) {
			if (state.Equals ("show")) {
				//Logger.Instance().Log ("Heyzup interstitial showed");
			}
			if (state.Equals ("click")) {
			}
			if (state.Equals ("hide")) {
				showing = false;
				//Logger.Instance().Log ("Heyzup interstitial hided " + waitingForAdEnd + " " + shouldShowBanner);
				if (waitingForAdEnd) {
					waitingForAdEnd = false;
					if(onFinish != null) onFinish (true);
					AudioListener.volume = storedVolume;
					Time.timeScale = storedTScale;
					if(shouldShowBanner) {
						HZBannerShowOptions options = new HZBannerShowOptions ();
						options.Position = HZBannerShowOptions.POSITION_BOTTOM;
						HZBannerAd.ShowWithOptions (options);
					}
				}

			}
			if (state.Equals ("failed")) {
				showing = false;
				HZInterstitialAd.Fetch();
			}
			if (state.Equals ("fetch_failed")) {
				HZInterstitialAd.Fetch();
			}
		};
		HZInterstitialAd.SetDisplayListener (interstitialListener);

		HZVideoAd.AdDisplayListener videoListener = delegate(string state, string tag) {

			//Logger.Instance().Log ("Heyzup video callback " + state);

			if (state.Equals ("show")) {
			}
			if (state.Equals ("click")) {
			}
			if (state.Equals ("hide")) {
				showing = false;
				if (waitingForAdEnd) {
					waitingForAdEnd = false;
					if(onFinish != null) onFinish (true);
					AudioListener.volume = storedVolume;
					Time.timeScale = storedTScale;
					if(shouldShowBanner) {
						HZBannerShowOptions options = new HZBannerShowOptions ();
						options.Position = HZBannerShowOptions.POSITION_BOTTOM;
						HZBannerAd.ShowWithOptions (options);
					}
				}
			}
			if (state.Equals ("failed")) {
				showing = false;
				HZVideoAd.Fetch();
			}
			if (state.Equals ("fetch_failed")) {
				HZVideoAd.Fetch();
			}
		};
		HZVideoAd.SetDisplayListener (videoListener);

		HZIncentivizedAd.AdDisplayListener uvideoListener = delegate(string state, string tag) {
			if (state.Equals ("show")) {
			}
			if (state.Equals ("click")) {
			}
			if (state.Equals ("hide")) {
				showing = false;
				if(shouldShowBanner) {
					HZBannerShowOptions options = new HZBannerShowOptions ();
					options.Position = HZBannerShowOptions.POSITION_BOTTOM;
					HZBannerAd.ShowWithOptions (options);
				}
			}
			if (state.Equals ("failed")) {
				showing = false;
				HZIncentivizedAd.Fetch();
			}
			if (state.Equals ("fetch_failed")) {
				HZIncentivizedAd.Fetch();
			}

			if (state.Equals ("incentivized_result_complete")) {
				showing = false;
				if (waitingForAdEnd) {
					waitingForAdEnd = false;
					if(onFinish != null) onFinish (true);
					AudioListener.volume = storedVolume;
					Time.timeScale = storedTScale;
				}
			}

			if (state.Equals ("incentivized_result_incomplete")) {
				showing = false;
				if (waitingForAdEnd) {
					waitingForAdEnd = false;
					if(onFinish != null) onFinish (false);
					AudioListener.volume = storedVolume;
					Time.timeScale = storedTScale;
				}
			}
		};
		HZIncentivizedAd.SetDisplayListener (uvideoListener);

		HZBannerAd.AdDisplayListener bannerListener = delegate(string state, string tag) {

			//Logger.Instance().Log ("Heyzup banner callback " + state);
		};
		HZBannerAd.SetDisplayListener (bannerListener);

		//Logger.Instance().Log ("Heyzup inited 2");
	}

	private bool shouldShowBanner;
	private bool waitingForAdEnd;
	private float storedVolume;
	private float storedTScale;

	public override void ShowAd (AdType type, System.Action<bool> onFinish = null)
	{

        
		if (!inited) {
			Init ();
			if(!inited) return;
		}

        //Debug.Log(string.Format("Чонибудь {0} {1} {2}", inited, showing, onFinish));

        if (showing)
			return;

#if UNITY_EDITOR
        showing = false;
        if (onFinish != null) onFinish(true);
        return;
#else

        this.onFinish = onFinish;
		waitingForAdEnd = true;

		if (type == AdType.skipablevideo && HZVideoAd.IsAvailable ()) {
			HZVideoAd.Show ();
			showing = true;
		} else if (type == AdType.unskipablevideo && HZIncentivizedAd.IsAvailable ()) {
			HZIncentivizedAd.Show ();
			showing = true;
		} else if (type == AdType.interstitial && HZInterstitialAd.IsAvailable ()) {
			HZInterstitialAd.Show ();
			showing = true;
		} else if (type == AdType.banner) {
			shouldShowBanner = true;
			HZBannerShowOptions options = new HZBannerShowOptions ();
			options.Position = HZBannerShowOptions.POSITION_BOTTOM;
			HZBannerAd.ShowWithOptions (options);
		}

		if (showing) {
			HZBannerAd.Hide ();
			storedVolume = AudioListener.volume;
			storedTScale = Time.timeScale;
			AudioListener.volume = 0;
			Time.timeScale = 0.001f;
		}
#endif

	}

	public override bool IsAdReady(AdType type) {

		//Logger.Instance ().Log ("Heyzup IsAdReady " + type + ": " + HZVideoAd.IsAvailable () + " " + HZIncentivizedAd.IsAvailable () + " " + HZInterstitialAd.IsAvailable ());
		if (showing)
			return false;

		if (type == AdType.skipablevideo) {
#if UNITY_EDITOR
			return true;
#else
			return HZVideoAd.IsAvailable ();
#endif
		}

		if (type == AdType.unskipablevideo) {
#if UNITY_EDITOR
			return true;
#else
			return HZIncentivizedAd.IsAvailable ();
#endif
		}

		if (type == AdType.interstitial) {
#if UNITY_EDITOR
			return true;
#else
			return HZInterstitialAd.IsAvailable ();
#endif
		}

		if (type == AdType.banner) {
			return true;
		}

		return false;
	}

	public override void HideBanner ()
	{
		shouldShowBanner = false;
		HZBannerAd.Hide ();
	}

	public override void HideAd(AdType type) {

		if (type == AdType.banner) {
			shouldShowBanner = false;
			HZBannerAd.Hide ();   
		}
	}

	public void Update () {

		if (Input.GetKeyDown (KeyCode.Escape))
		if (HeyzapAds.OnBackPressed ())
			return;
	}
}
