﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPNoAds : MonoBehaviour {
	
    public static bool noAds = false;
    public GameObject restoreButton;

	// Use this for initialization
	void Start () {
#if UNITY_ANDROID
        restoreButton.gameObject.SetActive(false);
#endif
    }

    public void NoAds()
    {
        noAds = true;
		#if ADS_CONTROLLER
        AdsController.Instance().NoAds();
		#endif
        restoreButton.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
