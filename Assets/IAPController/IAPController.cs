﻿#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
// before receipt validation will compile in this sample.
#define RECEIPT_VALIDATION
#endif

using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;
#if RECEIPT_VALIDATION
using UnityEngine.Purchasing.Security;
#endif

/// <summary>
/// To use with your account, configure the product ids (AddProduct)
/// and Google Play key (SetPublicKey).
/// </summary>
public class IAPController : MonoBehaviour, IStoreListener
{

	private static IAPController instance;
	public static IAPController Instance() {
	
		if (instance == null) {
		
			instance = FindObjectOfType<IAPController> ();
			if (instance == null) {
				GameObject go = new GameObject ("IAPController");
				instance = go.AddComponent<IAPController> ();
			}
			instance.Init ();
		}

		return instance;
	}

	public List<IAP> iaps;

	#if UNITY_ANDROID
	public string googleKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnkQegboXC3Pr8+3yb374PDxV9pglzBfnyNp+xCapX0Vkf1zT9/GcSmBWT3Hwsy6ppzUkX4xKImad3APZ6juMK72o5/PAAPalXIAvtmSQmpTmqdSAKEDzdPWHCOczPPjmglJNg34m0H7VeUvcxK84dqIgsZpf8an1nMx8HRq/aZlQczokAL5MfPiPj/fGhcfqJ8FwMEgP8tYmOjN14ZRbzL13dO2pTSUjKHworSon0M5sDopC3dfXkkNJkW0Ukx99mHyYxjprzkwkqNN/UIqz+DNbpJVblx7UccnFGBpOeQOeKIMc83lBgHu+/a/2swmVas047rPTwtX4hbJlSBvIXQIDAQAB";
	#endif

	private IAP purchasedItem = null;

	// Unity IAP objects 
	private IStoreController m_Controller;
	private IAppleExtensions m_AppleExtensions;

	private bool m_PurchaseInProgress;
	private bool restoring;

	private Selectable m_InteractableSelectable; // Optimization used for UI state management

	public GameObject block;

	#if RECEIPT_VALIDATION && !UNITY_EDITOR
	private CrossPlatformValidator validator;
	#endif

	/// <summary>
	/// This will be called when Unity IAP has finished initialising.
	/// </summary>
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		m_Controller = controller;
		m_AppleExtensions = extensions.GetExtension<IAppleExtensions> ();
        
		// On Apple platforms we need to handle deferred purchases caused by Apple's Ask to Buy feature.
		// On non-Apple platforms this will have no effect; OnDeferred will never be called.
		m_AppleExtensions.RegisterPurchaseDeferredListener(OnDeferred);

		Log("Available items:");
		foreach (var item in controller.products.all)
		{
			if (item.availableToPurchase)
			{
				Log(string.Join(" - ",
					new[]
					{
						item.metadata.localizedTitle,
						item.metadata.localizedDescription,
						item.metadata.isoCurrencyCode,
						item.metadata.localizedPrice.ToString(),
						item.metadata.localizedPriceString
					}));
			}
		}

        CheckBuyedProducts();

        ready = true;
	}

	public void RestorePurchases () {
	
		if (!ready)
			return;

		restoring = true;

		m_AppleExtensions.RestoreTransactions (result => {
			if (result) {
				// This does not mean anything was restored,
				// merely that the restoration process succeeded.
				Log("Restoration process ok");
			} else {
				// Restoration failed.
				Log("Restoration process failed");
			}
		});
	}

	public UnityEngine.UI.Text log;
    public void Log(string s) {
	
		Debug.Log (s);
		if (log == null) return;
		log.text += s + "\n";
	}

	private void Log(object s) {

		Debug.Log (s);
		if (log == null) return;
		if (s == null) return;
		log.text += s.ToString() + "\n";
	}

	/// <summary>
	/// This will be called when a purchase completes.
	/// </summary>
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
	{
        Log("[ProcessPurchase]");
		Log ("Purchased item " + purchasedItem == null ? "null" : purchasedItem.iapid.ToString());
		Log ("Purchase OK: " + e.purchasedProduct.definition.id);

		m_PurchaseInProgress = false;

		if(e.purchasedProduct.hasReceipt) {
			for(int i = 0; i < iaps.Count; i++) {
				if(iaps[i].iapName.Equals(e.purchasedProduct.receipt)) {
					purchasedItem = iaps[i];
					break;
				}
			}
		}

		#if UNITY_EDITOR
		if (purchasedItem != null) purchasedItem.Buyed ();
		HideBlock ();
		return PurchaseProcessingResult.Complete;
		#elif RECEIPT_VALIDATION
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer) {

			try {
				var result = validator.Validate(e.purchasedProduct.receipt);
				Log("Receipt is valid. Contents:");
				foreach (IPurchaseReceipt productReceipt in result) {
					Log(productReceipt.productID);
					Log(productReceipt.purchaseDate);
					Log(productReceipt.transactionID);

					Log ("Calling onPurchaseComplete");

					purchasedItem = null;
					for(int i = 0; i < iaps.Count; i++) {
						if(iaps[i].iapName.Equals(productReceipt.productID)) {
							purchasedItem = iaps[i];
							break;
						}
					}

					// You should unlock the content here.
					if (purchasedItem != null) purchasedItem.Buyed ();

					/*GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
					if (null != google) {
						Log(google.purchaseState);
						Log(google.purchaseToken);
					}

					AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
					if (null != apple) {
						Log(apple.originalTransactionIdentifier);
						Log(apple.cancellationDate);
						Log(apple.quantity);
					}//*/
				}
			} catch (Exception ex) {
					
				Log("Invalid receipt, not unlocking content " + ex.ToString());
			}
		}
		#else
		if (purchasedItem != null) purchasedItem.Buyed();
		#endif

		#if !UNITY_EDITOR
		HideBlock ();
		// Indicate we have handled this purchase, we will not be informed of it again.
		return PurchaseProcessingResult.Complete;
		#endif
	}

	/// <summary>
	/// This will be called is an attempted purchase fails.
	/// </summary>
	public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
	{
		Log("Purchase failed: " + item.definition.id);
		Log(r);

		m_PurchaseInProgress = false;

		HideBlock ();
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Log("Billing failed to initialize!");
		Log ("Configuring " + builder.products.Count);

		switch (error)
		{
		case InitializationFailureReason.AppNotKnown:
			Log("Is your App correctly uploaded on the relevant publisher console?");
			break;
		case InitializationFailureReason.PurchasingUnavailable:
			// Ask the user if billing is disabled in device settings.
			Log("Billing disabled!");
			break;
		case InitializationFailureReason.NoProductsAvailable:
			// Developer configuration error; check product metadata.
			Log("No products available for purchase!");
			break;
		}
	}

	public void Awake()
	{

		Log ("Awake");

		if (instance == null)
			instance = this;
		else if(instance != this) {
			Destroy (this);
			return;
		}

		DontDestroyOnLoad (gameObject);

		if(!inited) Init ();
	}
	
	private ConfigurationBuilder builder;

	private bool inited = false;
	public void Init() {
	
		Log ("Initialize purchasing " + inited);
		#if UNITY_ANDROID
		Log ("Key: " + googleKey);
		#endif

		if (inited)
			return;
		
		var module = StandardPurchasingModule.Instance();

		// The FakeStore supports: no-ui (always succeeding), basic ui (purchase pass/fail), and 
		// developer ui (initialization, purchase, failure code setting). These correspond to 
		// the FakeStoreUIMode Enum values passed into StandardPurchasingModule.useFakeStoreUIMode.
		// module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

		builder = ConfigurationBuilder.Instance(module);
		// This enables the Microsoft IAP simulator for local testing.
		// You would remove this before building your release package.
		//builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = true;

		#if UNITY_ANDROID
		builder.Configure<IGooglePlayConfiguration>().SetPublicKey(googleKey);
		#else
		builder.Configure<IAppleConfiguration> ();
		#endif
		// Define our products.
		// In this case our products have the same identifier across all the App stores,
		// except on the Mac App store where product IDs cannot be reused across both Mac and
		// iOS stores.
		// So on the Mac App store our products have different identifiers,
		// and we tell Unity IAP this by using the IDs class.
		for (int i = 0; i < iaps.Count; i++) {
            ProductType type = ProductType.Consumable;
            if (iaps[i].nonconsumable) type = ProductType.NonConsumable;
            Log ("Add " + iaps [i].iapName);
			builder.AddProduct (iaps [i].iapName, type);//, new IDs { { iaps [i].iapName + ".mac", MacAppStore.Name }, }
			iaps [i].iapid = i;
		}
		// Write Amazon's JSON description of our products to storage when using Amazon's local sandbox.
		// This should be removed from a production build.
		//builder.Configure<IAmazonConfiguration>().WriteSandboxJSON(builder.products);

		#if RECEIPT_VALIDATION && !UNITY_EDITOR
		validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
		#endif

		Log ("Configuring " + builder.products.Count);

		// Now we're ready to initialize Unity IAP.
		UnityPurchasing.Initialize(this, builder);

		inited = true;
	}

	/// <summary>
	/// This will be called after a call to IAppleExtensions.RestoreTransactions().
	/// </summary>
	private void OnTransactionsRestored(bool success)
	{
		restoring = false;
		Log("Transactions restored.");
	}

	/// <summary>
	/// iOS Specific.
	/// This is called as part of Apple's 'Ask to buy' functionality,
	/// when a purchase is requested by a minor and referred to a parent
	/// for approval.
	/// 
	/// When the purchase is approved or rejected, the normal purchase events
	/// will fire.
	/// </summary>
	/// <param name="item">Item.</param>
	private void OnDeferred(Product item)
	{
		Log("Purchase deferred: " + item.definition.id);
	}

	public void CheckBuyedProducts()
	{
		if (m_Controller == null)
		{
			return;
		}

        Log("[CheckBuyedProducts]");

		for (int t = 0; t < m_Controller.products.all.Length; t++)
		{
			var item = m_Controller.products.all [t];
            //item.definition.type = ProductType.Subscription
            // Collect history status report

            var str = item.definition.id;
			str += "\n" + item.definition.type;
            str += "\n" + item.receipt;
			str += "\n" + string.Format("{0} - {1}", item.metadata.localizedTitle, item.metadata.localizedPriceString);
            Log(str);
            Log(item.hasReceipt);
            if (item.hasReceipt) RestoreProduct(item);
        }
	}

    private void RestoreProduct(Product purchasedProduct)
    {
        Log("Restoring " + (purchasedProduct != null));
        //Log("Purchased item " + purchasedItem == null ? "null" : purchasedItem.iapid.ToString());
        //Log("Purchase OK: " + purchasedProduct.definition.id);

        m_PurchaseInProgress = false;

        if (purchasedProduct.hasReceipt)
        {
            for (int i = 0; i < iaps.Count; i++)
            {
                if (iaps[i].iapName.Equals(purchasedProduct.receipt))
                {
                    purchasedItem = iaps[i];
                    break;
                }
            }
        }

#if UNITY_EDITOR
        if (purchasedItem != null) purchasedItem.Buyed();
        HideBlock();
        return;
#elif RECEIPT_VALIDATION
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer) {

			try {
				var result = validator.Validate(purchasedProduct.receipt);
				Log("Receipt is valid. Contents:");
				foreach (IPurchaseReceipt productReceipt in result) {
					Log(productReceipt.productID);
					Log(productReceipt.purchaseDate);
					Log(productReceipt.transactionID);

					Log ("Calling onPurchaseComplete");

					purchasedItem = null;
					for(int i = 0; i < iaps.Count; i++) {
						if(iaps[i].iapName.Equals(productReceipt.productID)) {
							purchasedItem = iaps[i];
							break;
						}
					}

					// You should unlock the content here.
					if (purchasedItem != null) purchasedItem.Buyed ();

					/*GooglePlayReceipt google = productReceipt as GooglePlayReceipt;
					if (null != google) {
						Log(google.purchaseState);
						Log(google.purchaseToken);
					}

					AppleInAppPurchaseReceipt apple = productReceipt as AppleInAppPurchaseReceipt;
					if (null != apple) {
						Log(apple.originalTransactionIdentifier);
						Log(apple.cancellationDate);
						Log(apple.quantity);
					}//*/
				}
			} catch (Exception ex) {
					
				Log("Invalid receipt, not unlocking content " + ex.ToString());
			}
		}
#else
		if (purchasedItem != null) purchasedItem.Buyed();
#endif

#if !UNITY_EDITOR
		HideBlock ();
		// Indicate we have handled this purchase, we will not be informed of it again.
		return;
#endif
    }

    public void InitiatePurchase(int i) {
	
		Log ("Initiate purchase " + i + " " + m_PurchaseInProgress);

		if (m_PurchaseInProgress == true || restoring) {
			return;
		}

		if (i < 0 || i >= m_Controller.products.all.Length) return;
			
		ShowBlock ();

		purchasedItem = iaps[i];

		m_PurchaseInProgress = true;

		m_Controller.InitiatePurchase(m_Controller.products.all[i]);
	}

	public void InitiatePurchase(IAP iap) {
	
		InitiatePurchase (iap.iapid);
	}

	private void HideBlock () {
	
		CancelInvoke ("HideBlock");
		if(block != null) block.SetActive (false);
	}

	private void ShowBlock () {
	
		if(block != null) block.SetActive (true);
		Invoke ("HideBlock", 5);
	}

	private bool ready = false;
	public bool Ready() {
	
		return ready;
	}

	public string GetPrice(int i) {
	
		if (i < 0 || i >= m_Controller.products.all.Length) return "";

		var item = m_Controller.products.all [i];

		return item.metadata.localizedPriceString;
	}

	public void ClearConsole () {
	
		if (log != null)
			log.text = "";
	}
}
