﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class IAP : MonoBehaviour {

	public string iapName;

	[HideInInspector]public int iapid;

	public Text lblPrice;

	public UnityEvent onPurchased;

	[HideInInspector]public bool avaliable;

	public bool nonconsumable = false;

	// Use this for initialization
	void Start () {

		if (nonconsumable) {
			if (PlayerPrefs.GetInt ("IAP" + iapName + "Buyed", 0) == 1) {
				if (onPurchased != null)
					onPurchased.Invoke ();
			}
		}
	}
	
	public virtual void Buyed () {

		if (nonconsumable) {
			PlayerPrefs.SetInt ("IAP" + iapName + "Buyed", 1);
		}

		if (onPurchased != null)
			onPurchased.Invoke ();
	}

	private IEnumerator GetPrice () {
	
		if (lblPrice == null)
			yield break;

		while (!IAPController.Instance ().Ready ())
			yield return null;
		
		lblPrice.text = IAPController.Instance ().GetPrice (iapid);
	}

	void OnEnable () {
	
		if (lblPrice != null)
			StartCoroutine (GetPrice ());
	}

	public void Buy () {
	
		IAPController.Instance ().InitiatePurchase (this);
	}
}
